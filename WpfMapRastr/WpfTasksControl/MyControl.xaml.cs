﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WpfTasksControl
{
    /// <summary>
    /// Interaction logic for UserControlTasks.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        // 333
        public event EventHandler OnTaskAzimuth;

        public event EventHandler OnTaskTriang;

        public UserControl1()
        {
            InitializeComponent();
        }

        // ButtonAzimuth **********************************************************************************
        // 333
        public void ButtonAzimuthTsk_Click(object sender, RoutedEventArgs e)
        {
            // 333
            OnTaskAzimuth?.Invoke(this, new EventArgs());
        }

        // ********************************************************************************** ButtonAzimuth

        // ButtonTriang ***********************************************************************************
        public void ButtonTriangTsk_Click(object sender, RoutedEventArgs e)
        {
            OnTaskTriang?.Invoke(this, new EventArgs());
        }

        // *********************************************************************************** ButtonTriang
    } //  Class
} // Namespace