﻿using System;

// Для события клика на CheckBox В DataGrid (с передачей параметров)
namespace WpfRouteControl
{
    public class ClassArgRoute : EventArgs
    {
        // Индекс строки в DataGrid
        public int index { get; }

        // Нажатие птички в CheckBox
        public bool checkM { get; }

        public ClassArgRoute(int Index, bool CheckM)
        {
            index = Index;
            checkM = CheckM;
        }
    } // Class
} // Namespace