﻿using System.Collections.Generic;
using System.Collections.ObjectModel; // forObservableCollection
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfRouteControl
{
    public class GlobalRouteParam : INotifyPropertyChanged
    {
        public ObservableCollection<RouteParam> CollRouteParam { get; set; }
        public List<RouteParam> lstRouteParam { get; set; }

        public bool flfirst;

        // Это будет число занесенных маршрутов, пока примем за число строк при первоначальном занесении
        public int shroutes;

        public GlobalRouteParam()
        {
            CollRouteParam = new ObservableCollection<RouteParam> { };

            lstRouteParam = new List<RouteParam>();

            flfirst = false;
            shroutes = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    } // Class
} // Namespace