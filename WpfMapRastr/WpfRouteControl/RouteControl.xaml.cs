﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WpfRouteControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RouteControl : UserControl
    {
        // EVENTS **************************************************************************************

        // Нажатие кнопки Add вверху (добавить маршрут)
        public event EventHandler OnAddRoute;

        // Событие по нажатию Птички в CheckBox (с передачей параметров)
        //public event EventHandler OnCheckBoxClick;
        public event EventHandler<ClassArgRoute> OnCheckBoxClick = (object sender, ClassArgRoute data) => { };

        // Событие по нажатию кнопки DEL вверху: удалить маршрут выделенной строки (с передачей параметров)
        public event EventHandler<ClassArgRouteDel> OnDelRoute = (object sender, ClassArgRouteDel data) => { };

        // Нажатие кнопки ClearAll вверху (очистить все маршруты)
        public event EventHandler OnClearAll;

        // ************************************************************************************** EVENTS

        public RouteControl()
        {
            InitializeComponent();

            dgRoute.DataContext = new GlobalRouteParam();
            dgRoute.ItemsSource = (dgRoute.DataContext as GlobalRouteParam).CollRouteParam;
            //((GlobalRouteParam)dgRoute.DataContext).CollRouteParam обращение к коллекции
        }

        // CHANGE ***************************************************************************************
        // !!! Пока эту кнопку убрали

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
        }

        // *************************************************************************************** CHANGE

        // ADD ******************************************************************************************
        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            OnAddRoute?.Invoke(this, new EventArgs());
        }

        // ****************************************************************************************** ADD

        // DELETE ***************************************************************************************
        // Удалить маршрут выделенной строки

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (
                (((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count > 0)
                )
            {
                // ***
                //((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count -= 1;

                // ------------------------------------------------------------------------------------------
                int index = 0;
                // ------------------------------------------------------------------------------------------
                // Индекс выделенной строки
                index = dgRoute.SelectedIndex;
                // ------------------------------------------------------------------------------------------

                if (
                    (index != -1) &&
                    //(((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index].ListCoordinates!=null)
                    (((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index].Length != 0)
                   )
                {
                    // ------------------------------------------------------------------------------------------
                    // Нужно убрать строку из коллекции и из DataGrid

                    ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Remove(((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index]);
                    ((GlobalRouteParam)dgRoute.DataContext).shroutes -= 1;
                    // ------------------------------------------------------------------------------------------
                    // Нужно ли добавить пустую строку

                    AddEmptyRowsWork();
                    // ------------------------------------------------------------------------------------------
                    dgRoute.Items.Refresh();
                    // ------------------------------------------------------------------------------------------
                    // Событие с передачей параметров

                    //OnDelRoute(this, new ClassArgRouteDel(index));
                    // ------------------------------------------------------------------------------------------
                }

                // ------------------------------------------------------------------------------------------
                // Событие с передачей параметров

                OnDelRoute(this, new ClassArgRouteDel(index));
                // ------------------------------------------------------------------------------------------
            }
        }

        // *************************************************************************************** DELETE

        // CLEAR ****************************************************************************************
        // Очистить все маршруты

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            // ------------------------------------------------------------------------------------------
            int index = 0;
            // ------------------------------------------------------------------------------------------
            // Убрать строки из коллекции и DataGrid

            for (index = ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count - 1; index >= 0; index--)
            {
                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Remove(((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index]);
                ((GlobalRouteParam)dgRoute.DataContext).shroutes -= 1;
            } // FOR
            // ------------------------------------------------------------------------------------------
            // Добавить пустые строки, если нужно

            ((GlobalRouteParam)dgRoute.DataContext).shroutes = 0;
            AddEmptyRowsWork();

            dgRoute.Items.Refresh();
            // ------------------------------------------------------------------------------------------
            OnClearAll?.Invoke(this, new EventArgs());
            // ------------------------------------------------------------------------------------------
        }

        // **************************************************************************************** CLEAR

        // SizeChanged **********************************************************************************
        // Добавление пустых строк при изменении размера datagrid
        // Сюда входит при переходе на вкладку Route и изменении размера окна

        private void dgRoute_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (((GlobalRouteParam)dgRoute.DataContext).flfirst == false)
            {
                ((GlobalRouteParam)dgRoute.DataContext).flfirst = true;
                AddEmptyRowsInit();
                // Это будет число занесенных маршрутов, пока примем за число строк при первоначальном занесении
                ((GlobalRouteParam)dgRoute.DataContext).shroutes = 0;

                // -----------------------------------------------------------------------------------------
                // Если есть информация из БД

                if (((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Count > 0)
                {
                    for (int ij = 0; ij < ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Count; ij++)
                    {
                        AddRouteDG(
                                   ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[ij].Caption,
                                   (int)((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[ij].Length,
                                   ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[ij].npoints
                                  );
                    }
                }
                // -----------------------------------------------------------------------------------------
            }
            else
            {
                AddEmptyRowsWork();
            }
        }

        // ********************************************************************************** SizeChanged

        // EVENT_птичка в CheckBoxColumns ****************************************************************
        // Событие по установлению/снятию птички

        private void OnChecked(object sender, RoutedEventArgs e)
        {
            //throw new NotImplementedException();

            int index = 0;
            bool checkCB = false;

            // Индекс выделенной строки
            index = dgRoute.SelectedIndex;
            // Check/Uncheck
            checkCB = ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index].checkMap;

            // Событие с передачей параметров
            //OnCheckBoxClick?.Invoke(this, new EventArgs());
            OnCheckBoxClick(this, new ClassArgRoute(index, checkCB));
        }

        // **************************************************************** EVENT_птичка в CheckBoxColumns
    }  // Class
} // Namespace