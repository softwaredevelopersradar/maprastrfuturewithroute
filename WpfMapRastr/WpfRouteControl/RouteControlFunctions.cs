﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace WpfRouteControl
{
    // 2-я часть контрол.xaml.cs
    public partial class RouteControl : UserControl
    {
        // Добавить пустые строки DataGrid при инициализации ********************************************************

        public void AddEmptyRowsInit()
        {
            try
            {
                int сountRowsAll = dgRoute.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgRoute.ActualHeight; // визуализированная высота dataGrid
                double chh = dgRoute.ColumnHeaderHeight; // высота заголовка

                // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs);

                List<RouteParam> list = new List<RouteParam>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    RouteParam strSR = new RouteParam();

                    // Otl
                    // Для отладки
                    // strSR.Caption = "xxx";
                    // strSR.npoints = 100;
                    // strSR.Length = 33.7;
                    // strSR.checkMap = true;

                    strSR.Caption = "";
                    strSR.npoints = -3;
                    strSR.Length = -3;
                    strSR.checkMap = true;

                    list.Add(strSR);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Add(list[i]);
                }

                // Удалить 1-ю строку
                //((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[0].Caption = "hhh";
                //MessageBox.Show("OK");
                //((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Remove(((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[1]);

                //AddRouteDG(
                //                         "xxx",
                //                         3,
                //                         130000
                //                        );
            } // TRY
            catch { }
        }

        // ******************************************************** Добавить пустые строки DataGrid при инициализации

        // Добавление/удаление пустых строк в рабочем режиме ********************************************************
        public void AddEmptyRowsWork()
        {
            try
            {
                int сountRowsAll = dgRoute.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = dgRoute.ActualHeight; // визуализированная высота dataGrid
                double chh = dgRoute.ColumnHeaderHeight; // высота заголовка

                // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки
                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs);

                List<RouteParam> list = new List<RouteParam>();

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Количество строк в видимой области >= количества строк в таблице

                // IF1
                if (countRows >= сountRowsAll)
                {
                    //for (int i = 0; i < (countRows - (сountRowsAll - ((GlobalRouteParam)dgRoute.DataContext).shroutes)); i++)
                    for (int i = 0; i < countRows - сountRowsAll; i++)
                    {
                        RouteParam strSR = new RouteParam();
                        strSR.Caption = "";
                        strSR.npoints = -3;
                        strSR.Length = -3;
                        strSR.checkMap = true;
                        list.Add(strSR);
                    }

                    for (int i = 0; i < list.Count; i++)
                    {
                        ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Add(list[i]);
                    }
                } // IF1
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Количество строк в видимой области < количества строк в таблице

                // IF2
                //if (countRows < сountRowsAll)
                else
                {
                    // ------------------------------------------------------------------------------------------------
                    // Количество строк с занесенными маршрутами <= количества строк в видимой области

                    // IF3
                    if (((GlobalRouteParam)dgRoute.DataContext).shroutes <= countRows)
                    {
                        if (((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count > 0)
                        {
                            int i1_1 = ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count - 1;
                            for (int i1 = 0; i1 < (сountRowsAll - countRows); i1++)
                            {
                                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Remove(((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[i1_1]);

                                i1_1 -= 1;
                            }
                        }
                    } // IF3
                    // ------------------------------------------------------------------------------------------------
                    // Количество строк с занесенными маршрутами > количества строк в видимой области

                    // ELSE on IF3
                    else
                    {
                        if (((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count > 0)
                        {
                            int i2_1 = ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count - 1;
                            for (int i2 = 0; i2 < (сountRowsAll - ((GlobalRouteParam)dgRoute.DataContext).shroutes); i2++)
                            {
                                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Remove(((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[i2_1]);

                                i2_1 -= 1;
                            }
                        }
                    } // ELSE on IF3
                    // ------------------------------------------------------------------------------------------------
                } // IF2
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            } // TRY
            catch { }
        }

        // ******************************************************** Добавление/удаление пустых строк в рабочем режиме

        // Занесение маршрута в DataGrid ****************************************************************************

        //public async void AddRouteDG(
        public void AddRouteDG(
                               string name,
                               double length,
                               int npoints
                              )
        {
            // ----------------------------------------------------------------------------------------------------
            int index = 0;
            // ----------------------------------------------------------------------------------------------------
            // Количество занесенных маршрутов

            if (((GlobalRouteParam)dgRoute.DataContext).shroutes < 0)
                ((GlobalRouteParam)dgRoute.DataContext).shroutes = 0;

            index = ((GlobalRouteParam)dgRoute.DataContext).shroutes;

            // ----------------------------------------------------------------------------------------------------
            RouteParam strSR = new RouteParam();
            // ----------------------------------------------------------------------------------------------------
            int сountRowsAll = dgRoute.Items.Count; // количество всех строк в таблице
            // ----------------------------------------------------------------------------------------------------
            strSR.Caption = name;
            strSR.npoints = npoints;
            strSR.Length = length;
            // ----------------------------------------------------------------------------------------------------
            // Индекс занесения больше количества строк в таблице

            if (index > сountRowsAll)
            {
                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Add(strSR);
            }
            // ----------------------------------------------------------------------------------------------------
            // Индекс занесения меньше количества строк в таблице
            else
            {
                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index].Caption = name;
                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index].Length = length;
                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[index].npoints = npoints;
            }
            // ----------------------------------------------------------------------------------------------------
            ((GlobalRouteParam)dgRoute.DataContext).shroutes += 1;
            // ----------------------------------------------------------------------------------------------------

            dgRoute.Items.Refresh();
        }

        // **************************************************************************** Занесение маршрута в DataGrid

        // Занесение маршрута в DataGrid from DB*********************************************************************

        //public async void AddRouteDG(
        public void AddRouteDG1(
                              )
        {
            /*
                        // ----------------------------------------------------------------------------------------------------
                        int index = 0;

                        // Количество занесенных маршрутов
                        ((GlobalRouteParam)dgRoute.DataContext).shroutes = 0;

                        int сountRowsAll = dgRoute.Items.Count; // количество всех строк в таблице
                        // ----------------------------------------------------------------------------------------------------
            */

            /*
                        for (int i=0;i< ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Count;i++)
                        {
                            // ----------------------------------------------------------------------------------------------------
                            RouteParam strSR = new RouteParam();
                            // ----------------------------------------------------------------------------------------------------
                            strSR.Caption = ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[i].Caption;
                            strSR.npoints = ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[i].npoints;
                            strSR.Length = ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[i].Length;
                            // ----------------------------------------------------------------------------------------------------
                            // Индекс занесения больше количества строк в таблице

                            if (i>0)
                            {
                                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Add(strSR);
                            }
                            // ----------------------------------------------------------------------------------------------------
                            // Индекс занесения меньше количества строк в таблице
                            else
                            {
                                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[i].Caption = strSR.Caption;
                                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[i].Length = strSR.Length;
                                ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam[i].npoints = strSR.npoints;
                            }
                        // ----------------------------------------------------------------------------------------------------
                        ((GlobalRouteParam)dgRoute.DataContext).shroutes += 1;
                        // ----------------------------------------------------------------------------------------------------
                        }

                        dgRoute.Items.Refresh();
            */

            for (int i = 0; i < ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Count; i++)
            {
                AddRouteDG(
                                       ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[i].Caption,
                                       ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[i].Length,
                                       ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam[i].npoints
                          );
            }
        }

        // ********************************************************************* Занесение маршрута в DataGrid from DB

        // Занесение маршрута в лист из БД **************************************************************************

        public void AddRouteLst(
                           string name,
                           double length,
                           int npoints,
                           bool flag
                          )
        {
            if (flag == false)
                ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Clear();
            // ----------------------------------------------------------------------------------------------------
            RouteParam strSR = new RouteParam();
            // ----------------------------------------------------------------------------------------------------
            strSR.Caption = name;
            strSR.npoints = npoints;
            strSR.Length = length;
            // ----------------------------------------------------------------------------------------------------
            ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Add(strSR);
            // ----------------------------------------------------------------------------------------------------
        }

        // ************************************************************************** Занесение маршрута в лист из БД

        // Очистка коллекции ****************************************************************************************

        public void ClearColl(
                              )
        {
            ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Clear();
        }

        // **************************************************************************************** Очистка коллекции

        // Очистка листа ********************************************************************************************

        public void ClearLst(
                              )
        {
            ((GlobalRouteParam)dgRoute.DataContext).lstRouteParam.Clear();
        }

        // ******************************************************************************************** Очистка листа
    } // Class
} // Namespsce