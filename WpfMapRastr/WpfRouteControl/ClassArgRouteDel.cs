﻿using System;

// Для события по нажатию кнопки Del в DataGrid (Удалить маршрут по выделенной строке)
namespace WpfRouteControl
{
    public class ClassArgRouteDel : EventArgs
    {
        // Индекс строки в DataGrid
        public int index { get; }

        public ClassArgRouteDel(int Index)
        {
            index = Index;
        }
    } // Class
} // Namespace