﻿using ClientDataBase;
using InheritorsEventArgs;
using ModelsTablesDBLib;
using ReliefProfileControl.Models;
using System;
using System.Threading;
using System.Windows;

// Semen

namespace WpfMapRastr
{
    // Соединение/отключение к БД
    public partial class MainWindow
    {
        // DATA_BASE *********************************************************************************************

        private ClientDB clientDB;
        
        // ------------------------------------------------------------------------------------------------------
        // BUTTON BD

        private void ConnectBD_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                {
                    clientDB?.Disconnect();
                    clientDB = null;
                }
                else
                {
                    GlobalVarMapMain.fltbl = false;

                    Name = "Map";

                    clientDB = new ClientDB(this.Name, $"{SettingsMap.Settings.IP_DB}:{SettingsMap.Settings.NumPort}");

                    InitClientDB();
                    clientDB.ConnectAsync();
                }
                ChoiceLanguage();
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                HandlerDisconnect(this, null);
                MessageBox.Show(exceptClient.Message);
            }
            catch (Exception)
            { }
        }

        // ------------------------------------------------------------------------------------------------------

        private void InitClientDB()
        {
            // На остальные события подписываемся после загрузки таблиц
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
        }

        // ------------------------------------------------------------------------------------------------------

        //if(GlobalVarMapMain.fltmr_air==0)
        //{
        //    GlobalVarMapMain.fltmr_air = 1;
        //    //System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        //    GlobalVarMapMain.dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
        //    GlobalVarMapMain.dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
        //    //GlobalVarMapMain.dispatcherTimer.Start();
        //} // Tmr

        // Tmr ************************************************************************************************
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // code goes here
            // Draw
            if (
                (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_AP == true)
                )
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
        }

        // ************************************************************************************************ Tmr

        // ERROR_BD *******************************************************************************************

        private void HandlerErrorDataBase(object sender, OperationTableEventArgs e)
        {
            MessageBox.Show(e.GetMessage);
        }

        // ******************************************************************************************* ERROR_BD

        // DISCONNECT_BD **************************************************************************************

        private void HandlerDisconnect(object sender, ClientEventArgs e)
        {
            // Updated table of ИРИ ФРЧ
            (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable -= OnTempFWSUp;
            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange -= OnTempFWSAddRange;

            // Updated table of ИРИ ФРЧ ЦР
            (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable -= OnTableReconFWSUp;
            (clientDB.Tables[NameTable.TempSuppressFWS] as ITableUpdate<TempSuppressFWS>).OnUpTable -= OnTempSuppressFWS;
            // Updated table of ИРИ ФРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable -= OnTableSuppressFWSUp;

            // ИРИ ППРЧ
            // Для очистки
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable -= OnTableReconFHSSUp;
            // For Coordinates
            // Для очистки
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable -= OnTableSourceFHSSUp;
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange -= OnTableReconFHSSAddRange;
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableAddRange<TableSourceFHSS>).OnAddRange -= OnTableSourceFHSSAddRange;

            // Updated table of ИРИ ППРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable -= OnTableSuppressFHSSUp;
            // Updated table of ASP (СП)
            (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable -= OnTableAspUp;
            //fill ADSB table by planes + Map
            (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable -= HandlerUpdateTempADSB;
            (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord -= HandlerAddRecordTempADSB;
            //  GPS
            (clientDB.Tables[NameTable.TempGNSS] as ITableUpdate<TempGNSS>).OnUpTable -= OnTempGNSSUp;

            (clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable -= OnUpdateSpoof;
            mapCtrl.OnGetCoordSpoof -= MapCtrl_OnGetCoordSpoof;
            mapCtrl.OnAddIRIToSuppres -= MapCtrl_OnAdIRIToSuppress;
            mapCtrl.DeleteIRISuppress -= MapCtrl_OnDeleteIRISuppress;
           

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ButConnectBD.ShowDisconnect();
                clientDB = null;
            });

            objClassFunctionsMain.ClearLists();

            DispatchIfNecessary(() =>
            {
                mapCtrl.Txt10.Text = "";
            });

            //GlobalVarMapMain.dispatcherTimer.Stop();
            objClassFunctionsMain.ClearSpoof();
            // Azimuth*
            objClassFunctionsMain.ClearAzimuth();
            // -----------------------------------------------------------------
            // Clear and redraw
            // ??????????????????

            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            // -----------------------------------------------------------------
        }

        // ************************************************************************************** DISCONNECT_BD

        // CONNECT_BD *****************************************************************************************

        private void HandlerConnect(object sender, ClientEventArgs e)
        {
            try
            {
                GlobalVarMapMain.fltbl = false;

                ButConnectBD.ShowConnect();
                objClassFunctionsMain.ClearLists();

                LoadTables1();
                // -------------------------------------------------------------------------------------------------
            }
            catch (Exception ex)
            {
            }
        }

        // ***************************************************************************************** CONNECT_BD

        // ********************************************************************************************* DATA_BASE
    } // Class
} // Namespace