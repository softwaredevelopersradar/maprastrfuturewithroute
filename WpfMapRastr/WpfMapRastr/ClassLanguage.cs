﻿namespace WpfMapRastr
{
    public partial class MainWindow
    {
        // Button Language_Choice ***********************************************************************
        // Флаг языка: по умолчанию английский (false)
        // true  -> русский
        //FlagLanguage = false;

        // AAA
        //public void Language_Click(object sender, RoutedEventArgs e)
        public void ChoiceLanguage()

        {
            // Переход на русский **********************************************************************
            AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Eng;
            //if (GlobalVarMapMain.FlagLanguage==false)
            if (GlobalVarMapMain.FlagLanguage == 0)
            {
                // GlobalVarMapMain.FlagLanguage = true;

                // ------------------------------------------------------------------------------------
                // MAIN

                //TextLanguage.Text = "Русский";
                //Language.ToolTip = "Выбор языка";
                ButtonSettings.ToolTip = "Установки";
                TextBlockEnvironments.Text = "Обстановки";

                MenuEnvironment.ToolTip = "Тактическая и радиоэлектронная обстановка";
                StackPanelEnvironments.ToolTip = "Тактическая и радиоэлектронная обстановка";

                MenuItem1.Header = "Минск";
                MenuItem2.Header = "Береза";
                MenuItem3.Header = "Брест";

                TabCtrlTasksIt1.ToolTip = "Азимут";
                ADSBTable.ToolTip = "Таблица воздушных объектов";

                ButConnectBD.LabelText = "БД";
                ButConnectBD.ToolTip = "БД";
                // Route
                // RRR3
                TabCtrlTasksIt2.ToolTip = "Маршрут";

                // ------------------------------------------------------------------------------------
                // MAP_CONTROL

                // Menu_map
                mapCtrl.TextBlockMenuMap.Text = "Карта";
                mapCtrl.MenuMap.ToolTip = "Открыть/закрыть карту/матрицу высот";
                mapCtrl.StackPanelMenuMap.ToolTip = "Открыть/закрыть карту/матрицу высот";
                mapCtrl.MenuMapHeader1.Header = "Открыть карту";
                mapCtrl.MenuMapHeader2.Header = "Закрыть карту";
                mapCtrl.MenuMapHeader3.Header = "Открыть матрицу высот";

                // Button +
                mapCtrl.ButtonPlus.ToolTip = "Увеличить масштаб";
                mapCtrl.StackPanelButtonPlus.ToolTip = "Увеличить масштаб";

                // Button -
                mapCtrl.ButtonMinus.ToolTip = "Уменьшить масштаб";
                mapCtrl.StackPanelButtonMinus.ToolTip = "Уменьшить масштаб";

                // Button Base
                mapCtrl.ButtonBase.ToolTip = "Базовый масштаб";
                mapCtrl.StackPanelButtonBase.ToolTip = "Базовый масштаб";

                // Button Center
                mapCtrl.ButtonSenter.ToolTip = "Центрирование по базовой станции";
                mapCtrl.StackPanelButtonSenter.ToolTip = "Центрирование по базовой станции";
                mapCtrl.TextBlockButtonSenter.Text = "СП база";

                // Button JS
                mapCtrl.ButtonJS.ToolTip = "Станции помех";
                mapCtrl.StackPanelButtonJS.ToolTip = "Станции помех";
                mapCtrl.checkBoxJS.Content = " СП";

                // Button SRW_FRF
                // sourse of radio waves(SRW) fixed radio frequency(ies)
                // radio emission sources(RES) fixed radio frequency(ies)
                // ИРИ ФРЧ(фиксированной радиочастоты)
                mapCtrl.ButtonObjects1.ToolTip = "ИРИ фиксированной радиочастоты";
                mapCtrl.StackPanelButtonObjects1.ToolTip = "ИРИ фиксированной радиочастоты";
                mapCtrl.checkBoxObj1.Content = " ИРИФРЧ";

                // Button SRW_FRF_TD
                // sourse of radio waves(SRW) fixed radio frequency(ies) for target distribution
                // ИРИ ФРЧ(фиксированной радиочастоты) ЦР(для целераспределения)
                mapCtrl.ButtonObjects2.ToolTip = "ИРИ фиксированной радиочастоты для целераспределения";
                mapCtrl.StackPanelButtonObjects2.ToolTip = "ИРИ фиксированной радиочастоты для целераспределения";
                mapCtrl.checkBoxObj2.Content = " ИРИФРЧ ЦР";

                // Button SRW_FRF_RS
                // sourse of radio waves(SRW) fixed radio frequency(ies) for radio suppression
                // ИРИ ФРЧ(фиксированной радиочастоты) РП(для радиоподавления)
                mapCtrl.ButtonObjects3.ToolTip = "ИРИ фиксированной частоты для радиоподавления";
                mapCtrl.StackPanelButtonObjects3.ToolTip = "ИРИ фиксированной частоты для радиоподавления";
                mapCtrl.checkBoxObj3.Content = " ИРИФРЧ РП";

                // Button SRW_STRF
                // sourse of radio waves(SRW) with software tunable RF
                // ИРИ ППРЧ(программно перестраиваемой радиочастоты)
                mapCtrl.ButtonObjects4.ToolTip = "ИРИ программно перестраиваемой частоты";
                mapCtrl.StackPanelButtonObjects4.ToolTip = "ИРИ программно перестраиваемой частоты";
                mapCtrl.checkBoxObj4.Content = " ИРИ ППРЧ";

                // Bearing
                mapCtrl.ButtonObjects5.ToolTip = "Пеленги";
                mapCtrl.StackPanelButtonObjects5.ToolTip = "Пеленги";
                mapCtrl.checkBoxObj5.Content = " Пеленги";

                // Aircraft
                mapCtrl.ButtonAirplanes.ToolTip = "Воздушные объекты";
                mapCtrl.StackPanelButtonAirplanes.ToolTip = "Воздушные объекты";
                mapCtrl.checkBoxAirplanes.Content = " ВО";

                // Button Antennas
                // Направления антенн
                mapCtrl.ButtonAntennas.ToolTip = "Направления антенн";
                mapCtrl.StackPanelButtonAntennas.ToolTip = "Направления антенн";
                mapCtrl.checkBoxAntennas.Content = " Антенны";

                // S от базовой СП
                mapCtrl.ButtonSSP.ToolTip = "Расстояние от своей станции";
                //mapCtrl.StackPanelButtonSSP.ToolTip = "Расстояние от своей станции";
                mapCtrl.checkBoxSSP.Content = "Расстояние";

                mapCtrl.Txt10.Text = "Количество ВО";
                // ------------------------------------------------------------------------------------

                // ----------------------------------------------------------------------------------------
                // Russian в контроле азимута

                languages = AzimutTask.Enums.Languages.Rus;
                AzimutTaskCntr.ChangeLanguge(languages);
                // ----------------------------------------------------------------------------------------
            }
            // ********************************************************************** Переход на русский

            // Переход на английский *******************************************************************
            else if (GlobalVarMapMain.FlagLanguage == 1)
            {
                //GlobalVarMapMain.FlagLanguage = false;

                // ------------------------------------------------------------------------------------
                // MAIN

                //TextLanguage.Text = "English";
                //Language.ToolTip = "LanguageChoice";
                ButtonSettings.ToolTip = "Settings";
                TextBlockEnvironments.Text = "Environments";

                MenuEnvironment.ToolTip = "Tactical and radio environment";
                StackPanelEnvironments.ToolTip = "Tactical and radio environment";

                MenuItem1.Header = "Environment1";
                MenuItem2.Header = "Environment2";
                MenuItem3.Header = "Environment3";

                TabCtrlTasksIt1.ToolTip = "Azimuth";
                ADSBTable.ToolTip = "Aircraft table";

                ButConnectBD.LabelText = "DB";
                ButConnectBD.ToolTip = "DB";
                // Route
                // RRR3
                TabCtrlTasksIt2.ToolTip = "Route";

                // ------------------------------------------------------------------------------------
                // MAP_CONTROL

                // Menu_map
                mapCtrl.TextBlockMenuMap.Text = "Map";
                mapCtrl.MenuMap.ToolTip = "Open/Close Map/Open the Height Matrix";
                mapCtrl.StackPanelMenuMap.ToolTip = "Open/Close Map/Open the Height Matrix";
                mapCtrl.MenuMapHeader1.Header = "Open map";
                mapCtrl.MenuMapHeader2.Header = "Close map";
                mapCtrl.MenuMapHeader3.Header = "Open the Height Matrix";

                // Button +
                mapCtrl.ButtonPlus.ToolTip = "Increase scale";
                mapCtrl.StackPanelButtonPlus.ToolTip = "Increase scale";

                // Button -
                mapCtrl.ButtonMinus.ToolTip = "Decrease scale";
                mapCtrl.StackPanelButtonMinus.ToolTip = "Decrease scale";

                // Button Base
                mapCtrl.ButtonBase.ToolTip = "Base scale";
                mapCtrl.StackPanelButtonBase.ToolTip = "Base scale";

                // Button Center
                mapCtrl.ButtonSenter.ToolTip = "Base station";
                mapCtrl.StackPanelButtonSenter.ToolTip = "Base station";
                mapCtrl.TextBlockButtonSenter.Text = "Base JS";

                // Button JS
                mapCtrl.ButtonJS.ToolTip = "Jammer stations";
                mapCtrl.StackPanelButtonJS.ToolTip = "Jammer stations";
                mapCtrl.checkBoxJS.Content = " JS";

                // Button SRW_FRF
                // sourse of radio waves(SRW) fixed radio frequency(ies)
                // radio emission sources(RES) fixed radio frequency(ies)
                // ИРИ ФРЧ(фиксированной радиочастоты)
                mapCtrl.ButtonObjects1.ToolTip = "RES of fixed frequency";
                mapCtrl.StackPanelButtonObjects1.ToolTip = "RES of fixed frequency";
                mapCtrl.checkBoxObj1.Content = " RES FF";

                // Button SRW_FRF_TD
                // sourse of radio waves(SRW) fixed radio frequency(ies) for target distribution
                // ИРИ ФРЧ(фиксированной радиочастоты) ЦР(для целераспределения)
                mapCtrl.ButtonObjects2.ToolTip = "RES of fixed frequency for target distribution";
                mapCtrl.StackPanelButtonObjects2.ToolTip = "RES of fixed frequency for target distribution";
                mapCtrl.checkBoxObj2.Content = " RES FF TD";

                // Button SRW_FRF_RS
                // sourse of radio waves(SRW) fixed radio frequency(ies) for radio suppression
                // ИРИ ФРЧ(фиксированной радиочастоты) РП(для радиоподавления)
                mapCtrl.ButtonObjects3.ToolTip = "RES of fixed frequency for jamming";
                mapCtrl.StackPanelButtonObjects3.ToolTip = "RES of fixed frequency for jamming";
                mapCtrl.checkBoxObj3.Content = " RES FF J";

                // Button SRW_STRF
                // sourse of radio waves(SRW) with software tunable RF
                // ИРИ ППРЧ(программно перестраиваемой радиочастоты)
                mapCtrl.ButtonObjects4.ToolTip = "RES of frequency hopping spread spectrum";
                mapCtrl.StackPanelButtonObjects4.ToolTip = "RES of frequency hopping spread spectrum";
                mapCtrl.checkBoxObj4.Content = " RES FHSS";

                // Bearing
                mapCtrl.ButtonObjects5.ToolTip = "Bearing";
                mapCtrl.StackPanelButtonObjects5.ToolTip = "Bearing";
                mapCtrl.checkBoxObj5.Content = " Bearing";

                // Aircraft
                mapCtrl.ButtonAirplanes.ToolTip = "Aircraft";
                mapCtrl.StackPanelButtonAirplanes.ToolTip = "Aircraft";
                mapCtrl.checkBoxAirplanes.Content = " Aircraft";

                // Button Antennas
                // Направления антенн
                mapCtrl.ButtonAntennas.ToolTip = "Antenna directions";
                mapCtrl.StackPanelButtonAntennas.ToolTip = "Antenna directions";
                mapCtrl.checkBoxAntennas.Content = " Antennas";

                // S от базовой СП
                mapCtrl.ButtonSSP.ToolTip = "Distance from own station";
                //mapCtrl.StackPanelButtonSSP.ToolTip = "Distance from own station";
                mapCtrl.checkBoxSSP.Content = "Distance";

                mapCtrl.Txt10.Text = "Number of aircraft";

                // ------------------------------------------------------------------------------------

                // ----------------------------------------------------------------------------------------
                // English в контроле азимута

                languages = AzimutTask.Enums.Languages.Eng;
                AzimutTaskCntr.ChangeLanguge(languages);
                // ----------------------------------------------------------------------------------------
            }
            else if (GlobalVarMapMain.FlagLanguage == 2)
            {
                // GlobalVarMapMain.FlagLanguage = true;

                // ------------------------------------------------------------------------------------
                // MAIN

                //TextLanguage.Text = "Русский";
                //Language.ToolTip = "Выбор языка";
                ButtonSettings.ToolTip = "Quraşdırma";
                TextBlockEnvironments.Text = "Vəziyyətin";

                MenuEnvironment.ToolTip = "Taktiki və elektron vəziyyət";
                StackPanelEnvironments.ToolTip = "Taktiki və elektron vəziyyət";

                MenuItem1.Header = "Ətraf mühit 1";
                MenuItem2.Header = "Ətraf mühit 2";
                MenuItem3.Header = "Ətraf mühit 3";

                TabCtrlTasksIt1.ToolTip = "Azimut";
                ADSBTable.ToolTip = "Hava obyektləri masası";

                ButConnectBD.LabelText = "MB";
                ButConnectBD.ToolTip = "MB";
                // Route
                // RRR3
                TabCtrlTasksIt2.ToolTip = "Marşrut";

                // ------------------------------------------------------------------------------------
                // MAP_CONTROL

                // Menu_map
                mapCtrl.TextBlockMenuMap.Text = "Xəritə";
                mapCtrl.MenuMap.ToolTip = "Xəritəni açın / bağlayın / DEM";
                mapCtrl.StackPanelMenuMap.ToolTip = "Xəritəni açın / bağlayın / DEM";
                mapCtrl.MenuMapHeader1.Header = "Xəritəni aç";
                mapCtrl.MenuMapHeader2.Header = "Xəritəni bağla";
                mapCtrl.MenuMapHeader3.Header = "Hündürlük matrisalarını aç";

                // Button +
                mapCtrl.ButtonPlus.ToolTip = "Miqyası böyüt";
                mapCtrl.StackPanelButtonPlus.ToolTip = "Miqyası böyüt";

                // Button -
                mapCtrl.ButtonMinus.ToolTip = "Miqyası azalt";
                mapCtrl.StackPanelButtonMinus.ToolTip = "Miqyası azalt";

                // Button Base
                mapCtrl.ButtonBase.ToolTip = "Əsas miqyaslı";
                mapCtrl.StackPanelButtonBase.ToolTip = "Əsas miqyaslı";

                // Button Center
                mapCtrl.ButtonSenter.ToolTip = "Əsas stansiyanın mərkəzləşdirilməsi";
                mapCtrl.StackPanelButtonSenter.ToolTip = "Əsas stansiyanın mərkəzləşdirilməsi";
                mapCtrl.TextBlockButtonSenter.Text = "MS baza";

                // Button JS
                mapCtrl.ButtonJS.ToolTip = "Maneə stansiyası";
                mapCtrl.StackPanelButtonJS.ToolTip = "Maneə stansiyası";
                mapCtrl.checkBoxJS.Content = " MS";

                // Button SRW_FRF
                // sourse of radio waves(SRW) fixed radio frequency(ies)
                // radio emission sources(RES) fixed radio frequency(ies)
                // ИРИ ФРЧ(фиксированной радиочастоты)
                mapCtrl.ButtonObjects1.ToolTip = "RŞM Fiks olunmuş radiotezlik";
                mapCtrl.StackPanelButtonObjects1.ToolTip = "RŞM Fiks olunmuş radiotezlik";
                mapCtrl.checkBoxObj1.Content = " RŞM FRT";

                // Button SRW_FRF_TD
                // sourse of radio waves(SRW) fixed radio frequency(ies) for target distribution
                // ИРИ ФРЧ(фиксированной радиочастоты) ЦР(для целераспределения)
                mapCtrl.ButtonObjects2.ToolTip = "RŞM Fiks olunmuş radiotezlik üçün Hədəf Bölgüsü";
                mapCtrl.StackPanelButtonObjects2.ToolTip = "RŞM Fiks olunmuş radiotezlik üçün Hədəf Bölgüsü";
                mapCtrl.checkBoxObj2.Content = " RŞM FRT HB";

                // Button SRW_FRF_RS
                // sourse of radio waves(SRW) fixed radio frequency(ies) for radio suppression
                // ИРИ ФРЧ(фиксированной радиочастоты) РП(для радиоподавления)
                mapCtrl.ButtonObjects3.ToolTip = "RŞM Fiks olunmuş radiotezlik üçün radiosusdurma";
                mapCtrl.StackPanelButtonObjects3.ToolTip = "RŞM Fiks olunmuş radiotezlik üçün radiosusdurma";
                mapCtrl.checkBoxObj3.Content = " RŞM FRT RS";

                // Button SRW_STRF
                // sourse of radio waves(SRW) with software tunable RF
                // ИРИ ППРЧ(программно перестраиваемой радиочастоты)
                mapCtrl.ButtonObjects4.ToolTip = "RŞM proqramlaşdırıla bilən tezlik";
                mapCtrl.StackPanelButtonObjects4.ToolTip = "RŞM proqramlaşdırıla bilən tezlik";
                mapCtrl.checkBoxObj4.Content = " RŞM İTTK";

                // Bearing
                mapCtrl.ButtonObjects5.ToolTip = "Rulmanlar";
                mapCtrl.StackPanelButtonObjects5.ToolTip = "Rulmanlar";
                mapCtrl.checkBoxObj5.Content = " Rulmanlar";

                // Aircraft
                mapCtrl.ButtonAirplanes.ToolTip = "Hava obyektləri";
                mapCtrl.StackPanelButtonAirplanes.ToolTip = "Hava obyektləri";
                mapCtrl.checkBoxAirplanes.Content = " HO";

                // Button Antennas
                // Направления антенн
                mapCtrl.ButtonAntennas.ToolTip = "Antena istiqamətləri";
                mapCtrl.StackPanelButtonAntennas.ToolTip = "Antena istiqamətləri";
                mapCtrl.checkBoxAntennas.Content = " Antena";

                // S от базовой СП
                mapCtrl.ButtonSSP.ToolTip = "Stansiyanızdan məsafə";
                //mapCtrl.StackPanelButtonSSP.ToolTip = "Расстояние от своей станции";
                mapCtrl.checkBoxSSP.Content = "Məsafə";

                mapCtrl.Txt10.Text = "Nömrə HО";
                // ------------------------------------------------------------------------------------

                // ----------------------------------------------------------------------------------------
                // Russian в контроле азимута

                languages = AzimutTask.Enums.Languages.Az;
                AzimutTaskCntr.ChangeLanguge(languages);
                // ----------------------------------------------------------------------------------------
            }

            // ******************************************************************* Переход на английский

            // ------------------------------------------------------------------------------------
            // AAA
            // Событие по нажатию кнопки для передачи в контрол карты

            //OnButtonLanguage? .Invoke(this, new EventArgs());
            // ------------------------------------------------------------------------------------

            // ForMap
            // AAA
            // Для изменения флага языка в контроле карты
            mapCtrl.fChangeLanguage((byte)languages);
        }

        // *********************************************************************** Button Language_Choice
    } // Class
} // Namespace