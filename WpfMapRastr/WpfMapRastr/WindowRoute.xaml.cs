﻿using System;
using System.Windows;

namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for WindowRoute.xaml
    /// </summary>
    public partial class WindowRoute : Window
    {
        // Нажатие кнопки Close
        public event EventHandler OnClouse;

        public WindowRoute()
        {
            InitializeComponent();
            this.Closing += WndRoute_Closing;
            // ROUTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Otladka
            // Пробуем установки в контроле маршрута с PropertyGrid
            /*
                        CtrlRoute.PropertyRout = new ClassPropertyRout
                        {
                            NameRoute = "NameFirst",
                            SRoute = 5000,
                            NumbPoints = 101
                        };
            */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ROUTE
        }

        // Closing ************************************************************************
        public void WndRoute_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            OnClouse?.Invoke(this, new EventArgs());

            //GlobalVarMapMain.flRouteWindow = false;
            //mapCtrl.objClassInterfaceMap.flRouteWindow = false;

            // Hide (спрятать) window, but NOT delete
            e.Cancel = true;
            Hide();
        }

        // ************************************************************************ Closing
    } // Class
} // Namespace