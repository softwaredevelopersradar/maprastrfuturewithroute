﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

// Semen

namespace WpfMapRastr
{
    public partial class MainWindow
    {
        // AZIMUTH ************************************************************************************
        // Azimuth*
        // BLOCK FOR AZIMUTH

        // ShowAzimutChange ***************************************************************************
        // Обработка изменения состояния птички отображения азимута около станций

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

            //mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;
            if (AzimutTaskCntr.IsShowValue == true)
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = false;
            else
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = true;

            // !!! Расчет идет в перерисовке азимута
            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
        }

        // *************************************************************************** ShowAzimutChange

        // ChangeItemInTabControl *********************************************************************
        // Переход на вкладку в NabControl задач

        private void OnTabItemChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabControl = sender as TabControl; // e.Source could have been used instead of sender as well
            TabItem item = tabControl.SelectedValue as TabItem;

            // ------------------------------------------------------------------------------------------
            // Azimuth*

            if (item.Name == "TabCtrlTasksIt1")
            {
                mapCtrl.objClassInterfaceMap.flAzimuth = true;
            }
            else
            {
                mapCtrl.objClassInterfaceMap.flAzimuth = false;
            }
            // ------------------------------------------------------------------------------------------
            // Route

            if (item.Name == "TabCtrlTasksIt2")
            {
                mapCtrl.objClassInterfaceMap.flRoute = true;

                //RouteTaskCntr.AddEmptyRowsInit();
                //RouteTaskCntr.AddRouteDG1();

                if (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            }
            else
            {
                mapCtrl.objClassInterfaceMap.flRoute = false;
            }
            // ------------------------------------------------------------------------------------------
        }

        // *********************************************************************** ChangeItemInTabControl

        // !!! См. еще клик мыши на карте

        // ButtonClear **********************************************************************************

        private void ButtonOnClear(object sender, EventArgs e)
        {
            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                objClassFunctionsMain.ClearAzimuth();
            }
        }

        // *********************************************************************************** ButtonClear

        // ButtonExecute *********************************************************************************
        // Выполнить, если координаты источника ввели вручную

        private void ButtonOnExecute(object sender, AzimutTask.Models.ICoord e)
        {
            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                // !!! Здесь нужно занести координаты источника из окошек Таниного контрола
                // ?????????????????????
                //mapCtrl.objClassInterfaceMap.LatSource_Azimuth=
                //mapCtrl.objClassInterfaceMap.LongSource_Azimuth =
                //mapCtrl.objClassInterfaceMap.HSource_Azimuth =
                //int yy = (int)(e as AzimutTask.Models.CoordXY).X;
                //AzimutTask.Models mmm = new AzimutTask.Models();
                //var pp = e as AzimutTask.Models.CoordXY;

                //objClassFunctionsMain.ReadCurrentCoordinatesAzimuth();

                AzimutTask.Models.CoordXY modelXY = new AzimutTask.Models.CoordXY();
                AzimutTask.Models.CoordDegree modelDegree = new AzimutTask.Models.CoordDegree();
                AzimutTask.Models.CoordDdMmSs modelDdMmSs = new AzimutTask.Models.CoordDdMmSs();

                if (e.GetType() == typeof(AzimutTask.Models.CoordXY))
                {
                    //AzimutTask.Models.CoordXY modelXY = e as AzimutTask.Models.CoordXY;
                    modelXY = e as AzimutTask.Models.CoordXY;
                }
                else if (e.GetType() == typeof(AzimutTask.Models.CoordDegree))
                {
                    //AzimutTask.Models.CoordDegree modelDegree = e as AzimutTask.Models.CoordDegree;
                    modelDegree = e as AzimutTask.Models.CoordDegree;
                }
                else if (e.GetType() == typeof(AzimutTask.Models.CoordDdMmSs))
                {
                    //AzimutTask.Models.CoordDdMmSs modelDdMmSs = e as AzimutTask.Models.CoordDdMmSs;
                    modelDdMmSs = e as AzimutTask.Models.CoordDdMmSs;
                }

                objClassFunctionsMain.ReadCurrentCoordinatesAzimuth(modelXY, modelDegree, modelDdMmSs);

                // ----------------------------------------------------------------------------------------
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;

                // !!! Расчет идет в перерисовке азимута
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                // ----------------------------------------------------------------------------------------
            } // if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
        }

        // ******************************************************************************* ButtonExecute

        // ************************************************************************************ AZIMUTH
    } // Class
} // Namespace