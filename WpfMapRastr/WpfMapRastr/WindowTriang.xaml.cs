﻿using System.Windows;

namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for WindowTriang.xaml
    /// </summary>
    public partial class WindowTriang : Window
    {
        public WindowTriang()
        {
            InitializeComponent();

            this.Closing += WndTriang_Closing;
        }

        // Closing ************************************************************************

        private void WndTriang_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int ii = 0;

            GlobalVarMapMain.flTriang = false;

            // Hide (спрятать) window, but NOT delete
            e.Cancel = true;
            Hide();
        }

        // ************************************************************************ Closing
    } // Class
} // Namespace