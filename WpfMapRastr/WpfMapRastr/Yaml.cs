﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapRastrControl;
using YamlDotNet.Serialization;

namespace WpfMapRastr
{
    public partial class MainWindow
    {
        public GlobalVarMap YamlLoad()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader("GlobalVarMap.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var GlobalVarMap = new GlobalVarMap();
            try
            {
                GlobalVarMap = deserializer.Deserialize<GlobalVarMap>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (GlobalVarMap == null)
            {
                GlobalVarMap = GenerateDefaultGlobalVarMap();
                YamlSave(GlobalVarMap);
            }
            return GlobalVarMap;
        }

        private GlobalVarMap GenerateDefaultGlobalVarMap()
        {
            var GlobalVarMap = new GlobalVarMap();
            return GlobalVarMap;
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        public void YamlSave(GlobalVarMap GlobalVarMap)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(GlobalVarMap);

                using (StreamWriter sw = new StreamWriter("GlobalVarMap.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
