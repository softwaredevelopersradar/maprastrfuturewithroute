﻿using ModelsTablesDBLib;
using System.Collections.Generic;
using WpfMapRastrControl;

// Semen

namespace WpfMapRastr
{
    // Функции для работы маршрута с БД
    public partial class MainWindow
    {
        // RecordRouteForBD *********************************************************************************
        // Сделать одну запись i-го маршрута в формате для БД, используя свой класс для i-го маршрута

        public TableRoute RecordRouteForBD(ClassRoute objClassRoute)
        {
            int i = 0;
            // -------------------------------------------------------------------------------------------------
            TableRoute objTableRoute = new TableRoute();
            objTableRoute.ListCoordinates = new List<CoordRoute>();
            // -------------------------------------------------------------------------------------------------
            // Имя маршрута

            objTableRoute.Caption = objClassRoute.Name_Route;
            // -------------------------------------------------------------------------------------------------
            // Массив точек маршрута

            for (i = 0; i < objClassRoute.list_Points_Route.Count; i++)
            {
                CoordRoute objr = new CoordRoute();
                objr.Coordinates = new Coord();
                objr.Coordinates.Latitude = objClassRoute.list_Points_Route[i].Latitude;
                objr.Coordinates.Longitude = objClassRoute.list_Points_Route[i].Longitude;
                objr.Coordinates.Altitude = objClassRoute.list_Points_Route[i].Altitude;
                objTableRoute.ListCoordinates.Add(objr);
            } // FOR (points of route)
            // -------------------------------------------------------------------------------------------------
            return objTableRoute;
            // -------------------------------------------------------------------------------------------------
        }

        // ********************************************************************************* RecordRouteForBD
    } // Class
} // Namespace