﻿using Bearing;
using ClientDataBase;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Windows.Threading;
using WpfMapRastrControl;

// Semen

namespace WpfMapRastr
{
    // Подпрограмма первоначальной загрузки таблиц
    public partial class MainWindow
    {
        // LOAD_TABLES ****************************************************************************************
        private async void LoadTables1()
        {
            // JS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // JS

            // Здесь просто отрисуются все станции, которые получили, если нажата птичка по станциям
            var stations = await clientDB.Tables[NameTable.TableASP].LoadAsync<TableASP>();
            var sectorsRanges = await clientDB.Tables[NameTable.TableSectorsRangesRecon]
                .LoadAsync<TableSectorsRangesRecon>();

            for (int i = 0; i < sectorsRanges.Count; i++)
            {
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_SectorsRangesRecon.Add(sectorsRanges[i]);
            }

           
           


            // Убрать без координат
            if (stations.Count > 0)
            {
                for (int iijjs = (stations.Count - 1); iijjs >= 0; iijjs--)
                {
                    if (
                        (stations[iijjs].Coordinates.Latitude == -1) ||
                        (stations[iijjs].Coordinates.Longitude == -1) ||
                        (stations[iijjs].Coordinates.Latitude == 0) ||
                        (stations[iijjs].Coordinates.Longitude == 0)
                        )
                    {
                        try
                        {
                            stations.Remove(stations[iijjs]);
                        }
                        catch
                        { }
                    }
                }
            }

            if (stations.Count > 0)
            {
                for (int iijjss = (stations.Count - 1); iijjss >= 0; iijjss--)
                {
                    try
                    {
                        stations[iijjss].Coordinates.Latitude = stations[iijjss].Coordinates.Latitude < 0 ? stations[iijjss].Coordinates.Latitude * -1 : stations[iijjss].Coordinates.Latitude;
                        stations[iijjss].Coordinates.Longitude = stations[iijjss].Coordinates.Longitude < 0 ? stations[iijjss].Coordinates.Longitude * -1 : stations[iijjss].Coordinates.Longitude;
                    }
                    catch
                    { }
                }
            }

            int flPel = -1;

            if ((mapCtrl.objClassInterfaceMap.ShowJS == true) && (mapCtrl.objClassInterfaceMap.Show_SRW_STRF_RS == true))
                flPel = 1;

            // Добавляем элементы из входного массива от БД (тип TableASP) в
            // массив классов АСП (List<ClassJS>)
            if (stations.Count > 0)
            {
                for (int iijjs1 = 0; iijjs1 < stations.Count; iijjs1++)
                {
                    objClassFunctionsMain.AddOneJS(stations[iijjs1], flPel);
                }
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> JS

            // ИРИ ФРЧ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            //List<TempFWS> lstTempFWS = new List<TempFWS>();
            var obj1 = await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>();
            //lstTempFWS = obj1;

            // Otl ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ  Otladka

            /*
                                                obj1.Clear();

                                                TempFWS obj;

                                                Random rand = new Random();
                                                Random rand1 = new Random();
                                                int j = 0;
                                                int j1 = 0;
                                                int f = 50;
                                                double lt_temp = 0;
                                                double ln_temp = 0;

                                                for (j1 = 0; j1 < 10; j1++)
                                                {
                                                    for (j = 0; j < 5; j++)
                                                    {
                                                        lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                                                        ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                                                        obj = new TempFWS();
                                                        obj.Coordinates = new Coord();
                                                        obj.Coordinates.Latitude = lt_temp;
                                                        obj.Coordinates.Longitude = ln_temp;
                                                        obj.FreqKHz = f;
                                                        //e.Table.Add(obj);
                                                        obj1.Add(obj);
                                                    }
                                                     if (j1 < 4)
                                                        f += 50;
                                                     else
                                                        f += 250;
                                                }

                                    obj1[0].ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
                                    JamDirect o1 = new JamDirect();
                                    o1.NumberASP = 111;
                                    o1.Bearing = 50;
                                    obj1[0].ListQ.Add(o1);

                                    //for(int ii=0;ii<20;ii++)
                                    //{
                                   //     obj1[ii].Coordinates.Latitude = 56.18;
                                   //     obj1[ii].Coordinates.Longitude = 22.98 + ii * 0.3;
                                   // }

            */
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl

            // ----------------------------------------------------------------- OnTempFWSUp

            // Свой список __________________________________________________________________________
            // Создание своего списка классов ИРИ ФРЧ (в т.ч. для пеленгов)

            // Добавляем элементы из входного массива от БД (тип TempFWS) в
            // массив классов ИРИ ФРЧ (List<Class_IRIFRCH>)
            if (obj1.Count > 0)
            {
                for (int iijfr1 = 0; iijfr1 < obj1.Count; iijfr1++)
                {
                    objClassFunctionsMain.AddOneIRIFRCH(obj1[iijfr1]);
                }
            }

            // QQQ Otladka
            //mapCtrl.objClassInterfaceMap.List_SRW_FRF[0].IsSelected = true;
            // __________________________________________________________________________ Свой список

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ

            // ИРИ ФРЧ ЦР >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj2 = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
            mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD = obj2;

            if (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count > 0)
            {
                for (int iijf1 = (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count - 1); iijf1 >= 0; iijf1--)
                {
                    if (
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Latitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Longitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Latitude == 0) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Longitude == 0)
                        )
                    {
                        try
                        {
                            mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Remove(mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1]);
                        }
                        catch
                        { }
                    }
                }
            }

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ ЦР

            // ИРИ ФРЧ РП >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj3 = await clientDB.Tables[NameTable.TableSuppressFWS].LoadAsync<TableSuppressFWS>();
            mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS = obj3;

            if (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count > 0)
            {
                for (int iijf2 = (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count - 1); iijf2 >= 0; iijf2--)
                {
                    if (
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Latitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Longitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Latitude == 0) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Longitude == 0)
                        )
                    {
                        // 1202
                        try
                        {
                            mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Remove(mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2]);
                        }
                        catch
                        { }
                    }
                }
            }

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ РП

            // ИРИ ППРЧ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj4 = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<TableReconFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon = obj4;
            var obj4_1 = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<TableSourceFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord = obj4_1;

            // Otl1 ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ1  Otladka
            // Для ввода своих ППРЧ просто откомментировать этот кусок otl1
            /*
                                                TableReconFHSS obj;
                                                TableSourceFHSS obj_1;

                                                Random rand = new Random();
                                                Random rand1 = new Random();
                                                int j = 0;
                                                int j1 = 0;
                                                int f = 50;
                                                int num = 0;
                                                double lt_temp = 0;
                                                double ln_temp = 0;

                                                for (j1 = 0; j1 < 10; j1++)

                                                {
                                                    for (j = 0; j < 5; j++)
                                                    {
                                                        lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                                                        ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                                                        obj = new TableReconFHSS();
                                                        obj_1 = new TableSourceFHSS();
                                                        obj.Id = num;
                                                        obj_1.IdFHSS = num;
                                                        obj_1.Coordinates = new Coord();
                                                        obj_1.Coordinates.Latitude = lt_temp;
                                                        obj_1.Coordinates.Longitude = ln_temp;
                                                        obj.FreqMinKHz = f;
                                                        obj.FreqMaxKHz = f + 2;
                                                        obj4.Add(obj);
                                                        obj4_1.Add(obj_1);
                                                        num += 1;
                                                }
                                                     if (j1 < 4)
                                                        f += 50;
                                                    else
                                                        f += 250;
                                                }
            */
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl1

            //obj1[0].ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
            //JamDirect o1 = new JamDirect();
            //o1.NumberASP = 111;
            //o1.Bearing = 50;
            //obj1[0].ListQ.Add(o1);

            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl

            /*
                        if (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count != 0)
                        {
                            for (int iijf3 = (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count - 1); iijf3 >= 0; iijf3--)
                            {
                                int indxf = -1;
                                indxf = mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.FindIndex(x => (x.IdFHSS == mapCtrl.objClassInterfaceMap.List_SRW_STRF[iijf3].Id));
                                if (indxf >= 0)
                                {
                                    if (
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Latitude == -1) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Longitude == -1) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Latitude == 0) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Longitude == 0)
                                        )
                                        mapCtrl.objClassInterfaceMap.List_SRW_STRF.Remove(mapCtrl.objClassInterfaceMap.List_SRW_STRF[iijf3]);
                                }
                            }
                        }
            */

            // Свой список __________________________________________________________________________
            // Создание своего списка классов ИРИ ППРЧ (в т.ч. для пеленгов)

            // Добавляем элементы из входного массива от БД (тип TableReconFHSS+TableSourceFHSS) в
            // массив классов ИРИ ППРЧ (List<Class_IRIPPRCH>)

            if (obj4_1.Count > 0)
            {
                for (int iijfr5 = 0; iijfr5 < obj4_1.Count; iijfr5++)
                {
                    objClassFunctionsMain.AddOneIRIPPRCH(obj4_1[iijfr5], obj4);
                }
            }

            // __________________________________________________________________________ Свой список

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ППРЧ

            // ИРИ ППРЧ РП >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // !!! Не рисуем
            var obj5 = await clientDB.Tables[NameTable.TableSuppressFHSS].LoadAsync<TableSuppressFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS = obj5;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ППРЧ РП

            // Airplanes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            List<TempADSB> listTableADSB = new List<TempADSB>();

            var objAirPl = await clientDB.Tables[NameTable.TempADSB].LoadAsync<TempADSB>();
            listTableADSB = objAirPl;

            // Если нет координат -> удаляем элемент из списка
            //  NEW1
            if (listTableADSB.Count > 0)
            {
                for (int iijl = (listTableADSB.Count - 1); iijl >= 0; iijl--)
                {
                    if (
                        (listTableADSB[iijl].Coordinates.Latitude == -1) ||
                        (listTableADSB[iijl].Coordinates.Longitude == -1) ||
                        (listTableADSB[iijl].Coordinates.Latitude == 0) ||
                        (listTableADSB[iijl].Coordinates.Longitude == 0)
                        )
                        if (listTableADSB.Count > 0)
                        {
                            // 1202
                            try
                            {
                                listTableADSB.Remove(listTableADSB[iijl]);
                            }
                            catch
                            {
                                //MessageBox.Show("Error1");
                            }
                        }
                }
            }
            // ...............................................................

            // NEW1
            GlobalVarMapMain.fl_AirPlane = 0;
            objClassFunctionsMain.GetADSB_DB(listTableADSB, 0);

            DispatchIfNecessary(() =>
            {
                if (GlobalVarMapMain.FlagLanguage == 1)
                    mapCtrl.Txt10.Text = "Number of aircraft:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
                else if (GlobalVarMapMain.FlagLanguage == 0)
                    mapCtrl.Txt10.Text = "Количество ВО:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
                else if (GlobalVarMapMain.FlagLanguage == 2)
                    mapCtrl.Txt10.Text = "HO sayı:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
            });

            // ForArsen
            List<TempADSB> lst1 = new List<TempADSB>();

            for (int ia1 = 0; ia1 < mapCtrl.objClassInterfaceMap.List_AP.Count; ia1++)
            {
                ModelsTablesDBLib.TempADSB ob1 = new TempADSB();

                ob1.Coordinates = new Coord();
                ob1.ICAO = mapCtrl.objClassInterfaceMap.List_AP[ia1].ICAO;
                ob1.Id = mapCtrl.objClassInterfaceMap.List_AP[ia1].Id;
                ob1.Coordinates.Altitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Altitude;
                ob1.Coordinates.Latitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Latitude;
                ob1.Coordinates.Longitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Longitude;
                ob1.Time = mapCtrl.objClassInterfaceMap.List_AP[ia1].Time;
                lst1.Add(ob1);
            }

            // Arsen_Tab
            try
            {
                aDSBControl.AddRange(lst1);
            }
            catch
            {
                //MessageBox.Show("Error2");
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes

            // ROUTES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            int iroute = 0;
            int ipoints = 0;

            // RRR3
            var tabRoutes = await clientDB.Tables[NameTable.TableRoute].LoadAsync<TableRoute>();

            mapCtrl.objClassInterfaceMap.List_Routes.Clear();
            RouteTaskCntr.ClearLst();

            // Otl OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
            // !!! NE UBIRAT

            // Polina
            // !!! Не убирать
            // Запись в БД
            //clientDB?.Tables[NameTable.TableRoute].AddAsync(record); // запись одной записи
            //clientDB?.Tables[NameTable.TableRoute].AddRangeAsync(list); // Запись всего списка
            //clientDB?.Tables[NameTable.TableRoute].ChangeAsync(record); // Изменение записи
            // clientDB?.Tables[NameTable.TableRoute].CLearAsync(); // Очистка таблицы

            /*
                        if (tabRoutes.Count == 0)
                        {
                            // ----------------------------------------------------------------------------------------
                            // Route1

                            TableRoute objTableRoute1 = new TableRoute();
                            objTableRoute1.ListCoordinates = new List<CoordRoute>();

                            objTableRoute1.Caption = "M1";

                            // Point1
                            CoordRoute obj1r = new CoordRoute();
                            obj1r.Coordinates = new Coord();
                            obj1r.Coordinates.Latitude = 55.253189086914063;
                            obj1r.Coordinates.Longitude = 30.195817947387695;
                            objTableRoute1.ListCoordinates.Add(obj1r);

                            // Point2
                            CoordRoute obj2r = new CoordRoute();
                            obj2r.Coordinates = new Coord();
                            obj2r.Coordinates.Latitude = 53.861366271972656;
                            obj2r.Coordinates.Longitude = 31.448259353637695;
                            objTableRoute1.ListCoordinates.Add(obj2r);

                            // Point3
                            CoordRoute obj3r = new CoordRoute();
                            obj3r.Coordinates = new Coord();
                            obj3r.Coordinates.Latitude = 52.30035400390625;
                            obj3r.Coordinates.Longitude = 30.294694900512695;
                            objTableRoute1.ListCoordinates.Add(obj3r);

                            tabRoutes.Add(objTableRoute1);
                            // ----------------------------------------------------------------------------------------
                            // Route2

                            TableRoute objTableRoute2 = new TableRoute();
                            objTableRoute2.ListCoordinates = new List<CoordRoute>();

                            objTableRoute2.Caption = "M2";

                            // Point1
                            CoordRoute obj1r_1 = new CoordRoute();
                            obj1r_1.Coordinates = new Coord();
                            obj1r_1.Coordinates.Latitude = 55.472217559814453;
                            obj1r_1.Coordinates.Longitude = 23.647966384887695;
                            objTableRoute2.ListCoordinates.Add(obj1r_1);

                            // Point2
                            CoordRoute obj2r_1 = new CoordRoute();
                            obj2r_1.Coordinates = new Coord();
                            obj2r_1.Coordinates.Latitude = 55.931312561035156;
                            obj2r_1.Coordinates.Longitude = 26.152849197387695;
                            objTableRoute2.ListCoordinates.Add(obj2r_1);

                            // Point3
                            CoordRoute obj3r_1 = new CoordRoute();
                            obj3r_1.Coordinates = new Coord();
                            obj3r_1.Coordinates.Latitude = 55.590610504150391;
                            obj3r_1.Coordinates.Longitude = 29.152116775512695;
                            objTableRoute2.ListCoordinates.Add(obj3r_1);

                            tabRoutes.Add(objTableRoute2);

                            // ----------------------------------------------------------------------------------------

                            clientDB?.Tables[NameTable.TableRoute].AddRangeAsync(tabRoutes);
                        }  // tabRoutes.Count == 0
            */

            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO Otl

            // Обработка принятых данных >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // RRR3

            // Перепишем в свой лист
            for (iroute = 0; iroute < tabRoutes.Count; iroute++)
            {
                // ---------------------------------------------------------------------------------------
                // i-й маршрут

                ClassRoute objClassRoute = new ClassRoute();
                // Лист точек текущего маршрута
                objClassRoute.list_Points_Route = new List<Objects>();
                // ---------------------------------------------------------------------------------------

                // Имя
                objClassRoute.Name_Route = tabRoutes[iroute].Caption;

                objClassRoute.Length_Route = 0;
                // ---------------------------------------------------------------------------------------
                // Массив точек маршрута

                for (ipoints = 0; ipoints < tabRoutes[iroute].ListCoordinates.Count; ipoints++)
                {
                    // ....................................................................................
                    // j-я точка i-го маршрута
                    Objects objObjects = new Objects();
                    // ....................................................................................
                    objObjects.Latitude = tabRoutes[iroute].ListCoordinates[ipoints].Coordinates.Latitude;
                    objObjects.Longitude = tabRoutes[iroute].ListCoordinates[ipoints].Coordinates.Longitude;
                    objObjects.Altitude = tabRoutes[iroute].ListCoordinates[ipoints].Coordinates.Altitude;
                    // .....................................................................................
                    // Вычислить расстояние между текущей и предыдущей точкой

                    // Точки уже были
                    if (ipoints >= 1)
                    {
                        objObjects.ds = ClassBearing.f_D_2Points(
                                                                  // Предыдущая
                                                                  objClassRoute.list_Points_Route[ipoints - 1].Latitude,
                                                                  objClassRoute.list_Points_Route[ipoints - 1].Longitude,
                                                                  // Координаты текущей
                                                                  objObjects.Latitude,
                                                                  objObjects.Longitude,
                                                                 1);

                        // Наращиваем длину i-го маршрута
                        objClassRoute.Length_Route += objObjects.ds;
                    }
                    // Это 1-я точка
                    else
                    {
                        objObjects.ds = 0;
                    }
                    // ....................................................................................
                    // Добавляем точку к маршруту

                    objClassRoute.list_Points_Route.Add(objObjects);
                    // ....................................................................................
                } // FOR по точкам маршрута
                  // ---------------------------------------------------------------------------------------
                  // Добавляем маршрут в лист маршрутов

                mapCtrl.objClassInterfaceMap.List_Routes.Add(objClassRoute);
                // ---------------------------------------------------------------------------------------
                // Добавить маршрут в DataGrid

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //(RouteTaskCntr.(GlobalRouteParam)dgRoute.DataContext).flfirst = true;

                //RouteTaskCntr.AddEmptyRowsInit();

                // Это будет число занесенных маршрутов, пока примем за число строк при первоначальном занесении
                //((GlobalRouteParam)dgRoute.DataContext).shroutes = ((GlobalRouteParam)dgRoute.DataContext).CollRouteParam.Count;
                //((GlobalRouteParam)dgRoute.DataContext).shroutes = 0;
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                if (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
                {
                    Dispatcher.Invoke(() =>
                     {
                         //RouteTaskCntr.AddRouteDG(
                         //                         objClassRoute.Name_Route,
                         //                         (int)objClassRoute.Length_Route,
                         //                         objClassRoute.list_Points_Route.Count
                         //                         );

                         bool fff = false;
                         if (iroute == 0)
                             fff = false;
                         else
                             fff = true;

                         RouteTaskCntr.AddRouteLst(
                                                  objClassRoute.Name_Route,
                                                  (int)objClassRoute.Length_Route,
                                                  objClassRoute.list_Points_Route.Count,
                                                  fff
                                                  );

                         //RouteTaskCntr.AddRouteDG1();
                     });
                }
                // ---------------------------------------------------------------------------------------
            } // FOR по маршрутам

            if (mapCtrl.objClassInterfaceMap.flRoute == true)
            {
                RouteTaskCntr.AddRouteDG1();
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Обработка принятых данных

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ROUTES

            // Clear and redraw >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Clear and redraw

            // -------------------------------------------------------------------------------------------------
            // Подписка на все остальные события

            // Updated table of ИРИ ФРЧ
            // Для очистки
            (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable += OnTempFWSUp;
            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += OnTempFWSAddRange;

            // Updated table of ИРИ ФРЧ ЦР
            (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable += OnTableReconFWSUp;
            (clientDB.Tables[NameTable.TempSuppressFWS] as ITableUpdate<TempSuppressFWS>).OnUpTable += OnTempSuppressFWS;
            // !!! Не надо
            //(clientDB.Tables[NameTable.TableReconFWS] as ITableAddRange<TableReconFWS>).OnAddRange += OnTableReconFWSAddRange;

            // Updated table of ИРИ ФРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable += OnTableSuppressFWSUp;

            // Updated table of ИРИ ППРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable += OnTableSuppressFHSSUp;

            // ИРИ ППРЧ
            // Для очистки
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += OnTableReconFHSSUp;
            // For Coordinates
            // Для очистки
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable += OnTableSourceFHSSUp;
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange += OnTableReconFHSSAddRange;
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableAddRange<TableSourceFHSS>).OnAddRange += OnTableSourceFHSSAddRange;

            // Updated table of ASP (СП)
            (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable += OnTableAspUp;


            // ??????????????????????????
            // Updated table of AP
            // OLD
            //(clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += OnTempADSB;
            //fill ADSB table by planes + Map
            (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += HandlerUpdateTempADSB;
            (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord += HandlerAddRecordTempADSB;

            //  GPS
            (clientDB.Tables[NameTable.TempGNSS] as ITableUpdate<TempGNSS>).OnUpTable += OnTempGNSSUp;
            // -------------------------------------------------------------------------------------------------
            // ????????????????????????????????????????????   Событий нет?
            //(clientDB.Tables[NameTable.TableRoute]) as ITableUpdate<TableRoute>.
            (clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += OnUpdateSpoof;
            mapCtrl.OnGetCoordSpoof += MapCtrl_OnGetCoordSpoof;
            mapCtrl.OnAddIRIToSuppres += MapCtrl_OnAdIRIToSuppress;
            mapCtrl.DeleteIRISuppress += MapCtrl_OnDeleteIRISuppress;
            
            FirstUpdateSpoof();
            // -------------------------------------------------------------------------------------------------
            GlobalVarMapMain.fltbl = true;
            // -------------------------------------------------------------------------------------------------
        }

        // **************************************************************************************** LOAD_TABLES
    } // Class
} // Namespace