﻿using System;
using System.IO;
using YamlDotNet.Serialization;

namespace WpfMapRastr
{
    public partial class MainWindow
    {
        // NEW !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // T YamlLoad *******************************************************************************

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        /*
                public string YamlLoad(string NameDotYaml)
                {
                    string text = "";
                    try
                    {
                        using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                        {
                            text = sr.ReadToEnd();
                            sr.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    return text;
                }
        */
        // ******************************************************************************* T YamlLoad

        // YamlSave<T> ******************************************************************************

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // ****************************************************************************** YamlSave<T>

        // VARS *************************************************************************************

        public class GlobalVarLanguageYaml
        {
            // Language
            public string lang { get; set; }

            public GlobalVarLanguageYaml()
            {
                //lang = "Rus";
            }
        }

        // ************************************************************************************* VARS

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NEW

        // OLD !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // YamlLoad ******************************************************************************
        /*
                public LocalProperties YamlLoad()
                {
                    string text = "";
                    try
                    {
                        using (StreamReader sr = new StreamReader("LocalProperties.yaml", System.Text.Encoding.Default))
                        {
                            text = sr.ReadToEnd();
                            sr.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    var deserializer = new DeserializerBuilder().Build();

                    var localProperties = new LocalProperties();
                    try
                    {
                        localProperties = deserializer.Deserialize<LocalProperties>(text);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    if (localProperties == null)
                    {
                        localProperties = GenerateDefaultLocalProperties();
                        YamlSave(localProperties);
                    }
                    return localProperties;
                }
        */
        // ****************************************************************************** YamlLoad

        // GenerateDefaultLocalProperties ********************************************************
        /*
                private LocalProperties GenerateDefaultLocalProperties()
                {
                    var localProperties = new LocalProperties();
                    localProperties.ADSB.IpAddress = "192.168.0.11";
                    localProperties.ADSB.Port = 30005;
                    localProperties.DbServer.IpAddress = "192.168.0.102";
                    localProperties.DbServer.Port = 8302;
                    localProperties.DspServer.IpAddress = "192.168.0.102";
                    localProperties.DspServer.Port = 10001;
                    return localProperties;
                }
        */
        // ******************************************************** GenerateDefaultLocalProperties

        // T YamlLoad ****************************************************************************
        /*
                public T YamlLoad<T>(string NameDotYaml) where T : new()
                {
                    string text = "";
                    try
                    {
                        using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                        {
                            text = sr.ReadToEnd();
                            sr.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    var deserializer = new DeserializerBuilder().Build();

                    var t = new T();
                    try
                    {
                        t = deserializer.Deserialize<T>(text);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    //if (t == null)
                    //{
                    //    t = new T();
                    //    YamlSave(t, NameDotYaml);
                    //}
                    return t;
                }
        */
        // **************************************************************************** T YamlLoad

        // YamlSave ******************************************************************************
        /*
                public void YamlSave(LocalProperties localProperties)
                {
                    try
                    {
                        var serializer = new SerializerBuilder().EmitDefaults().Build();
                        var yaml = serializer.Serialize(localProperties);

                        using (StreamWriter sw = new StreamWriter("LocalProperties.yaml", false, System.Text.Encoding.Default))
                        {
                            sw.WriteLine(yaml);
                            sw.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
        */
        // ****************************************************************************** YamlSave

        // YamlSave<T> ***************************************************************************
        /*
                public void YamlSave<T>(T t, string NameDotYaml) where T : new()
                {
                    try
                    {
                        var serializer = new SerializerBuilder().EmitDefaults().Build();
                        var yaml = serializer.Serialize(t);

                        using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                        {
                            sw.WriteLine(yaml);
                            sw.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
        */
        // *************************************************************************** YamlSave<T>

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! OLD
    } //Class
} // Namespace