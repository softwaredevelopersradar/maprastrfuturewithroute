﻿using ADSBControl;
using Bearing;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using GeoCalculator;
using LineOfSightZoneControl;
using LineOfSightZoneControl.Models;
using LosZoneLib;
using Mapsui;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using Mapsui.UI.Wpf;
using ModelsTablesDBLib;
using OSGeo.GDAL;
using ReliefProfileControl;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using WpfControlRouteMap;
using WpfMapControl;
using WpfMapRastrControl;
using WpfRouteControl;

namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        // -------------------------------------------------------------------------------------------
        public WindowAzimuth objWindowAzimuth;

        public WindowTriang objWindowTriang;

        // Окно для контрола с PropertyGrid
        public WindowRoute objWindowRoute;

        public ClassFunctionsMain objClassFunctionsMain;

        public GlobalVarLanguageYaml globalVarLanguageYaml;

        public GlobalProperties global;

        public TrackBall objTrackBall;

        private ZoneControl zoneControl;
        private ViewModel viewModel { get; set; }
        // -------------------------------------------------------------------------------------------

        // Az_птичка >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // Azimuth*
        // Для азимута (нажатие/отжатие птички)

        private bool isShowProp;

        public bool IsShowProp
        {
            get => isShowProp;
            set
            {
                if (value == isShowProp) return;
                isShowProp = value;
                OnPropertyChanged();
            }
        }


        // !!! Обработчик смотри в блоке азимута
        public event PropertyChangedEventHandler PropertyChanged;

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Az_птичка

        // --------------------------------------------------------------------------------------------
        // AAA
        // Для кнопки языка

        //public event EventHandler OnButtonLanguage;
        // --------------------------------------------------------------------------------------------

        // Constructor ********************************************************************************
        public MainWindow()
        {
            // ----------------------------------------------------------------------------------------
            InitializeComponent();
            mapCtrl.MapControl.MapType = MapType.OSM;
            
            // -----------.-----------------------------------------------------------------------------
            // EVENTS MainWindow

            this.Loaded += MainWindow_Loaded;
            this.Closing += MainWindow_Closing;

            //this.Closed += MainWindow_Closed;
            // ----------------------------------------------------------------------------------------
            // Other windows

            objWindowAzimuth = new WindowAzimuth();
            GlobalVarMapMain.objWindowAzimuthG = objWindowAzimuth;

            objWindowTriang = new WindowTriang();
            GlobalVarMapMain.objWindowTriangG = objWindowTriang;

            // Окно для контрола с PropertyGrid
            objWindowRoute = new WindowRoute();
            GlobalVarMapMain.objWindowRouteG = objWindowRoute;

            objClassFunctionsMain = new ClassFunctionsMain();
            mapCtrl.PropertyChanged += MapCtrl_PropertyChanged;

            objTrackBall = new TrackBall();

            viewModel = new ViewModel(GetHeightFunc, ConvertLonLat);

            viewModel.OnPolygonChanged += ViewModel_OnPolygonChanged;
            viewModel.OnAntenna += ViewModel_OnAntenna;
            viewModel.OnAntennaEnemyObj += ViewModel_OnAntennaEnemyObj;
            viewModel.ComboBoxPoints = new ObservableCollection<Point3D_WithName>() { new Point3D_WithName(10, 10, 0, "XY"), new Point3D_WithName(27, 53, 190, "АСП 1"), new Point3D_WithName(10, 10, 0, "АСП 2")};


            // ----------------------------------------------------------------------------------------

            // EVENTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // Click Button of Azimuth task (WpfTasksControl)
            //tasksCtrl.OnTaskAzimuth += ButtonAz;
            //tasksCtrl.OnTaskTriang += ButtonTriang;

            // ........................................................................................
            // Route
            // RRR3

            // Нажатие кнопки в TabControl1/вкладка Маршрут/кнопка Добавить маршрут
            RouteTaskCntr.OnAddRoute += ButtonAddRoute;

            // Нажатие кнопки Closing
            //           WindowRoute windowRoute = new WindowRoute();
            //windowRoute.OnClouse += ButtonClouse;
            objWindowRoute.Closing += ButtonClouse;

            // Нажатие кнопки в TabControl1/вкладка Маршрут/кнопка DEL маршрут
            RouteTaskCntr.OnDelRoute += RouteTaskCntr_OnDelRoute; ;

            // Нажатие птички в CheckBox в TabControl1
            RouteTaskCntr.OnCheckBoxClick += RouteTaskCntr_OnCheckBoxClick; ;

            // But
            // Нажатие кнопки Птичка в контроле2 маршрута с PropertyGrid
            GlobalVarMapMain.objWindowRouteG.CtrlRoute.OnButSaveClick += CtrlRoute_OnButSaveClick;

            // Нажатие кнопки в TabControl1/вкладка Маршрут/кнопка ClearAll
            RouteTaskCntr.OnClearAll += RouteTaskCntr_OnClearAll;

            // ........................................................................................

            // Click Button "Calc" in Triangulation Control
            //objWindowTriang.tasksTriang.OnCalcTriang += ButtonCalcTriang;

            // Click Button "Clear" in Azimuth Control
            //objWindowAzimuth.tasksAzimuth.OnClearAzimuth += ButtonClearAzimuth;
            // Show azimuth title  near station
            //objWindowAzimuth.tasksAzimuth.OnCheckedShowAzimuth += CheckShowAzimuth;
            //objWindowAzimuth.tasksAzimuth.OnUnCheckedShowAzimuth += UnCheckShowAzimuth;

            // Mouse Click On the map
            mapCtrl.OnMouseCl += MapCtrl_OnMouseCl;
            ADSBCleanerEvent.OnCleanADSBTable += ADSBCleanerEvent_OnCleanADSBTable;
            mapCtrl.OnAddRelief += MapCtrl_OnAddRelief1;
            mapCtrl.OnAddLOS += MapCtrl_OnAddLOS;
            //11_13new
            // Двойной клик по строке таблицы самолетов
            aDSBControl.OnSelectedRowADSB += new EventHandler<string>(ADSBControl_OnSelectedRowADSB);
            
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Language
            // Изменение языка в контроле Settings

            SettingsMap.OnLanguageChanged += SettingsMap_OnLanguageChanged;
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EVENTS

            // LANGUAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Language
            // Установка языка проекта

            // !!!
            // Считать язык из Settings
            //var lng = SettingsMap.Settings.Language;
            // Проверка
            //if (Convert.ToString(lng) == "Rus")
            // Установка языка в Settings
            //SettingsMap.Settings = new ClassSettings
            //{
            //    Language = ControlSettingsMap.Languages.Eng
            //};

            // .......................................................................................
            // Считываем текущий язык из файла LanguageSettings.yaml

            // Чтение
            // !!! До конструктора описан
            //GlobalVarLanguageYaml globalVarLanguageYaml = new GlobalVarLanguageYaml();
            //globalVarLanguageYaml = new GlobalVarLanguageYaml();

            //globalVarLanguageYaml.lang= YamlLoad("LanguageSettings.yaml");
            //GlobalVarLanguageYaml globalVarLanguageYaml = YamlLoad<GlobalVarLanguageYaml>("LanguageSettings.yaml");
            globalVarLanguageYaml = YamlLoad<GlobalVarLanguageYaml>("LanguageSettings.yaml");

            // Запись
            //globalVarLanguageYaml.lang = "Rus";
            //YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            // .......................................................................................
            // Головная программа

            GlobalVarMapMain.LNG = globalVarLanguageYaml.lang;

            if (GlobalVarMapMain.LNG == "Eng")
            {
                GlobalVarMapMain.FlagLanguage = 1;
                mapCtrl.objClassInterfaceMap.FlLng = 1;
            }
            else if (GlobalVarMapMain.LNG == "Rus")
            {
                GlobalVarMapMain.FlagLanguage = 0;
                mapCtrl.objClassInterfaceMap.FlLng = 0;
            }
            else if (GlobalVarMapMain.LNG == "Az")
            {
                GlobalVarMapMain.FlagLanguage = 2;
                mapCtrl.objClassInterfaceMap.FlLng = 2;
            }

            ChoiceLanguage();
            // .......................................................................................
            // !!! В контроле Settings язык установим после считывании других переменных из .Yaml файла

            // .......................................................................................
            // Azimuth
            // Установка соответствующего языка в контроле азимута

            if (GlobalVarMapMain.FlagLanguage == 0) // Русский
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Rus;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            else if (GlobalVarMapMain.FlagLanguage == 1)
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Eng;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            else if (GlobalVarMapMain.FlagLanguage == 2)
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Az;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            // .......................................................................................
            // ADSB

            if (GlobalVarMapMain.FlagLanguage == 0) // Русский
            {
                aDSBControl.TextBlockLen1.Text = "Кол-во ВО без координат: ";
                aDSBControl.TextBlockLen2.Text = "Кол-во ВО с координатами: ";
                aDSBControl.ColLen1.Header = "Фл";
                aDSBControl.ColLen2.Header = "Тип";
                aDSBControl.ColLen3.Header = "Шир,°         Долг,°";
                aDSBControl.ColLen5.Header = " Выс, м";
                aDSBControl.TTPrint.Content = "Печать файла";
                aDSBControl.TTSave.Content = "Сохранить файл как Word/Doc";
                aDSBControl.TTDel.Content = "Удалить все";
            }
            else if (GlobalVarMapMain.FlagLanguage == 1)
            {
                aDSBControl.TextBlockLen1.Text = "№ planes w/o coordinates: ";
                aDSBControl.TextBlockLen2.Text = "№ planes with coordinates: ";
                aDSBControl.ColLen1.Header = "Fl";
                aDSBControl.ColLen2.Header = "Type";
                aDSBControl.ColLen3.Header = "Lat,°         Long,°";
                aDSBControl.ColLen5.Header = " Alt, m";
                aDSBControl.TTPrint.Content = "Print file";
                aDSBControl.TTSave.Content = "Save file as Word/Doc";
                aDSBControl.TTDel.Content = "Delete all";
            }
            else if (GlobalVarMapMain.FlagLanguage == 2)
            {
                aDSBControl.TextBlockLen1.Text = "koordinatları olmayan HO sayı: ";
                aDSBControl.TextBlockLen2.Text = "koordinatları olan HO sayı: ";
                aDSBControl.ColLen1.Header = "Fl";
                aDSBControl.ColLen2.Header = "Tip";
                aDSBControl.ColLen3.Header = "Enlik,°         Uzunluq,°";
                aDSBControl.ColLen5.Header = " Hündürlük, м";
                aDSBControl.TTPrint.Content = "Fayl çap et";
                aDSBControl.TTSave.Content = "Faylı Word / Doc olaraq qeyd edin";
                aDSBControl.TTDel.Content = "Hər şeyi sil";
            }
            // .......................................................................................
            // Route -> 1-й контрол, где DataGrid
            // !!! Во 2-ом контроле, где PropertyGrid, установки в конце конструктора

            if (GlobalVarMapMain.FlagLanguage == 0) // Русский
            {
                // RRR3

                RouteTaskCntr.bAdd.ToolTip = "Добавить маршрут";
                //RouteTaskCntr.bChange.ToolTip = "Изменить маршрут";
                RouteTaskCntr.bDelete.ToolTip = "Удалить маршрут";
                RouteTaskCntr.bClear.ToolTip = "Удалить все";
                RouteTaskCntr.DGCBC_Map.Header = "Карта";
                RouteTaskCntr.DGCBC_Title.Header = "Имя маршрута";
                RouteTaskCntr.DGCBC_Length.Header = "Длина маршрута,м";
                RouteTaskCntr.DGCBC_Points.Header = "Число точек";

                GlobalVarMapMain.objWindowRouteG.Title = "Маршрут";
            }
            else if (GlobalVarMapMain.FlagLanguage == 1)
            {
                // RRR3

                RouteTaskCntr.bAdd.ToolTip = "Add route";
                //RouteTaskCntr.bChange.ToolTip = "Change route";
                RouteTaskCntr.bDelete.ToolTip = "Delete route";
                RouteTaskCntr.bClear.ToolTip = "Clear all";
                RouteTaskCntr.DGCBC_Map.Header = "Map";
                RouteTaskCntr.DGCBC_Title.Header = "Title route";
                RouteTaskCntr.DGCBC_Length.Header = "Route length,m";
                RouteTaskCntr.DGCBC_Points.Header = "Number of points";

                GlobalVarMapMain.objWindowRouteG.Title = "Route";
            }
            else if (GlobalVarMapMain.FlagLanguage == 2)
            {
                // RRR3

                RouteTaskCntr.bAdd.ToolTip = "Marşrut əlavə edin";
                //RouteTaskCntr.bChange.ToolTip = "Change route";
                RouteTaskCntr.bDelete.ToolTip = "Marşrutu silin";
                RouteTaskCntr.bClear.ToolTip = "Hər şeyi sil";
                RouteTaskCntr.DGCBC_Map.Header = "Xəritə";
                RouteTaskCntr.DGCBC_Title.Header = "Marşrut adı";
                RouteTaskCntr.DGCBC_Length.Header = "Marşrut uzunluğu,m";
                RouteTaskCntr.DGCBC_Points.Header = "Xal sayı";

                GlobalVarMapMain.objWindowRouteG.Title = "Marşrut";
            }
            // .......................................................................................

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> LANGUAGE

            // YAML >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // From YAML

            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // File .yaml was read --------------------------------------------------------------------

            if (s == "")
            {
                // To Property Grid
                SettingsMap.Settings = new ClassSettings
                {
                    // 0 - DD.dddddd
                    // 1 - DD MM.mmmmmm
                    // 2-  DD MM SS.ssssss
                    FormCoord = (FormatCoordinates)initMapYaml.FormatCoord,
                    IP_DB = initMapYaml.IP_DB,
                    NumPort = initMapYaml.NumPort_DB,
                    // 0 - WGS84degree
                    // 1 - SK42degree
                    // 2 - SK42_XY
                    // 3 - Mercator
                    SysCoord = (SystemCoordinates)initMapYaml.SysCoord,
                    Path = initMapYaml.Path,
                    PathMatr = initMapYaml.PathMatr,
                    // 1 - Gegraphic
                    // 0 - Mercator
                    TypeProjection = (TypesProjection)initMapYaml.TypeMap
                };

                //SettingsMap.
                // ..............................................................................
                // Language in Setting

                if (GlobalVarMapMain.FlagLanguage == 1) // Английский
                    SettingsMap.Settings.Language = ControlSettingsMap.Languages.Eng;
                else if (GlobalVarMapMain.FlagLanguage == 0)
                    SettingsMap.Settings.Language = ControlSettingsMap.Languages.Rus;
                else if (GlobalVarMapMain.FlagLanguage == 2)
                    SettingsMap.Settings.Language = ControlSettingsMap.Languages.Az;
                // ..............................................................................

                // ..............................................................................
                // Azimuth* SysCoord

                AzimutTask.Enums.EnumSysCoord enumSysCoord = new AzimutTask.Enums.EnumSysCoord();
                if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
                    enumSysCoord = AzimutTask.Enums.EnumSysCoord.XY;
                else
                {
                    if (initMapYaml.FormatCoord == 0)
                    {
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.Degree;
                    }
                    else
                    {
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.DdMmSs;
                    }
                } // degree
                AzimutTaskCntr.ChangeSystemCoord(enumSysCoord);
                // ..............................................................................
            }
            // -------------------------------------------------------------------- File .yaml was read

            // File .yaml wasn't read -----------------------------------------------------------------
            else
            {
                if (GlobalVarMapMain.FlagLanguage == 1)
                    MessageBox.Show("Can't open .yaml file");
                else if (GlobalVarMapMain.FlagLanguage == 0)
                    MessageBox.Show("Невозможно открыть .yaml файл");
                else if (GlobalVarMapMain.FlagLanguage == 2)
                    MessageBox.Show(".Yaml faylı aça bilmir");
            }
            // ----------------------------------------------------------------- File .yaml wasn't read

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YAML

            // ----------------------------------------------------------------------------------------
            GlobalVarMapMain.objMainWindowG = this;
            // ----------------------------------------------------------------------------------------
            // Azimuth*

            DataContext = this;
            // ----------------------------------------------------------------------------------------

            // ROUTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Otladka
            // Пробуем установки в контроле маршрута с PropertyGrid
            // !!! 2-й контрол

            GlobalVarMapMain.objWindowRouteG.CtrlRoute.PropertyRout = new ClassPropertyRout
            {
                NameRoute = "NameFirst",
                SRoute = 5000,
                NumbPoints = 101
            };
            // .......................................................................................
            // Route
            // Установка соответствующего языка в контроле маршрута

            if (GlobalVarMapMain.FlagLanguage == 0) // Русский
            {
                WpfControlRouteMap.Enums.Languages lll = WpfControlRouteMap.Enums.Languages.Rus;
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.ChangeLanguge(lll);
            }
            else if (GlobalVarMapMain.FlagLanguage == 1)
            {
                WpfControlRouteMap.Enums.Languages lll = WpfControlRouteMap.Enums.Languages.Eng;
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.ChangeLanguge(lll);
            }
            else if (GlobalVarMapMain.FlagLanguage == 2)
            {
                WpfControlRouteMap.Enums.Languages lll = WpfControlRouteMap.Enums.Languages.Az;
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.ChangeLanguge(lll);
            }
            // .......................................................................................

            // Флаг начала маршрута
            //mapCtrl.objClassInterfaceMap.flStartRoute = false;
            // Лист маршрутов
            //mapCtrl.objClassInterfaceMap.List_Routes = new List<ClassRoute>();

            // ***
            // Очистка коллекции при загрузке программы
            RouteTaskCntr.ClearColl();
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ROUTE
        }

        private void ViewModel_OnAntennaEnemyObj(object sender, double e)
        {
            mapCtrl.antennaEnemyObj = e;
        }

        private void ViewModel_OnAntenna(object sender, Antenna e)
        {
            mapCtrl.antennaJS = e.Height;

        }

        private void MapCtrl_OnAddLOS(object sender, ClassArg e)
        {
            if (this.ZoneControl.DataContext is ViewModel vm) 
            {
                var point = MercatorDependOnMapType.FromLonLat(e.lon, e.lat);

                var centerZone = new Point3DMerkator(point.X, point.Y, e.h);
                if (mapCtrl.objClassInterfaceMap.List_JS.Count == 1) 
                {
                    var AltOwn = (int)mapCtrl.MapControl.DtedMass.GetElevation(mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Longitude, mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Latitude);
                    
                    viewModel.ComboBoxPoints = new ObservableCollection<Point3D_WithName>() { new Point3D_WithName(e.lon, e.lat, (int)e.h, "XY"), new Point3D_WithName(mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Longitude, mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Latitude, AltOwn, "АСП 1"), new Point3D_WithName(0, 0, 0, "АСП 2")};

                } if (mapCtrl.objClassInterfaceMap.List_JS.Count == 2) 
                    {
                    var AltOwn = (int)mapCtrl.MapControl.DtedMass.GetElevation(mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Longitude, mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Latitude);
                    var AltSlave = (int)mapCtrl.MapControl.DtedMass.GetElevation(mapCtrl.objClassInterfaceMap.List_JS[1].Coordinates.Longitude, mapCtrl.objClassInterfaceMap.List_JS[1].Coordinates.Latitude);
                    viewModel.ComboBoxPoints = new ObservableCollection<Point3D_WithName>() { new Point3D_WithName(e.lon, e.lat, (int)e.h, "XY"), new Point3D_WithName(mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Longitude, 
                        mapCtrl.objClassInterfaceMap.List_JS[0].Coordinates.Latitude, AltOwn, "АСП 1"), new Point3D_WithName(mapCtrl.objClassInterfaceMap.List_JS[1].Coordinates.Longitude, 
                        mapCtrl.objClassInterfaceMap.List_JS[1].Coordinates.Latitude, AltSlave, "АСП 2")};
                    }
                
            }
        }


        private void MapCtrl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(mapCtrl.DistanceVisibility))
            {
                ReliefControl.Visibility = mapCtrl.DistanceVisibility ? Visibility.Visible : Visibility.Hidden;
                GrMain2.RowDefinitions[2].Height = new GridLength(250);
                GridSplitter.Visibility= Visibility.Visible;

                if (ReliefControl.Visibility == Visibility.Hidden && this.ReliefControl.DataContext is TrackBallViewModel vm) 
                {
                    GrMain2.RowDefinitions[2].Height = new GridLength(0);
                    GridSplitter.Visibility = Visibility.Collapsed;
                    vm.PropertyChanged -= TrackBallViewModelPropertyChanged;
                }
            }

        }

        private void MapCtrl_OnAddRelief1(object sender, ReliefEventArgs e)
        {
            if (this.ReliefControl.DataContext is TrackBallViewModel vm) 
            {
                vm.PropertyChanged += TrackBallViewModelPropertyChanged;
                var list = new List<ReliefProfileControl.Models.Coordinates>();
                for (int i = 0; i < e.coordinates.Count; i++)
                     {
                        list.Add(new ReliefProfileControl.Models.Coordinates() { Altitude = (int)e.coordinates[i].Alt, Latitude = e.coordinates[i].Lat, Longitude = e.coordinates[i].Lon });

                     }  
                 vm.Model = new ReliefProfileControl.Models.TrackBallModel() {CoordinatesList = list, HeightAntennaJS = e.AntennaJS, HeightAntennaEnemyObj = e.AntennaEnemy };
                 
            }
      
        }
        
        private void TrackBallViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is TrackBallViewModel vm)
            {
                if (e.PropertyName == nameof(vm.MouseSelectedCoordinate))
                {

                    mapCtrl.objClassMapRastrReDraw.DeleteReliefPoint(999);
                    
                    mapCtrl.objClassMapRastrReDraw.ReDrawPointRelief(vm.MouseSelectedCoordinate.Latitude, vm.MouseSelectedCoordinate.Longitude);
                }
            }
           
        }

        // ******************************************************************************** Constructor

        // Change_Language ****************************************************************************
        // Language
        //  Событие от контрола Settings об изменении языка

        private void SettingsMap_OnLanguageChanged(object sender, ControlSettingsMap.Languages e)
        {
            //throw new NotImplementedException();

            // .......................................................................................
            // Azimuth
            // Установка соответствующего языка в контроле азимута

            if (Convert.ToString(e) == "Rus")
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Rus;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            else if (Convert.ToString(e) == "Eng")
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Eng;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            else if (Convert.ToString(e) == "Az")
            {
                AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Az;
                AzimutTaskCntr.ChangeLanguge(languages);
            }
            // .......................................................................................
            // ADSB

            if (Convert.ToString(e) == "Rus")
            {
                aDSBControl.TextBlockLen1.Text = "Кол-во ВО без координат: ";
                aDSBControl.TextBlockLen2.Text = "Кол-во ВО с координатами: ";
                aDSBControl.ColLen1.Header = "Фл";
                aDSBControl.ColLen2.Header = "Тип";
                aDSBControl.ColLen3.Header = "Шир,°         Долг,°";
                aDSBControl.ColLen5.Header = " Выс, м";
                aDSBControl.TTPrint.Content = "Печать файла";
                aDSBControl.TTSave.Content = "Сохранить файл как Word/Doc";
                aDSBControl.TTDel.Content = "Удалить все";
            }
            else if (Convert.ToString(e) == "Eng")
            {
                aDSBControl.TextBlockLen1.Text = "№ planes w/o coordinates: ";
                aDSBControl.TextBlockLen2.Text = "№ planes with coordinates: ";
                aDSBControl.ColLen1.Header = "Fl";
                aDSBControl.ColLen2.Header = "Type";
                aDSBControl.ColLen3.Header = "Alt,°         Long,°";
                aDSBControl.ColLen5.Header = " Alt, м";
                aDSBControl.TTPrint.Content = "Print file";
                aDSBControl.TTSave.Content = "Save file as Word/Doc";
                aDSBControl.TTDel.Content = "Delete all";
            }
            else if (Convert.ToString(e) == "Az")
            {
                aDSBControl.TextBlockLen1.Text = "koordinatları olmayan HO sayı: ";
                aDSBControl.TextBlockLen2.Text = "koordinatları olan HO sayı: ";
                aDSBControl.ColLen1.Header = "Fl";
                aDSBControl.ColLen2.Header = "Tip";
                aDSBControl.ColLen3.Header = "Enlik,°         Uzunluq,°";
                aDSBControl.ColLen5.Header = " Hündürlük, м";
                aDSBControl.TTPrint.Content = "Fayl çap et";
                aDSBControl.TTSave.Content = "Faylı Word / Doc olaraq qeyd edin";
                aDSBControl.TTDel.Content = "Hər şeyi sil";
            }
            // .......................................................................................
            // Route

            if (Convert.ToString(e) == "Rus")
            {
                // RRR3

                RouteTaskCntr.bAdd.ToolTip = "Добавить маршрут";
                //RouteTaskCntr.bChange.ToolTip = "Изменить маршрут";
                RouteTaskCntr.bDelete.ToolTip = "Удалить маршрут";
                RouteTaskCntr.bClear.ToolTip = "Удалить все";
                RouteTaskCntr.DGCBC_Map.Header = "Карта";
                RouteTaskCntr.DGCBC_Title.Header = "Имя маршрута";
                RouteTaskCntr.DGCBC_Length.Header = "Длина маршрута,м";
                RouteTaskCntr.DGCBC_Points.Header = "Число точек";

                GlobalVarMapMain.objWindowRouteG.Title = "Маршрут";
            }
            else if (Convert.ToString(e) == "Eng")
            {
                // RRR3

                RouteTaskCntr.bAdd.ToolTip = "Add route";
                //RouteTaskCntr.bChange.ToolTip = "Change route";
                RouteTaskCntr.bDelete.ToolTip = "Delete route";
                RouteTaskCntr.bClear.ToolTip = "Clear all";
                RouteTaskCntr.DGCBC_Map.Header = "Map";
                RouteTaskCntr.DGCBC_Title.Header = "Title route";
                RouteTaskCntr.DGCBC_Length.Header = "Route length,m";
                RouteTaskCntr.DGCBC_Points.Header = "Number of points";

                GlobalVarMapMain.objWindowRouteG.Title = "Route";
            }
            else if (Convert.ToString(e) == "Az")
            {
                // RRR3

                RouteTaskCntr.bAdd.ToolTip = "Marşrut əlavə edin";
                //RouteTaskCntr.bChange.ToolTip = "Change route";
                RouteTaskCntr.bDelete.ToolTip = "Marşrutu silin";
                RouteTaskCntr.bClear.ToolTip = "Hər şeyi sil";
                RouteTaskCntr.DGCBC_Map.Header = "Xəritə";
                RouteTaskCntr.DGCBC_Title.Header = "Marşrut adı";
                RouteTaskCntr.DGCBC_Length.Header = "Marşrut uzunluğu,m";
                RouteTaskCntr.DGCBC_Points.Header = "Xal sayı";

                GlobalVarMapMain.objWindowRouteG.Title = "Marşrut";
            }
            // ROUTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // !!! 2-й контрол
            // Установка соответствующего языка в контроле маршрута

            if (Convert.ToString(e) == "Rus")
            {
                WpfControlRouteMap.Enums.Languages lll = WpfControlRouteMap.Enums.Languages.Rus;
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.ChangeLanguge(lll);
            }
            else if (Convert.ToString(e) == "Eng")
            {
                WpfControlRouteMap.Enums.Languages lll = WpfControlRouteMap.Enums.Languages.Eng;
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.ChangeLanguge(lll);
            }
            else if (Convert.ToString(e) == "Az")
            {
                WpfControlRouteMap.Enums.Languages lll = WpfControlRouteMap.Enums.Languages.Az;
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.ChangeLanguge(lll);
            }
            // .......................................................................................

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ROUTE

            // .......................................................................................
            // Вся программа

            if (Convert.ToString(e) == "Rus")
            {
                GlobalVarMapMain.FlagLanguage = 0;
                mapCtrl.objClassInterfaceMap.FlLng = 0;
            }
            else if (Convert.ToString(e) == "Eng")
            {
                GlobalVarMapMain.FlagLanguage = 1;
                mapCtrl.objClassInterfaceMap.FlLng = 1;
            }
            else if (Convert.ToString(e) == "Az")
            {
                GlobalVarMapMain.FlagLanguage = 2;
                mapCtrl.objClassInterfaceMap.FlLng = 2;
            }

            ChoiceLanguage();
            // .......................................................................................
            // Записать в файл LanguageSettings.yaml

            // Запись
            //globalVarLanguageYaml.lang = "Rus";
            //YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");

            if (Convert.ToString(e) == "Rus")
            {
                globalVarLanguageYaml.lang = "Rus";
                YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            }
            else if (Convert.ToString(e) == "Eng")
            {
                globalVarLanguageYaml.lang = "Eng";
                YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            }
            else if (Convert.ToString(e) == "Az")
            {
                globalVarLanguageYaml.lang = "Az";
                YamlSave<GlobalVarLanguageYaml>(globalVarLanguageYaml, "LanguageSettings.yaml");
            }
            // .......................................................................................
        }

        // **************************************************************************** Change_Language

        // DoubleClickADSB ****************************************************************************
        // Обработчик двойного клика мыши на строке таблицы ADSB

        private void ADSBControl_OnSelectedRowADSB(object sender, string e)
        {
            // НЕ пустая строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (e != "")
            {
                // ----------------------------------------------------------------------------------------
                // Выделение оранжевым либо сброс обратно в серый

                int index = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.ICAO == e));

                if (index >= 0)
                {
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Клик на НЕвыделенной строке

                    if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 0)
                    {
                        //.................................................................................
                        if (
                             (GlobalVarMapMain.flairone == false) // Нет выделенных строк
                           )
                        {
                            mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                            //&&&
                            GlobalVarMapMain.flairone = true;
                            GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;
                        }
                        //.................................................................................
                        // Уже была выделенная строка
                        else
                        {
                            // Старую снимаем
                            int index2 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));
                            if (index2 >= 0)
                            {
                                mapCtrl.objClassInterfaceMap.List_AP[index2].flColor = 0;
                                if (mapCtrl.objClassInterfaceMap.List_AP[index2].ICAO == GlobalVarMapMain.ICAO_LAST)
                                {
                                    GlobalVarMapMain.flairone = false;
                                    GlobalVarMapMain.ICAO_LAST = "";
                                }
                            } // index2>=0

                            // Новую устанавливаем
                            mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                            GlobalVarMapMain.flairone = true;
                            GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;
                        } // Уже была выделенная строка
                        //.................................................................................
                    } // Клик на НЕвыделенной строке
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Повторный клик для сброса в серый
                    else
                    {
                        mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 0;
                        //&&&
                        if (mapCtrl.objClassInterfaceMap.List_AP[index].ICAO == GlobalVarMapMain.ICAO_LAST)
                        {
                            GlobalVarMapMain.flairone = false;
                            GlobalVarMapMain.ICAO_LAST = "";
                        }
                    }
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

                    // ----------------------------------------------------------------------------------------
                    // Центровка

                    if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 1)
                    {
                        // ....................................................................................
                        bool bvar = false;
                        double x = 0;
                        double y = 0;
                        double lat = 0;
                        double lon = 0;
                        // ....................................................................................
                        lat = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude;
                        lon = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude;
                        // ....................................................................................
                        var p = SphericalMercator.FromLonLat(lon, lat);
                        x = p.X;
                        y = p.Y;
                        // ....................................................................................
                        // MercatorMap or GeographicMap?

                        string s = "";
                        InitMapYaml initMapYaml = new InitMapYaml();
                        s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                        // ....................................................................................
                        // MercatorMap

                        if (initMapYaml.TypeMap == 0)
                        {
                            // Center map on pozition XY (Mercator)
                            bvar = mapCtrl.objClassInterfaceMap.CenterMapToXY(x, y);
                        } // MercatorMap
                          // ....................................................................................
                          // GeographicMap
                        else
                        {
                            // Center map on pozition LatLong (degree)
                            bvar = mapCtrl.objClassInterfaceMap.CenterMapToLatLong(lat, lon);
                        } // GeographicMap
                          // ....................................................................................
                    } // Выделили самолет оранжевым
                      // ----------------------------------------------------------------------------------------
                      // Clear and redraw

                    Dispatcher.Invoke(() =>
                    {
                        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    });
                    // ----------------------------------------------------------------------------------------
                } // index>=0
            } // НЕ пустая строка
              // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! НЕ пустая строка

            // ПУСТАЯ строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            else
            {
                int index1 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));

                if (index1 >= 0)
                {
                    mapCtrl.objClassInterfaceMap.List_AP[index1].flColor = 0;
                    //&&&
                    if (mapCtrl.objClassInterfaceMap.List_AP[index1].ICAO == GlobalVarMapMain.ICAO_LAST)
                    {
                        GlobalVarMapMain.flairone = false;
                        GlobalVarMapMain.ICAO_LAST = "";
                    }

                    // Clear and redraw
                    Dispatcher.Invoke(() =>
                    {
                        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    });
                } // index1>=0
            } // ПУСТАЯ строка
              // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ПУСТАЯ строка
        }

        // **************************************************************************** DoubleClickADSB

        // PropertyGridSettings ***********************************************************************
        /// <summary>
        /// New settings from control SettingsMap
        /// </summary>
        public void UpdateSettings(object sender, ControlSettingsMap.ClassSettings settings)
        {
            try
            {
                // ------------------------------------------------------------------------------------
                // Write to YAML

                string s = "";

                InitMapYaml initMapYaml = new InitMapYaml();
                s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // ...........................................................................
                // File .yaml was read

                if (s == "")
                {
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // If Map type was changed, close map and open new map
                    // 999

                    if (initMapYaml.TypeMap != ((byte)SettingsMap.Settings.TypeProjection))
                    {
                        mapCtrl.CloseMap();
                        mapCtrl.CloseMatrix();
                        mapCtrl.OpenMap(SettingsMap.Settings.Path + "\\1_1-1.tif");
                        //mapCtrl.OpenMatrix(SettingsMap.Settings.Path + "\\");
                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    initMapYaml.IP_DB = SettingsMap.Settings.IP_DB;
                    initMapYaml.NumPort_DB = SettingsMap.Settings.NumPort;
                    initMapYaml.SysCoord = (byte)SettingsMap.Settings.SysCoord;
                    initMapYaml.FormatCoord = (byte)SettingsMap.Settings.FormCoord;
                    //initMapYaml.Path = SettingsMap.Settings.Path+"\\1_1-1.tif";
                    initMapYaml.Path = SettingsMap.Settings.Path;
                    initMapYaml.PathMatr = SettingsMap.Settings.PathMatr;
                    initMapYaml.TypeMap = (byte)SettingsMap.Settings.TypeProjection;

                    // ..............................................................................
                    // Azimuth* SysCoord

                    AzimutTask.Enums.EnumSysCoord enumSysCoord = new AzimutTask.Enums.EnumSysCoord();
                    if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.XY;
                    else
                    {
                        if (initMapYaml.FormatCoord == 0)
                        {
                            enumSysCoord = AzimutTask.Enums.EnumSysCoord.Degree;
                        }
                        else
                        {
                            enumSysCoord = AzimutTask.Enums.EnumSysCoord.DdMmSs;
                        }
                    } // degree
                    AzimutTaskCntr.ChangeSystemCoord(enumSysCoord);

                    // ..............................................................................

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // 19.07 GNSS
                    // Change SK in GNSS

                    long ichislo1 = 0;
                    double dchislo1 = 0;
                    long ichislo2 = 0;
                    double dchislo2 = 0;
                    double xxx = 0;
                    double yyy = 0;

                    if ((GlobalVarMapMain.GNSS_Lat != -1) && (GlobalVarMapMain.GNSS_Long != -1))
                    {
                        // WGS 84
                        if (initMapYaml.SysCoord == 0)
                        {
                            // Latitude
                            ichislo1 = (long)(GlobalVarMapMain.GNSS_Lat * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(GlobalVarMapMain.GNSS_Long * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;

                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " +
                                                         "W " + Convert.ToString(dchislo2) + '\xB0' +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                            });
                        } // WGS84

                        // SK42,m
                        else if (initMapYaml.SysCoord == 2)
                        {
                            ClassGeoCalculator.f_WGS84_Krug(GlobalVarMapMain.GNSS_Lat,
                                                            GlobalVarMapMain.GNSS_Long,
                                                            ref xxx, ref yyy);
                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx) + " m" + "   " + "Y " + Convert.ToString((int)yyy) + " m" +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                            });
                        } // SK42,m

                        // SK42grad
                        else if (initMapYaml.SysCoord == 1)
                        {
                            double xxx1 = 0;
                            double yyy1 = 0;

                            ClassGeoCalculator.f_WGS84_SK42_BL(GlobalVarMapMain.GNSS_Lat,
                                                               GlobalVarMapMain.GNSS_Long,
                                                               ref xxx1, ref yyy1);

                            // Latitude
                            ichislo1 = (long)(xxx1 * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(yyy1 * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;

                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " +
                                                         "W " + Convert.ToString(dchislo2) + '\xB0' +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " +
                                                         GlobalVarMapMain.GNSS_time;
                            });
                        } //SK42grad

                        // Mercator,m
                        else if (initMapYaml.SysCoord == 3)
                        {
                            double xxx2 = 0;
                            double yyy2 = 0;

                            ClassGeoCalculator.f_WGS84_Mercator(GlobalVarMapMain.GNSS_Lat,
                                                                GlobalVarMapMain.GNSS_Long,
                                                                ref xxx2, ref yyy2);

                            DispatchIfNecessary(() =>
                            {
                                mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx2) + " m" + "   " + "Y " + Convert.ToString((int)yyy2) + " m" +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                            });
                        } // Mercator,m
                    } // Lat,Long!=-1
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      // Пишем класс обратно в файл

                    try
                    {
                        ClassYaml.WriteYaml(
                                            "Setting.yaml",
                                            initMapYaml
                                           );

                        // Change SystemCoordinates On the Map (Отображение внизу карты)
                        mapCtrl.DrawCurrentCoordinates();

                        // Azimuth*
                        // !!! Здесь нужно занести координаты источника в окошки Таниного контрола
                        objClassFunctionsMain.DrawCurrentCoordinatesAzimuth();
                    }
                    catch
                    {
                        if (GlobalVarMapMain.FlagLanguage == 1)
                            MessageBox.Show("Can't write .yaml file");
                        else if (GlobalVarMapMain.FlagLanguage == 0)
                            MessageBox.Show("Невозможно записать .yaml файл");
                        else if (GlobalVarMapMain.FlagLanguage == 2)
                            MessageBox.Show(".Yaml faylı aça bilmir");
                    }
                } // IF( Файл .yaml was read)
                  // ...........................................................................
                  // File .yaml wasn't read
                else
                {
                    if (GlobalVarMapMain.FlagLanguage == 1)
                        MessageBox.Show("Can't read .yaml file");
                    else if (GlobalVarMapMain.FlagLanguage == 0)
                        MessageBox.Show("Невозможно прочитать .yaml файл");
                    else if (GlobalVarMapMain.FlagLanguage == 2)
                        MessageBox.Show(".Yaml faylı oxunmur");
                } // ELSE (// File .yaml wasn't read)
                  // ...........................................................................

                // ------------------------------------------------------------------------------------
            } // try get data from PropertyGrid
            catch
            {
                if (GlobalVarMapMain.FlagLanguage == 1)
                    MessageBox.Show("Can't get data");
                else if (GlobalVarMapMain.FlagLanguage == 0)
                    MessageBox.Show("Невозможно получить данные");
                else if (GlobalVarMapMain.FlagLanguage == 2)
                    MessageBox.Show("Məlumat əldə etmək olmur");
            }
        }

        // *********************************************************************** PropertyGridSettings

        // Mouse Click On the map *********************************************************************

        private void MapCtrl_OnMouseCl(object sender, ClassArg e)
        {
            // All_Part *******************************************************************************
            long ichislo = 0;
            double dchislo = 0;

            //  WGS84
            GlobalVarMapMain.LAT_Rastr = e.lat;
            if (GlobalVarMapMain.LAT_Rastr < -90)
                GlobalVarMapMain.LAT_Rastr = -90;
            if (GlobalVarMapMain.LAT_Rastr > 90)
                GlobalVarMapMain.LAT_Rastr = 90;

            GlobalVarMapMain.LONG_Rastr = e.lon;
            if (GlobalVarMapMain.LONG_Rastr < -180)
                GlobalVarMapMain.LONG_Rastr = -180;
            if (GlobalVarMapMain.LONG_Rastr > 180)
                GlobalVarMapMain.LONG_Rastr = 180;

            GlobalVarMapMain.H_Rastr = e.h;

            // Mercator
            GlobalVarMapMain.X_Rastr = e.x;
            GlobalVarMapMain.Y_Rastr = e.y;
            // MGRS
            GlobalVarMapMain.Str_Rastr = e.str;

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sall = "";
            // ******************************************************************************* All_Part

            // Triang *********************************************************************************

            if (GlobalVarMapMain.flTriang == true)
            {
                // ..................................................................................
                // Pel1

                if ((GlobalVarMapMain.flPeleng1 == false) && (GlobalVarMapMain.flPeleng2 == false))
                {
                    GlobalVarMapMain.flPeleng1 = true;

                    // Latitude
                    ichislo = (long)(e.lat * 1000000);
                    dchislo = ((double)ichislo) / 1000000;

                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox1.Text = "N " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LatPel1 = dchislo;

                    // Longitude
                    ichislo = (long)(e.lon * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox2.Text = "W " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LonPel1 = dchislo;

                    sall = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.LAT_Rastr,
                                                 GlobalVarMapMain.LONG_Rastr,
                                                 // Path to image
                                                 dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );
                } // (GlobalVarMapMain.flPeleng1==false)&&(GlobalVarMapMain.flPeleng1 == false)
                  // ..................................................................................
                  // Pel2
                else if ((GlobalVarMapMain.flPeleng1 == true) && (GlobalVarMapMain.flPeleng2 == false))
                {
                    GlobalVarMapMain.flPeleng2 = true;

                    // Latitude
                    ichislo = (long)(e.lat * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox3.Text = "N " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LatPel2 = dchislo;

                    // Longitude
                    ichislo = (long)(e.lon * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    DispatchIfNecessary(() =>
                    {
                        objWindowTriang.tasksTriang.textBox4.Text = "W " + Convert.ToString(dchislo) + '\xB0';
                    });
                    GlobalVarMapMain.LonPel2 = dchislo;

                    sall = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.LAT_Rastr,
                                                 GlobalVarMapMain.LONG_Rastr,
                                                 // Path to image
                                                 dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Otl
                    // !!! Проверка функции расстояния между точками с учетом кривизны Земли
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                } // (GlobalVarMapMain.flPeleng1==true)&&(GlobalVarMapMain.flPeleng1 == false)
                  // ..................................................................................
                else
                {
                    if (GlobalVarMapMain.FlagLanguage == 1)
                        MessageBox.Show("Coordinates are entered");
                    else if (GlobalVarMapMain.FlagLanguage == 0)
                        MessageBox.Show("Координаты введены");
                    else if (GlobalVarMapMain.FlagLanguage == 2)
                        MessageBox.Show("Koordinatlar daxil edildi");
                    return;
                }
                // ..................................................................................
            } // GlobalVarMapMain.flTriang == true
              // ******************************************************************************* Triang

            // Azimuth ********************************************************************************
            // Azimuth*

            if (
                (mapCtrl.objClassInterfaceMap.flAzimuth == true) &&
                (mapCtrl.objClassInterfaceMap.ShowJS == true) &&
                //(mapCtrl.objClassInterfaceMap.Show_S==false)
                (mapCtrl.objClassInterfaceMap.FlMouse == false) // Нажата левая кнопка мыши
               )
            {
                // Source coordinates
                mapCtrl.objClassInterfaceMap.LatSource_Azimuth = GlobalVarMapMain.LAT_Rastr;
                mapCtrl.objClassInterfaceMap.LongSource_Azimuth = GlobalVarMapMain.LONG_Rastr;
                mapCtrl.objClassInterfaceMap.HSource_Azimuth = GlobalVarMapMain.H_Rastr;

                // !!! Здесь нужно занести координаты источника в окошки Таниного контрола
                objClassFunctionsMain.DrawCurrentCoordinatesAzimuth();

                // .......................................................................................
                // Azimuth
                // Установка соответствующего языка в контроле азимута

                if (GlobalVarMapMain.FlagLanguage == 0) // Русский
                {
                    AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Rus;
                    AzimutTaskCntr.ChangeLanguge(languages);
                }
                else if (GlobalVarMapMain.FlagLanguage == 1)
                {
                    AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Eng;
                    AzimutTaskCntr.ChangeLanguge(languages);
                }
                else if (GlobalVarMapMain.FlagLanguage == 2)
                {
                    AzimutTask.Enums.Languages languages = AzimutTask.Enums.Languages.Az;
                    AzimutTaskCntr.ChangeLanguge(languages);
                }
                // .......................................................................................

                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;

                // !!! Расчет идет в перерисовке азимута
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            }
            // ------------------------------------------------------------------------------------------

            /*

                        if (GlobalVarMapMain.flAzimuth == true)
                        {
                        // ----------------------------------------------------------------------------------------
                         objClassFunctionsMain.ClearAzimuth();
                        // ----------------------------------------------------------------------------------------
                        // Latitude
                        ichislo = (long)(e.lat * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt1.Text = "N " + Convert.ToString(dchislo) + '\xB0';

                        // Longitude
                        ichislo = (long)(e.lon * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt3.Text = "W " + Convert.ToString(dchislo) + '\xB0';

                        // Altitude
                        ichislo = (long)(e.h * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt5.Text = Convert.ToString(dchislo) + " m";

                        objWindowAzimuth.tasksAzimuth.Txt7.Text = string.Format("    MGRS: {0}", e.str);
                        // ----------------------------------------------------------------------------------------

                         // ----------------------------------------------------------------------------------------

                            double az = 0;
                            string s3 = "";

                            for (int i = 0; i < mapCtrl.objClassInterfaceMap.List_JS.Count; i++)
                            {
                                az = mapCtrl.CalcAzimuth(mapCtrl.objClassInterfaceMap.List_JS[i].Coordinates.Latitude, mapCtrl.objClassInterfaceMap.List_JS[i].Coordinates.Longitude,
                                                         GlobalVarMapMain.LAT_Rastr, GlobalVarMapMain.LONG_Rastr);

                                ichislo = (long)(az * 1000);
                                dchislo = ((double)ichislo) / 1000;
                                az = dchislo;
                                GlobalVarMapMain.listAzimuth.Add(az);
                            } // for

                            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                                         GlobalVarMapMain.flAzimuth,
                                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                                         GlobalVarMapMain.LAT_Rastr,
                                                                         GlobalVarMapMain.LONG_Rastr,
                                                                         GlobalVarMapMain.listAzimuth
                                                                         );

                            // ----------------------------------------------------------------------------------------
                        } // GlobalVarMapMain.flAzimuth == true
            */
            // ******************************************************************************** Azimuth

            // ROUTE **********************************************************************************
            // Прокладывание текущего маршрута

            //GlobalVarMapMain.objWindowRouteG.CtrlRoute.PropertyRout = new ClassPropertyRout
            //{
            //    NameRoute = "NameFirst",
            //    SRoute = 5000,
            //    NumbPoints = 101
            //};

            // Открыта вкладка маршрут в таблице задач
            // Открыто окно для ввода маршрута
            if (
                (mapCtrl.objClassInterfaceMap.flRoute == true) &&
                (mapCtrl.objClassInterfaceMap.flRouteWindow == true) &&
                //(mapCtrl.objClassInterfaceMap.Show_S==false)
                (mapCtrl.objClassInterfaceMap.FlMouse == false) // Нажата левая кнопка мыши

                )
            {
                // Нет соединения с БД
                if (GlobalVarMapMain.fltbl == false)
                {
                    if (GlobalVarMapMain.FlagLanguage == 1)
                        MessageBox.Show("No DB connection");
                    else if (GlobalVarMapMain.FlagLanguage == 0)
                        MessageBox.Show("Нет соединения с БД");
                    else
                        MessageBox.Show("İlə əlaqə yoxdur VB");

                    return;
                }

                /*
                                // Otl OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

                                double lt1 = 0;
                                double lng1 = 0;
                                double lt2 = 0;
                                double lng2 = 0;
                                double s1 = 0;
                                double s2 = 0;
                                uint nmb = 0;
                                double Theta = 0;

                                // Pelengator
                                lt1 = 53.516551;
                                lng1 = 27.700927;

                                //s1 = 200000;
                                //Theta = 0;
                                //nmb = 10000;
                                s1 = 1000000;
                                Theta = 135;
                                nmb = 100000;

                                ClassBearing.f_Bearing(
                                                      Theta,
                                                      s1,
                                                      nmb,
                                                      lt1,
                                                      lng1,
                                                      1,
                                                      ref lt2,
                                                      ref lng2
                                                      );

                                s2 = ClassBearing.f_D_2Points(
                                                                         // Предыдущая
                                                                         lt1,
                                                                         lng1,
                                                                         // Координаты текущей
                                                                         lt2,
                                                                         lng2,
                                                                         1);

                                s2 = s2;
                                // Otl OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
                */
                // ----------------------------------------------------------------------------------------
                // Начало маршрута

                if (mapCtrl.objClassInterfaceMap.flStartRoute == false)
                {
                    // Начало i-го маршрута
                    mapCtrl.objClassInterfaceMap.flStartRoute = true;

                    // i-й маршрут
                    ClassRoute objClassRoute = new ClassRoute();
                    // Лист точек текущего маршрута
                    objClassRoute.list_Points_Route = new List<Objects>();
                    // Длина маршрута
                    objClassRoute.Length_Route = 0;

                    // Добавляем маршрут (пока пустой) в лист маршрутов
                    mapCtrl.objClassInterfaceMap.List_Routes.Add(objClassRoute);
                }
                // ----------------------------------------------------------------------------------------
                // Новая точка маршрута

                Objects objObjects = new Objects();

                objObjects.Latitude = GlobalVarMapMain.LAT_Rastr;
                objObjects.Longitude = GlobalVarMapMain.LONG_Rastr;
                objObjects.Altitude = GlobalVarMapMain.H_Rastr;
                // ----------------------------------------------------------------------------------------
                // Вычислить расстояние между текущей и предыдущей точкой

                if (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
                {
                    // ....................................................................................
                    // Точки уже были

                    if (mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].list_Points_Route.Count >= 1)
                    {
                        objObjects.ds = ClassBearing.f_D_2Points(
                                                                 // Предыдущая
                                                                 mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                                                                 list_Points_Route[mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].list_Points_Route.Count - 1].Latitude,
                                                                 mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                                                                 list_Points_Route[mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].list_Points_Route.Count - 1].Longitude,
                                                                 // Координаты текущей
                                                                 GlobalVarMapMain.LAT_Rastr,
                                                                 GlobalVarMapMain.LONG_Rastr,
                                                                 1);

                        // Наращиваем длину i-го маршрута
                        mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].Length_Route += objObjects.ds;
                    }
                    // ....................................................................................
                    // Это 1-я точка
                    else
                    {
                        objObjects.ds = 0;
                    }
                    // ....................................................................................
                    // Добавляем точку к маршруту

                    mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                    list_Points_Route.Add(objObjects);
                    // ....................................................................................
                } // Число маршрутов !=0
                  // ----------------------------------------------------------------------------------------
                  // !!! Здесь нужно занести данные в окошки контрола2 (с PropertyGrid)

                // ??????????????
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.PropertyRout.SRoute =
                (double)((int)mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].Length_Route);

                GlobalVarMapMain.objWindowRouteG.CtrlRoute.PropertyRout.NumbPoints =
                mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].list_Points_Route.Count;
                // ----------------------------------------------------------------------------------------
                // Отрисовка

                Dispatcher.Invoke(() =>
                {
                    if (mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                    list_Points_Route.Count > 1)
                    {
                        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    }
                });

                // ----------------------------------------------------------------------------------------
            } // if (mapCtrl.objClassInterfaceMap.flRoute == true)

            // ********************************************************************************** ROUTE

            // S **************************************************************************************
            // Расстояние от объекта, выбранного кликом мыши на карте, до своей станции

            /*
                        if (
                            (mapCtrl.objClassInterfaceMap.ShowJS == true) &&
                            (mapCtrl.objClassInterfaceMap.Show_S == true)&&
                            (mapCtrl.objClassInterfaceMap.FlMouse == true) // Нажата правая кнопка мыши
                           )
                        {
                            // .....................................................................................
                            // Source coordinates

                            mapCtrl.objClassInterfaceMap.LatSource_S = GlobalVarMapMain.LAT_Rastr;
                            mapCtrl.objClassInterfaceMap.LongSource_S = GlobalVarMapMain.LONG_Rastr;
                            mapCtrl.objClassInterfaceMap.HSource_S = GlobalVarMapMain.H_Rastr;
                            // .....................................................................................
                            // !!! Расчет идет в перерисовке

                            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            // .....................................................................................
                        }  // Show_S==true && ShowJS==true
            */
            // ************************************************************************************** S
        }

        // ********************************************************************* Mouse Click On the map

        // СобытиеПоНажатиюКнопкиПтичкаВКонтроле2RouteСPropertyGrid ************************************
        // Добавить маршрут

        private void CtrlRoute_OnButSaveClick(object sender, EventArgs e)
        {
            if (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
            {
                // RRR3

                // ----------------------------------------------------------------------------------------
                // Занести имя маршрута

                mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].Name_Route =
                GlobalVarMapMain.objWindowRouteG.CtrlRoute.PropertyRout.NameRoute;
                // ----------------------------------------------------------------------------------------
                // Добавить маршрут в DataGrid

                Dispatcher.Invoke(() =>
                 {
                     //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (System.Threading.ThreadStart)delegate ()
                     //{
                     RouteTaskCntr.AddRouteDG(
                                           GlobalVarMapMain.objWindowRouteG.CtrlRoute.PropertyRout.NameRoute,

                                           (int)((int)mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                                           Length_Route),
                                           mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                                           list_Points_Route.Count
                                          );
                 });

                // ----------------------------------------------------------------------------------------
                // Занести маршрут в БД

                // Сделать одну запись i-го маршрута в формате для БД, используя свой класс для i-го маршрута
                TableRoute objTableRoute = new TableRoute();
                objTableRoute = RecordRouteForBD(mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1]);

                // Добавить запись в БД
                clientDB?.Tables[NameTable.TableRoute].AddAsync(objTableRoute);
                // ----------------------------------------------------------------------------------------
                // Конец текущего маршрута

                mapCtrl.objClassInterfaceMap.flStartRoute = false;

                // ----------------------------------------------------------------------------------------
            }
        }

        // ************************************ СобытиеПоНажатиюКнопкиПтичкаВКонтроле2RouteСPropertyGrid

        // Click Button of Route task (WpfRouteControl) *********************************************************
        // Нажатие кнопки в TabControl/вкладка Маршрут/кнопка Добавить маршрут

        private void ButtonAddRoute(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------------------
            if (objWindowRoute == null)
            {
                objWindowRoute = new WindowRoute();
                objWindowRoute.Owner = this;
                GlobalVarMapMain.objWindowRouteG = objWindowRoute;
                objWindowRoute.Show();
            }
            else
            {
                objWindowRoute.Show();
            }
            // -------------------------------------------------------------------------------------------------
            mapCtrl.objClassInterfaceMap.flRouteWindow = true;
            // -------------------------------------------------------------------------------------------------
        }

        // ********************************************************* Click Button of Route task (WpfRouteControl)

        // Закрыть окно для ввода маршрута **********************************************************************
        private void ButtonClouse(object sender, EventArgs e)
        {
            mapCtrl.objClassInterfaceMap.flRouteWindow = false;
        }

        // ********************************************************************** Закрыть окно для ввода маршрута

        // Click CheckBox ***************************************************************************************
        // Click по птичке в CheckBox в Control1 в DataGrid

        private void RouteTaskCntr_OnCheckBoxClick(object sender, ClassArgRoute e)
        {
            //throw new NotImplementedException();
            // ----------------------------------------------------------------------------------------
            int index = 0;
            bool chk = false;
            // ----------------------------------------------------------------------------------------
            // Индекс строки в DataGrid
            index = e.index;
            // Состояние птички в CheckBox в DataGrid
            chk = e.checkM;

            if (mapCtrl.objClassInterfaceMap.List_Routes.Count <= index)
                return;

            if (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
            {
                // ----------------------------------------------------------------------------------------

                mapCtrl.objClassInterfaceMap.List_Routes[index].checkMap = chk;

                // ----------------------------------------------------------------------------------------
                // Отрисовка

                Dispatcher.Invoke(() =>
                {
                    //if (mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                    //list_Points_Route.Count > 1)
                    //{
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    //}
                });
                // ----------------------------------------------------------------------------------------
            }
        }

        // *************************************************************************************** Click CheckBox

        // ClickDel *********************************************************************************************
        // Нажатие кнопки DEL вверху DataGrid для удаления маршрута по выделенной строке
        // !!! Очистка DataGrid осуществляется в Controle1 по нажатию кнопки Del

        private void RouteTaskCntr_OnDelRoute(object sender, ClassArgRouteDel e)
        {
            //throw new NotImplementedException();

            // ----------------------------------------------------------------------------------------
            int index = 0;
            int i = 0;
            // ----------------------------------------------------------------------------------------
            // Индекс строки в DataGrid
            index = e.index;

            if (mapCtrl.objClassInterfaceMap.List_Routes.Count <= index)
                return;

            // ----------------------------------------------------------------------------------------
            // Если начали на карте рисовать маршрут и нажали DEL без сохранения маршрута
            if (
                //(index==-1)&&
                (mapCtrl.objClassInterfaceMap.flStartRoute == true) &&
                (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
               )
            {
                mapCtrl.objClassInterfaceMap.List_Routes.Remove(mapCtrl.objClassInterfaceMap.List_Routes[VisualChildrenCount - 1]);
            }
            // ----------------------------------------------------------------------------------------
            mapCtrl.objClassInterfaceMap.flStartRoute = false;
            // ----------------------------------------------------------------------------------------
            // Удалить маршрут

            if (
                (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0) &&
                (index != -1)
                )
            {
                mapCtrl.objClassInterfaceMap.List_Routes.Remove(mapCtrl.objClassInterfaceMap.List_Routes[index]);
                // ----------------------------------------------------------------------------------------
                // Удаление из БД

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Формирование нового списка для БД

                List<TableRoute> lstTableRoute = new List<TableRoute>();

                // FOR1
                for (i = 0; i < mapCtrl.objClassInterfaceMap.List_Routes.Count; i++)
                {
                    // Сделать одну запись i-го маршрута в формате для БД, используя свой класс для i-го маршрута
                    TableRoute objTableRoute = new TableRoute();
                    objTableRoute = RecordRouteForBD(mapCtrl.objClassInterfaceMap.List_Routes[i]);

                    // Добавить в лист для БД
                    lstTableRoute.Add(objTableRoute);
                } // FOR1 (по маршрутам)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // Очищаем таблицу БД

                clientDB?.Tables[NameTable.TableRoute].CLearAsync();
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Загружаем новый список

                clientDB?.Tables[NameTable.TableRoute].AddRangeAsync(lstTableRoute);
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                // ----------------------------------------------------------------------------------------
                // Отрисовка

                //Dispatcher.Invoke(() =>
                //{
                ////if (mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                ////list_Points_Route.Count > 1)
                //{
                //        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                //    }

                //});
                // ----------------------------------------------------------------------------------------
            } // Список маршрутов не нулевой

            // ----------------------------------------------------------------------------------------
            // Отрисовка

            Dispatcher.Invoke(() =>
            {
                //if (mapCtrl.objClassInterfaceMap.List_Routes[mapCtrl.objClassInterfaceMap.List_Routes.Count - 1].
                //list_Points_Route.Count > 1)
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                }
            });
            // ----------------------------------------------------------------------------------------
        }

        // ********************************************************************************************* ClickDel

        // ClearAll *********************************************************************************************
        // Нажатие кнопки ClearAll вверху DataGrid для удаления всех маршрутов
        // !!! Очистка DataGrid осуществляется в Controle1 по нажатию кнопки ClearAll

        private void RouteTaskCntr_OnClearAll(object sender, EventArgs e)
        {
            //throw new NotImplementedException();

            // ----------------------------------------------------------------------------------------
            int index = 0;
            // ----------------------------------------------------------------------------------------
            // Удалить маршруты

            if (mapCtrl.objClassInterfaceMap.List_Routes.Count > 0)
            {
                for (index = mapCtrl.objClassInterfaceMap.List_Routes.Count - 1; index >= 0; index--)
                {
                    mapCtrl.objClassInterfaceMap.List_Routes.Remove(mapCtrl.objClassInterfaceMap.List_Routes[index]);
                }
            }
            // ----------------------------------------------------------------------------------------
            // Если начали набирать маршрут, но не сохранили

            mapCtrl.objClassInterfaceMap.flStartRoute = false;
            // ----------------------------------------------------------------------------------------
            // Очистить таблицу в БД

            clientDB?.Tables[NameTable.TableRoute].CLearAsync();
            // ----------------------------------------------------------------------------------------
            // Отрисовка

            Dispatcher.Invoke(() =>
            {
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            });
            // ----------------------------------------------------------------------------------------
        }

        // ********************************************************************************************* ClearAll

        // Click Button of Azimuth task (WpfTasksControl) *******************************************************
        private void ButtonAz(object sender, EventArgs e)
        {
            /*
                        // -------------------------------------------------------------------------------------
                        if (objWindowAzimuth == null)
                        {
                            objWindowAzimuth = new WindowAzimuth();
                            objWindowAzimuth.Owner = this;
                            GlobalVarMapMain.objWindowAzimuthG = objWindowAzimuth;
                            objWindowAzimuth.Show();

                            //ClassMapRastrClear.f_Clear_FormAz1();
                        }
                        else
                        {
                            objWindowAzimuth.Show();
                        }

                        GlobalVarMapMain.flAzimuth = true;
                        // -------------------------------------------------------------------------------------

                        // Otl ---------------------------------------------------------------------------------
                        // --------------------------------------------------------------------------------- Otl
            */
        }

        // ******************************************************* Click Button of Azimuth task (WpfTasksControl)

        // Show azimuth title near station **********************************************************************

        private void CheckShowAzimuth(object sender, EventArgs e)
        {
            /*
                        GlobalVarMapMain.CheckShowAzimuth = true;

                        mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                                     GlobalVarMapMain.flAzimuth,
                                                                     GlobalVarMapMain.CheckShowAzimuth,
                                                                     GlobalVarMapMain.LAT_Rastr,
                                                                     GlobalVarMapMain.LONG_Rastr,
                                                                     GlobalVarMapMain.listAzimuth
                                                                     );
                    }
                    private void UnCheckShowAzimuth(object sender, EventArgs e)
                    {
                        GlobalVarMapMain.CheckShowAzimuth = false;

                        mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                                     GlobalVarMapMain.flAzimuth,
                                                                     GlobalVarMapMain.CheckShowAzimuth,
                                                                     GlobalVarMapMain.LAT_Rastr,
                                                                     GlobalVarMapMain.LONG_Rastr,
                                                                     GlobalVarMapMain.listAzimuth
                                                                     );
            */
        }

        // ********************************************************************** Show azimuth title near station

        // Click Button "Clear" in Azimuth Control **************************************************************

        public void ButtonClearAzimuth(object sender, EventArgs e)

        {
            objClassFunctionsMain.ClearAzimuth();
        }

        // ************************************************************** Click Button "Clear" in Azimuth Control

        // Click Button of Triangulation task (WpfTasksControl) *************************************************
        private void ButtonTriang(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------------------
            if (objWindowTriang == null)
            {
                objWindowTriang = new WindowTriang();
                objWindowTriang.Owner = this;
                GlobalVarMapMain.objWindowTriangG = objWindowTriang;
                objWindowTriang.Show();
            }
            else
            {
                objWindowTriang.Show();
            }
            // -------------------------------------------------------------------------------------------------
            GlobalVarMapMain.flTriang = true;
            // -------------------------------------------------------------------------------------------------
        }

        // ************************************************* Click Button of Triangulation task (WpfTasksControl)

        // Click Button "Calc" in Triangulation Control *********************************************************

        private void ButtonCalcTriang(object sender, EventArgs e)
        {
            objWindowTriang.Owner = this;
            objWindowTriang.Show();
            GlobalVarMapMain.flTriang = true;

            // Otl ---------------------------------------------------------------------------------------------
            // --------------------------------------------------------------------------------------------- Otl

            // Otl1 ---------------------------------------------------------------------------------------------
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;

            DispatchIfNecessary(() =>
            {
                GlobalVarMapMain.Pel1Grad = Convert.ToDouble(objWindowTriang.tasksTriang.textBox5.Text);
                GlobalVarMapMain.Pel2Grad = Convert.ToDouble(objWindowTriang.tasksTriang.textBox6.Text);
            });

            double[] arr_Pel1 = new double[GlobalVarMapMain.numberofdots * 2 + 1];     // R,Широта,долгота
            double[] arr_Pel2 = new double[GlobalVarMapMain.numberofdots * 2 + 1];     // R,Широта,долгота
            double[] arr_Pel_XYZ1 = new double[GlobalVarMapMain.numberofdots * 3 + 1];
            double[] arr_Pel_XYZ2 = new double[GlobalVarMapMain.numberofdots * 3 + 1];

            ClassBearing.f_Bearing(
                                   GlobalVarMapMain.Pel1Grad,
                                   GlobalVarMapMain.distance,
                                   GlobalVarMapMain.numberofdots,
                                   GlobalVarMapMain.LatPel1,
                                   GlobalVarMapMain.LonPel1,
                                   1,  // WGS84
                                   ref a1, ref b1, ref c1,
                                   ref d1, ref f1, ref g1,
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1
                                   );

            ClassBearing.f_Bearing(
                                   GlobalVarMapMain.Pel2Grad,
                                   GlobalVarMapMain.distance,
                                   GlobalVarMapMain.numberofdots,
                                   GlobalVarMapMain.LatPel2,
                                   GlobalVarMapMain.LonPel2,
                                   1,  // WGS84
                                   ref a2, ref b2, ref c2,
                                   ref d2, ref f2, ref g2,
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2
                                   );
            // .................................................................................................
            // Peleng1

            List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();
            int i1 = 0;

            pointPel1.Add(new Mapsui.Geometries.Point(GlobalVarMapMain.LonPel1, GlobalVarMapMain.LatPel1));

            for (i1 = 0; i1 < GlobalVarMapMain.numberofdots * 2; i1 += 2)
            {
                pointPel1.Add(new Mapsui.Geometries.Point(arr_Pel1[i1 + 1], arr_Pel1[i1]));
            }

            string s1 = mapCtrl.DrawLinesLatLong(
                                                // Degree
                                                pointPel1,
                                                // Color
                                                100,
                                                255,
                                                0,
                                                0
                                               );

            // .................................................................................................
            // Peleng2
            List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();
            int i2 = 0;

            pointPel2.Add(new Mapsui.Geometries.Point(GlobalVarMapMain.LonPel2, GlobalVarMapMain.LatPel2));

            for (i2 = 0; i2 < GlobalVarMapMain.numberofdots * 2; i2 += 2)
            {
                pointPel2.Add(new Mapsui.Geometries.Point(arr_Pel2[i2 + 1], arr_Pel2[i2]));
            }

            string s2 = mapCtrl.DrawLinesLatLong(
                                                // Degree
                                                pointPel2,
                                                // Color
                                                100,
                                                255,
                                                0,
                                                0
                                               );
            // .................................................................................................
            // Triangulation1
            // .................................................................................................
            // Triangulation2

            Coord JamStObj = new Coord();

            List<JamBearing> JamSt = new List<JamBearing>();

            JamBearing objJamBearing = new JamBearing();
            objJamBearing.Coordinate = new Coord();
            objJamBearing.Bearing = (float)GlobalVarMapMain.Pel1Grad;
            objJamBearing.Coordinate.Latitude = GlobalVarMapMain.LatPel1;
            objJamBearing.Coordinate.Longitude = GlobalVarMapMain.LonPel1;
            JamSt.Add(objJamBearing);

            JamBearing objJamBearing1 = new JamBearing();
            objJamBearing1.Coordinate = new Coord();
            objJamBearing1.Bearing = (float)GlobalVarMapMain.Pel2Grad;
            objJamBearing1.Coordinate.Latitude = GlobalVarMapMain.LatPel2;
            objJamBearing1.Coordinate.Longitude = GlobalVarMapMain.LonPel2;
            JamSt.Add(objJamBearing1);

            JamStObj = ClassBearing.DefineCoordTriang(JamSt);

            // Get current dir
            string dir1 = "";
            dir1 = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((JamStObj.Latitude != -1) && (JamStObj.Longitude != -1))
            {
                string s5 = mapCtrl.DrawImage(
                                             JamStObj.Latitude,
                                             JamStObj.Longitude,
                                             // Path to image
                                             dir1 + "\\Images\\OtherObject\\" + "tr10.png",
                                             // Text
                                             "",
                                             // Scale
                                             0.6
                                  );
            }
            // .................................................................................................

            // --------------------------------------------------------------------------------------------- Otl1
        }

        // ********************************************************* Click Button "Calc" in Triangulation Control

        // Loaded ************************************************************************************************
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Load");
            objWindowAzimuth.Owner = this;
            objWindowTriang.Owner = this;

            // Окно для контрола с PropertyGrid
            objWindowRoute.Owner = this;

            CultureInfo current = System.Threading.Thread.CurrentThread.CurrentUICulture;
            if (current.TwoLetterISOLanguageName != "fr")
            {
                CultureInfo newCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
                // Make current UI culture consistent with current culture.
                System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            }
        }

        // ************************************************************************************************ Loaded

        // Closing ************************************************************************************************
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // objWindowAzimuth.Close();
            // objWindowTriang.Close();

            // Окно для контрола с PropertyGrid
            objWindowRoute.Close();

            // Out from application
            Application.Current.Shutdown();
        }

        // ************************************************************************************************ Closing

        // Settings -> Property Grid ****************************************************************************
        // The OLD -> Ybrat

        private void ButtonSettings_Click(object sender, RoutedEventArgs e)
        {
            // --------------------------------------------------------------------------------------------------
            // True
            if (GlobalVarMapMain.ButtonSettingsOn == true)
            {
                Image myImage1 = new Image();
                BitmapImage bi1 = new BitmapImage();
                bi1.BeginInit();
                bi1.UriSource = new Uri("Resources/RedKv.PNG", UriKind.Relative);
                bi1.EndInit();

                GlobalVarMapMain.ButtonSettingsOn = false;
                //ImSettings.Source = bi1;

                //PropGridCtrl.Visibility = Visibility.Collapsed;

                Col0.Width = new GridLength(1, GridUnitType.Pixel);
            } // True
            // --------------------------------------------------------------------------------------------------
            // False
            else
            {
                Image myImage3 = new Image();
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri("Resources/file_document_box_outline.PNG", UriKind.Relative);
                bi3.EndInit();

                GlobalVarMapMain.ButtonSettingsOn = true;
                //ImSettings.Source = bi3;

                Col0.Width = new GridLength(451, GridUnitType.Pixel);
            } // False
            // --------------------------------------------------------------------------------------------------
        }


        public int GetHeightFunc(int x, int y)
        {
            if (mapCtrl.MapControl.DtedMass == null) return 0;
            var converted = MercatorDependOnMapType.ToLonLat(x, y);
            var height = mapCtrl.MapControl.DtedMass.GetElevation(converted.X, converted.Y);
            return height ?? 0;
        }

        public Mapsui.Geometries.Point ConvertLonLat(double x, double y)
        {
            return MercatorDependOnMapType.FromLonLat(x, y);
        }

        Mapsui.Styles.Color BrushZoneLos = Mapsui.Styles.Color.FromArgb(150, 109, 11, 255);
        List<Feature> losZone = new List<Feature>();
        private void ViewModel_OnPolygonChanged(object sender, IEnumerable<List<Mapsui.Geometries.Point>> e)
        {
            if (mapCtrl.MapControl.IsMapLoaded)
            {
                if (losZone != null)
                {
                    foreach (var b in losZone)
                        mapCtrl.MapControl.RemoveObject(b);
                }

                foreach (var line in e)
                {
                    losZone.Add(DrawFeaturePolygonLine(line, new Pen(BrushZoneLos, 3)));
                }

            }
        }

        private Feature DrawFeaturePolygonLine(List<Mapsui.Geometries.Point> line, Pen pen)
        {
            Feature ifeature = new Feature();
            if (line == null)
                return ifeature;
            try
            {
                ifeature = mapCtrl.MapControl.AddPolyline(line, pen);

                return ifeature;

            }
            catch { return null; }
        }

        public ViewModel ViewModel
        {
            get => viewModel;
            set
            {
                viewModel = value;
                OnPropertyChangedViewModel();
            }
        }

        #region NotifyPropertyChanged

        public void OnPropertyChangedViewModel([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion



        // *************************************************************************** Settings -> Property Grid
    } // Class
} // Namespace