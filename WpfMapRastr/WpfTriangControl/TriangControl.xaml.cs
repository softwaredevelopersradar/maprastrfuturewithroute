﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WpfTriangControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        public event EventHandler OnCalcTriang;

        public UserControl1()
        {
            InitializeComponent();
        }

        // Click *************************************************************
        public void ButtonCalc_Click(object sender, RoutedEventArgs e)
        {
            OnCalcTriang?.Invoke(this, new EventArgs());
        }

        // ************************************************************* Click
    } // Class
} // Namespace