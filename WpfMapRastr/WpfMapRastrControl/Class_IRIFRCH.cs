﻿using System.Collections.Generic;

namespace WpfMapRastrControl
{
    public class Class_IRIFRCH
    {
        public double Latitude = 0;
        public double Longitude = 0;
        public double FreqKHz = 0;
        public int Id = 0;
        public bool IsSelected = false;

        public List<ClassBearingMap> list_JSBearing = new List<ClassBearingMap>();
    } // Class
} // Namespace