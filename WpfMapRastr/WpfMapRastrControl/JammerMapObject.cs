﻿using Mapsui.Geometries;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace WpfMapRastrControl
{
    public class MapJammerObject : IMapJammerObject
    {
        public IFeature Feature { get; }

        public Point Position { get; }

        public MapObjectStyle Style { get; }

        public int ID { get; set; }

        public MapJammerObject(IFeature feature, Point position, MapObjectStyle style, int id)
        {
            Feature = feature;
            Position = position;
            Style = style;
            ID = id;
        }
    }
}
