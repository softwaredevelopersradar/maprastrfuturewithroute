﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace WpfMapRastrControl
{
    public class LineDistanceObj
    {
        public IMapObject lineDistanceObj { get; set; }

        public Feature LineDistance { get; set; }

        public int ID { get; set; }

        public LineDistanceObj()
        { }
    }
}
