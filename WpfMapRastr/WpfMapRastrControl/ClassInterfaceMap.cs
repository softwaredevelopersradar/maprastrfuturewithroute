﻿using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using Mapsui.UI.Wpf;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using WpfMapControl;
using static System.Net.Mime.MediaTypeNames;
using Bitmap = System.Drawing.Bitmap;
using Image = System.Drawing.Image;
using Point = System.Drawing.Point;

namespace WpfMapRastrControl
{
    public class ClassInterfaceMap : InterfaceMap
    {
        private List<MapObjectStyle> lst_placeObjectStylePlane;
        private List<MapObjectStyle> lst_placeObjectStylePlaneSelect;

        private MapObjectStyle _placeObjectStylePlane;
        private Image imagePlaneInitial;

        // Element for MAP
        private RasterMapControl objRasterMapControl = new RasterMapControl();

        // Constructor **************************************************************************
        public ClassInterfaceMap(RasterMapControl ObjRasterMapControl)
        {
            objRasterMapControl = ObjRasterMapControl;

            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            lst_placeObjectStylePlane = TestStyle_1(dir + "\\Images\\OtherObject\\" + "Airpl10.png", 0.5);
            lst_placeObjectStylePlaneSelect = TestStyle_1(dir + "\\Images\\OtherObject\\" + "Airpl10red.png", 0.5);
        }

        // ************************************************************************** Constructor

        // VAR **********************************************************************************

        // Язык .................................................................................
        public byte _FlLng = 1;

        // ................................................................................. Язык

        // Mouse .......................................................................
        // =false -> Left
        // =true -> Right

        public bool _FlMouse = false;
        // ....................................................................... Mouse

        // ......................................................................................

        private bool _MapOpened = false;
        private bool _MatrixOpened = false;
        private double _Scale = -1;

        // JS
        //public List<TableASP> _List_JS = new List<TableASP>();
        public List<ClassJS> _List_JS = new List<ClassJS>();

        public List<TableSectorsRangesRecon> _List_SectorsRangesRecon = new List<TableSectorsRangesRecon>();
        // =1 -> draw JS
        public bool _ShowJS = false;

        // =1 -> draw AntennasDirections
        public bool _ShowAntennas = false;

        // ......................................................................................

        // Objects1 (ИРИ ФРЧ)
        //public List<TempFWS> _List_SRW_FRF = new List<TempFWS>();
        public List<Class_IRIFRCH> _List_SRW_FRF = new List<Class_IRIFRCH>();

        // =1 -> draw Objects1
        public bool _Show_SRW_FRF = false;

        // Objects2 (ИРИ ФРЧ ЦР)
        public List<TableReconFWS> _List_SRW_FRF_TD = new List<TableReconFWS>();

        // =1 -> draw Objects2
        public bool _Show_SRW_FRF_TD = false;

        // Objects3 (ИРИ ФРЧ РП)
        public List<TableSuppressFWS> _List_SRW_FRF_RS = new List<TableSuppressFWS>();
        public List<TempSuppressFWS> _List_SRW_FRF_RS_TEMP = new List<TempSuppressFWS>();
        // =1 -> draw Objects3
        public bool _Show_SRW_FRF_RS = false;

        // Objects4 (ИРИ ППРЧ)
        public List<TableReconFHSS> _List_SRW_STRF_Recon = new List<TableReconFHSS>();

        public List<Class_IRIPPRCH> _List_SRW_STRF = new List<Class_IRIPPRCH>();
        public List<TableSourceFHSS> _List_SRW_STRF_Coord = new List<TableSourceFHSS>();

        // =1 -> draw Objects4
        public bool _Show_SRW_STRF = false;

        // Objects5 (ИРИ ППРЧ РП)
        // Otl -> другой тип вписать
        //public List<TableSuppressFWS> _List_SRW_STRF_RS = new List<TableSuppressFWS>();
        public List<TableSuppressFHSS> _List_SRW_STRF_RS = new List<TableSuppressFHSS>();

        // =1 -> draw Objects5
        public bool _Show_SRW_STRF_RS = false;

        // Airplanes
        // 033
        public List<TempADSB_AP> _List_AP = new List<TempADSB_AP>();

        //public List<TempADSB> _List_AP = new List<TempADSB>();

        // =1 -> draw Objects5
        public bool _Show_AP = false;

        // S from station ..............................................................
        public bool _Show_S = false;

        // Coordinates of source
        public double _LatSource_S = -1;

        public double _LongSource_S = -1;
        public double _HSource_S = -1;
        public double _SObjSP = 0;
        // .............................................................. S from station

        // ......................................................................................
        /*
                // get Lat from Map on mouse click
                public double _LatMap = 0;
                // get Long from Map on mouse click
                public double _LongMap = 0;
                // get H from Map on mouse click
                public double _HMap = 0;
                // get X(Mercator) from Map on mouse click
                public double _XMap = 0;
                // get Y(Mercator) from Map on mouse click
                public double _YMap = 0;
        */
        // ......................................................................................

        // Azimuth ..............................................................................

        // =true -> активна вкладка Азимут в TabControl задач
        public bool _flAzimuth = false;

        // =true -> отображать значение азимута около станций
        public bool _CheckShowAzimuth = false;
        public bool _CheckShowReliefProfile = false;
        // Coordinates of source
        public double _LatSource_Azimuth = -1;

        public double _LongSource_Azimuth = -1;
        public double _HSource_Azimuth = -1;
        // .............................................................................. Azimuth

        // Route ................................................................................
        // Route

        // =true -> активна вкладка Route в TabControl задач
        public bool _flRoute = false;

        // =true -> начало откладывания маршрута
        public bool _flStartRoute = false;

        // =true, если активно окно с Control2(PropertyGrid)
        public bool _flRouteWindow = false;

        // Routes
        // Лист маршрутов
        public List<ClassRoute> _List_Routes = new List<ClassRoute>();

        // ................................................................................ Route

        // ********************************************************************************** VAR

        // Properties ***************************************************************************

        // Язык .................................................................................
        public byte FlLng
        {
            get { return _FlLng; }
            set { _FlLng = value; }
        }

        // ................................................................................. Язык

        // Mouse ................................................................................
        // =false -> Left
        // =true -> Right

        public bool FlMouse
        {
            get { return _FlMouse; }
            set { _FlMouse = value; }
        }

        // ............................................................................... Mouse

        // ......................................................................................
        // =1 -> Tha map is opened
        public bool MapOpened
        {
            get { return _MapOpened; }
        }

        // =1 -> The height matrix is opened
        public bool MatrixOpened
        {
            get { return _MatrixOpened; }
        }

        // Current Scale
        public double Scale
        {
            get { return _Scale; }
        }

        // ......................................................................................
        //Jammer stations

        //public List<TableASP> List_JS
        public List<ClassJS> List_JS
        {
            get { return _List_JS; }
            set
            {
                //_List_JS = (List<TableASP>)value;
                _List_JS = (List<ClassJS>)value;
            }
        }

        public List<TableSectorsRangesRecon> List_SectorsRangesRecon
        {
            get { return _List_SectorsRangesRecon; }
            set
            {
                _List_SectorsRangesRecon = (List<TableSectorsRangesRecon>)value;
            }
        }

        public bool ShowJS
        {
            get { return _ShowJS; }
            set { _ShowJS = value; }
        }

        // ......................................................................................
        // S from station

        public bool Show_S
        {
            get { return _Show_S; }
            set { _Show_S = value; }
        }

        public double LatSource_S
        {
            get { return _LatSource_S; }
            set { _LatSource_S = value; }
        }

        public double LongSource_S
        {
            get { return _LongSource_S; }
            set { _LongSource_S = value; }
        }

        public double HSource_S
        {
            get { return _HSource_S; }
            set { _HSource_S = value; }
        }

        public double SObjSP
        {
            get { return _SObjSP; }
            set { _SObjSP = value; }
        }

        // ......................................................................................
        // AntennasDirections

        public bool ShowAntennas
        {
            get { return _ShowAntennas; }
            set { _ShowAntennas = value; }
        }

        // ......................................................................................

        // Objects1 (ИРИ ФРЧ)

        //public List<TempFWS> List_SRW_FRF
        public List<Class_IRIFRCH> List_SRW_FRF
        {
            get { return _List_SRW_FRF; }

            set
            {
                //_List_SRW_FRF = (List<TempFWS>)value;
                _List_SRW_FRF = (List<Class_IRIFRCH>)value;
            }
        }

        public bool Show_SRW_FRF
        {
            get { return _Show_SRW_FRF; }
            set { _Show_SRW_FRF = value; }
        }

        // ......................................................................................
        // Objects2 (ИРИ ФРЧ ЦР)

        public List<TableReconFWS> List_SRW_FRF_TD
        {
            get { return _List_SRW_FRF_TD; }
            set
            {
                _List_SRW_FRF_TD = (List<TableReconFWS>)value;
            }
        }

        public bool Show_SRW_FRF_TD
        {
            get { return _Show_SRW_FRF_TD; }
            set { _Show_SRW_FRF_TD = value; }
        }

        // ......................................................................................
        // Objects3 (ИРИ ФРЧ РП)

        public List<TableSuppressFWS> List_SRW_FRF_RS
        {
            get { return _List_SRW_FRF_RS; }
            set
            {
                _List_SRW_FRF_RS = (List<TableSuppressFWS>)value;
            }
        }

        public List<TempSuppressFWS> List_SRW_FRF_RS_TEMP
        {
            get { return _List_SRW_FRF_RS_TEMP; }
            set
            {
                _List_SRW_FRF_RS_TEMP = (List<TempSuppressFWS>)value;
            }
        }


        public bool Show_SRW_FRF_RS
        {
            get { return _Show_SRW_FRF_RS; }
            set { _Show_SRW_FRF_RS = value; }
        }

        // ......................................................................................
        // Objects4 (ИРИ ППРЧ)

        //public List<TableReconFHSS> List_SRW_STRF
        public List<Class_IRIPPRCH> List_SRW_STRF
        {
            get { return _List_SRW_STRF; }
            set
            {
                //_List_SRW_STRF = (List<TableReconFHSS>)value;
                _List_SRW_STRF = (List<Class_IRIPPRCH>)value;
            }
        }

        public List<TableReconFHSS> List_SRW_STRF_Recon
        {
            get { return _List_SRW_STRF_Recon; }
            set
            {
                _List_SRW_STRF_Recon = (List<TableReconFHSS>)value;
            }
        }

        public List<TableSourceFHSS> List_SRW_STRF_Coord
        {
            get { return _List_SRW_STRF_Coord; }
            set
            {
                _List_SRW_STRF_Coord = (List<TableSourceFHSS>)value;
            }
        }

        public bool Show_SRW_STRF
        {
            get { return _Show_SRW_STRF; }
            set { _Show_SRW_STRF = value; }
        }

        // ......................................................................................
        // Objects5 (ИРИ ППРЧ РП)

        // Otl -> другой тип вписать
        //public List<TableSuppressFWS> List_SRW_STRF_RS
        public List<TableSuppressFHSS> List_SRW_STRF_RS
        {
            get { return _List_SRW_STRF_RS; }
            set
            {
                _List_SRW_STRF_RS = (List<TableSuppressFHSS>)value;
            }
        }

        public bool Show_SRW_STRF_RS
        {
            get { return _Show_SRW_STRF_RS; }
            set { _Show_SRW_STRF_RS = value; }
        }

        // ......................................................................................
        // Airplanes

        // 033
        /*
                public List<TempADSB> List_AP
                {
                    get { return _List_AP; }
                    set
                    {
                        _List_AP = (List<TempADSB>)value;
                    }
                }
        */

        public List<TempADSB_AP> List_AP
        {
            get { return _List_AP; }
            set
            {
                _List_AP = (List<TempADSB_AP>)value;
            }
        }

        public bool Show_AP
        {
            get { return _Show_AP; }
            set { _Show_AP = value; }
        }

        // ......................................................................................

        /*
                // get Lat from Map on mouse click
                public double LatMap
                {
                    get { return _LatMap; }
                }
                // get Long from Map on mouse click
                public double LongMap
                {
                    get { return _LongMap; }
                }
                // get H from Map on mouse click
                public double HMap
                {
                    get { return _HMap; }
                }
                // get X(Mercator) from Map on mouse click
                public double XMap
                {
                    get { return _XMap; }
                }
                // get Y(Mercator) from Map on mouse click
                public double YMap
                {
                    get { return _YMap; }
                }
        */
        // ......................................................................................
        // Azimuth

        public bool flAzimuth
        {
            get { return _flAzimuth; }
            set { _flAzimuth = value; }
        }

        public bool CheckShowAzimuth
        {
            get { return _CheckShowAzimuth; }
            set { _CheckShowAzimuth = value; }
        }

        public bool CheckShowReliefProfile
        {
            get { return _CheckShowReliefProfile; }
            set { _CheckShowReliefProfile = value; }
        }


        public double LatSource_Azimuth
        {
            get { return _LatSource_Azimuth; }
            set { _LatSource_Azimuth = value; }
        }

        public double LongSource_Azimuth
        {
            get { return _LongSource_Azimuth; }
            set { _LongSource_Azimuth = value; }
        }

        public double HSource_Azimuth
        {
            get { return _HSource_Azimuth; }
            set { _HSource_Azimuth = value; }
        }

        // ......................................................................................
        // Route

        public bool flRoute
        {
            get { return _flRoute; }
            set { _flRoute = value; }
        }

        public bool flStartRoute
        {
            get { return _flStartRoute; }
            set { _flStartRoute = value; }
        }

        public bool flRouteWindow
        {
            get { return _flRouteWindow; }
            set { _flRouteWindow = value; }
        }

        public List<ClassRoute> List_Routes
        {
            get { return _List_Routes; }
            set
            {
                _List_Routes = (List<ClassRoute>)value;
            }
        }

        // ......................................................................................

        // *************************************************************************** Properties

        // METHODS

        // OpenMap ******************************************************************************
        // Open Map (path - path to map: from dialog or from ini file)

        public void OpenMap(string pathMap)
        {
            // Map was opened
            try
            {
                objRasterMapControl.OpenMap(pathMap);
                _MapOpened = true;
            } //try (Map was opened)

            // Map wasn't opened
            //catch (Exception ex)
            catch
            {
                _MapOpened = false;
            } // Catch (Map wasn't opened)
        }

        // ****************************************************************************** OpenMap

        // OpenMatrix ***************************************************************************
        // Open Matrix (path - path to Matrix: from dialog or from ini file)

        public void OpenMatrix(string pathMatrix)
        {
            
            // Matrix was opened
            try
            {
                if (pathMatrix != "")
                {
                    string PathMatrix = Path.GetDirectoryName(pathMatrix);
                    if (PathMatrix != null)
                    {
                        string[] files = Directory.GetFiles(PathMatrix);

                        objRasterMapControl.OpenDted(files);
                    }

                    _MatrixOpened = true;
                }

                
            } //try (Matrix was opened)

            // Matrix wasn't opened
            catch
            {
                _MatrixOpened = false;
            } // Catch (Matrix wasn't opened)
        }

        // *************************************************************************** OpenMatrix

        // SetScale *****************************************************************************

        public void SetScale(double scale)
        {
            // Scale is set
            try
            {
                objRasterMapControl.Resolution = scale;
                _Scale = scale;
            } //try (Scale is set)

            // Scale isn't set
            catch
            {
                _Scale = -1;
            } // Catch (Scale isn't set)
        }

        // ***************************************************************************** SetScale

        // CloseMap *****************************************************************************
        // Close Map

        public void CloseMap()
        {
            // Map was closed
            try
            {
                objRasterMapControl.CloseMap();
                _MapOpened = false;
            } //try (Map was closed)

            // Map wasn't closed
            catch
            {
            } // Catch (Map wasn't closed)
        }

        // ***************************************************************************** CloseMap

        // CloseMatrix **************************************************************************
        // Close the height matrix

        public void CloseMatrix()
        {
            // Matrix was closed
            try
            {
                objRasterMapControl.CloseDted();
                _MatrixOpened = false;
            } //try (Matrix was closed)

            // Matrix wasn't closed
            catch
            {
            } // Catch (Matrix wasn't closed)
        }

        // ************************************************************************** CloseMatrix

        // Increase the scale *******************************************************************

        public bool IncreaseScale()
        {
            try
            {
                objRasterMapControl.Resolution = objRasterMapControl.Resolution - 10000;

                if (objRasterMapControl.Resolution <= objRasterMapControl.MinResolution)
                    objRasterMapControl.Resolution = objRasterMapControl.MinResolution;

                // Emirates
                //else if (objRasterMapControl.Resolution == objRasterMapControl.MaxResolution)
                //    objRasterMapControl.Resolution = 73942;

                _Scale = objRasterMapControl.Resolution;

                return true;
            } //try
            catch
            {
                return false;
            } // Catch
        }

        // ******************************************************************* Increase the scale

        // Decrease the scale *******************************************************************

        public bool DecreaseScale()
        {
            try
            {
                objRasterMapControl.Resolution = objRasterMapControl.Resolution + 10000;

                if (objRasterMapControl.Resolution >= objRasterMapControl.MaxResolution)
                    objRasterMapControl.Resolution = objRasterMapControl.MaxResolution;
                // Emirates
                //else if (objRasterMapControl.Resolution == objRasterMapControl.MinResolution)
                //    objRasterMapControl.Resolution = 13942;

                _Scale = objRasterMapControl.Resolution;

                return true;
            } //try
            catch
            {
                return false;
            } // Catch
        }

        // ******************************************************************* Decrease the scale

        // Base scale ***************************************************************************

        public bool BaseScale()
        {
            try
            {
                // Emirates
                //objRasterMapControl.Resolution = 43942;
                // Belarus
                objRasterMapControl.Resolution = objRasterMapControl.MinResolution;

                _Scale = objRasterMapControl.Resolution;

                return true;
            } //try
            catch
            {
                return false;
            } // Catch
        }

        // *************************************************************************** Base scale

        // CenterMapToXY ************************************************************************
        // Center the map on pozition XY (For Mercator Map)
        // 999

        public bool CenterMapToXY(double x, double y)
        {
            try
            {
                /*
                                double lat;
                                double lon;
                                Mapsui.Geometries.Point g = new Mapsui.Geometries.Point();
                                var p = Mercator.ToLonLat(x, y);
                                lat = p.Y;
                                lon = p.X;
                                g.X = lon;
                                g.Y = lat;
                                objRasterMapControl.NavigateTo(g);
                */
                Mapsui.Geometries.Point g = new Mapsui.Geometries.Point();
                g.X = x;
                g.Y = y;
                objRasterMapControl.NavigateTo(g);

                return true;
            } //try
            catch
            {
                return false;
            } // Catch
        }

        // ************************************************************************ CenterMapToXY

        // CenterMapToLatLong *******************************************************************
        // Center the map on pozition Lat,Long (degree) for geographic Map

        public bool CenterMapToLatLong(double Lat, double Long)
        {
            try
            {
                Mapsui.Geometries.Point g = new Mapsui.Geometries.Point();
                g.X = Long;
                g.Y = Lat;
                objRasterMapControl.NavigateTo(g);

                return true;
            } //try
            catch
            {
                return false;
            } // Catch
        }

        // ******************************************************************* CenterMapToLatLong

        // DrawImage ****************************************************************************
        // Draw Image with path to image
        //public IMapJammerObject AddMapJammerObject(MapObjectStyle objectStyle, string text, Mapsui.Geometries.Point wgs84Position, int Id)
        //{
        //    Mapsui.Styles.Brush backColor = new Mapsui.Styles.Brush(Mapsui.Styles.Color.FromArgb(150, 230, 230, 230));
        //    LabelStyle textStyle = new LabelStyle
        //    {
        //        Text = text,
        //        BackColor = backColor,
        //        Offset = objectStyle.TextOffset,
        //        VerticalAlignment = LabelStyle.VerticalAlignmentEnum.Bottom
        //    };
        //    return AddMapJammerObject(objectStyle, textStyle, wgs84Position, Id);
        //}

        //private int _currentObjectId = 0;
        //private readonly List<IMapJammerObject> _mapJammerObjects = new List<IMapJammerObject>();
        //public MemoryProvider _objectLayerMemoryProvider;
        //public IMapJammerObject AddMapJammerObject(MapObjectStyle objectStyle, LabelStyle textStyle, Mapsui.Geometries.Point wgs84Position, int Id)
        //{
        //    Mapsui.Geometries.Point mapPosition = objRasterMapControl.GetMapPosition(wgs84Position);
        //    int num = Interlocked.Increment(ref _currentObjectId);
            
        //    Feature feature = new Feature
        //    {
        //        Geometry = mapPosition,
        //        ["key"] = num
        //    };
        //    MapJammerObject mapJammerObject = new MapJammerObject(feature, wgs84Position, objectStyle, Id);
        //    mapJammerObject.Feature.Styles.Add(objectStyle.Style);
        //    mapJammerObject.Feature.Styles.Add(textStyle);
        //    //_objectLayerMemoryProvider.Features.Add(mapJammerObject.Feature);
        //    _mapJammerObjects.Add(mapJammerObject);
        //    //map.Refresh();
        //    return mapJammerObject;
        //}

        public string DrawImageWithInfo(
                       // Degree (for Geographic Map)
                       // Long=X, Lat=Y for Mercator Map
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      )
        {
            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                pointOwn.X = Long;
                pointOwn.Y = Lat;
                // ..........................................................................
                _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

                                     //"1.png",
                                     s1,
                                     //(Bitmap)imageList1.Images[station.indzn],
                                     scale: scl,
                                     //objectOffset: new Offset(0, 0), // (-15,15) -> (влево,вверх)
                                     objectOffset: new Offset(0, 0),  // смещение центра изображения
                                     textOffset: new Offset(70, -30)
                                     );
                objectGrozaS1 = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Image" + s1;
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможен рисунок изображения" + s1;
                else
                    return "Şəkillər çəkmək mümkün deyil" + s1;
            }
        }

        public string DrawImagePointRelief(
               // Degree (for Geographic Map)
               // Long=X, Lat=Y for Mercator Map
               double Lat,
               double Long,
               // Path to image
               String s1,
               // Title
               String s,
               // Scale of image
               double scl,
               int Id
              )
        {
            
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                pointOwn.X = Long;
                pointOwn.Y = Lat;
                // ..........................................................................
                _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

                                     //"1.png",
                                     s1,
                                     //(Bitmap)imageList1.Images[station.indzn],
                                     scale: scl,
                                     //objectOffset: new Offset(0, 0), // (-15,15) -> (влево,вверх)
                                     objectOffset: new Offset(0, 0),  // смещение центра изображения
                                     textOffset: new Offset(0, 15)
                                     );
                var reliefPoint = new ReliefPontMapObj() { ID = Id };
                reliefPoint.ReliefPoint = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);
                reliefPontMapObjs.Add(reliefPoint);
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Image" + s1;
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможен рисунок изображения" + s1;
                else
                    return "Şəkillər çəkmək mümkün deyil" + s1;
            }
        }



        public string DrawImageWithInfoJS(
                       // Degree (for Geographic Map)
                       // Long=X, Lat=Y for Mercator Map
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Title
                       String s,
                       // Scale of image
                       double scl, int Ind
                      )
        {
            
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                pointOwn.X = Long;
                pointOwn.Y = Lat;
                // ..........................................................................
                _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

                                     //"1.png",
                                     s1,
                                     //(Bitmap)imageList1.Images[station.indzn],
                                     scale: scl,
                                     //objectOffset: new Offset(0, 0), // (-15,15) -> (влево,вверх)
                                     objectOffset: new Offset(0, 0),  // смещение центра изображения
                                     textOffset: new Offset(70, -30)
                                     );
                Mapsui.Styles.Brush backColor = new Mapsui.Styles.Brush(Mapsui.Styles.Color.FromArgb(150, 230, 230, 230));
                LabelStyle textStyle = new LabelStyle
                {
                    Text = s,
                    BackColor = backColor,
                    Offset = _placeObjectStyleOwn.TextOffset,
                    VerticalAlignment = LabelStyle.VerticalAlignmentEnum.Bottom
                };
                var station = new JSMapObj() { ID = Ind };
                station.Station = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);
                stationsHighlight.Add(station);
                //objectGroza = AddMapJammerObject(_placeObjectStyleOwn, s, pointOwn, Ind);
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Image" + s1;
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможен рисунок изображения" + s1;
                else
                    return "Şəkillər çəkmək mümkün deyil" + s1;
            }
        }


        private List<JSMapObj> stationsHighlight = new List<JSMapObj>();
        private List<ReliefPontMapObj> reliefPontMapObjs = new List<ReliefPontMapObj>();
        private List<PointDistanceToObj> pointDistanceToObjs = new List<PointDistanceToObj>();
        private List<LineDistanceObj> lineDistanceObjs = new List<LineDistanceObj>();
        public string DeletehInfoJS(int Id)
        
        {
            
            try
            {
                var station = stationsHighlight.FirstOrDefault(t => t.ID == Id);
                if (station == null)
                    return "";

                objRasterMapControl.RemoveObject(station.Station);
                stationsHighlight.Remove(station);
                return "";
            }
            catch
            {
                return "Delete error";
            }
        }

        public string DeletehReliefPoint(int Id)

        {

            try
            {
                var reliefPoint = reliefPontMapObjs.FirstOrDefault(t => t.ID == Id);
                if (reliefPoint == null)
                    return "";

                objRasterMapControl.RemoveObject(reliefPoint.ReliefPoint);
                reliefPontMapObjs.Remove(reliefPoint);
                return "";
            }
            catch
            {
                return "Delete error";
            }
        }

        public string DeletePointDistance(int Id) 
        {
            try
            {

                for (int i = pointDistanceToObjs.Count; i <= pointDistanceToObjs.Count; i--) 
                {
                    var pointDistanceToObj = pointDistanceToObjs.FirstOrDefault(t => t.ID == Id);

                    if (pointDistanceToObj == null)
                        return "";

                    objRasterMapControl.RemoveObject(pointDistanceToObj.PointDistance);
                    pointDistanceToObjs.Remove(pointDistanceToObj);
                }
             
                return "";
            }
            catch
            {
                return "Delete error";
            }
        }

        public string DeleteLineDistance(int Id)
        {
            try
            {
                for (int i = lineDistanceObjs.Count; i <= lineDistanceObjs.Count; i--) 
                {
                    var lineDistanceObj = lineDistanceObjs.FirstOrDefault(t => t.ID == Id);

                    if (lineDistanceObj == null)
                        return "";

                    objRasterMapControl.RemoveObject(lineDistanceObj.LineDistance);
                    lineDistanceObjs.Remove(lineDistanceObj);

                }

                return "";
            }
            catch
            {
                return "Delete error";
            }
        }


        //public string DrawImageWithInfoJS(
        //               // Degree (for Geographic Map)
        //               // Long=X, Lat=Y for Mercator Map
        //               double Lat,
        //               double Long,
        //               // Path to image
        //               String s1,
        //               // Title
        //               String s,
        //               // Scale of image
        //               double scl, int Ind
        //              )
        //{
        //    IMapJammerObject objectGroza;
        //    MapObjectStyle _placeObjectStyleOwn;
        //    Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
        //    // ------------------------------------------------------------------------------
        //    try
        //    {
        //        // ..........................................................................
        //        pointOwn.X = Long;
        //        pointOwn.Y = Lat;
        //        // ..........................................................................
        //        _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

        //                             //"1.png",
        //                             s1,
        //                             //(Bitmap)imageList1.Images[station.indzn],
        //                             scale: scl,
        //                             //objectOffset: new Offset(0, 0), // (-15,15) -> (влево,вверх)
        //                             objectOffset: new Offset(0, 0),  // смещение центра изображения
        //                             textOffset: new Offset(70, -30)
        //                             );
        //        objectGroza = AddMapJammerObject(_placeObjectStyleOwn, s, pointOwn, Ind);
        //        return "";
        //    }
        //    catch
        //    {
        //        if (GlobalVarMap.FlagLanguageMap == 1)
        //            return "Can't draw Image" + s1;
        //        else if (GlobalVarMap.FlagLanguageMap == 0)
        //            return "Невозможен рисунок изображения" + s1;
        //        else
        //            return "Şəkillər çəkmək mümkün deyil" + s1;
        //    }
        //}

        public string DrawImage(
                       // Degree (for Geographic Map)
                       // Long=X, Lat=Y for Mercator Map
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      )
        {
            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                pointOwn.X = Long;
                pointOwn.Y = Lat;
                // ..........................................................................
                _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

                                     //"1.png",
                                     s1,
                                     //(Bitmap)imageList1.Images[station.indzn],
                                     scale: scl,
                                     //objectOffset: new Offset(0, 0), // (-15,15) -> (влево,вверх)
                                     objectOffset: new Offset(0, 0),  // смещение центра изображения
                                     textOffset: new Offset(0, 15)
                                     );
                objectGrozaS1 = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Image" + s1;
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможен рисунок изображения" + s1;
                else
                    return "Şəkillər çəkmək mümkün deyil" + s1;
            }
        }

        public string DrawPointDistance(

               double Lat,
               double Long,
               // Path to image
               String s1,
               // Title
               String s,
               // Scale of image
               double scl,
               int Id
              )
        {
          
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                pointOwn.X = Long;
                pointOwn.Y = Lat;
                // ..........................................................................
                _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

                                     //"1.png",
                                     s1,
                                     //(Bitmap)imageList1.Images[station.indzn],
                                     scale: scl,
                                     //objectOffset: new Offset(0, 0), // (-15,15) -> (влево,вверх)
                                     objectOffset: new Offset(0, 0),  // смещение центра изображения
                                     textOffset: new Offset(0, 15)
                                     );
                var pointDistance = new PointDistanceToObj() { ID = Id };
                pointDistance.PointDistance = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);
                pointDistanceToObjs.Add(pointDistance);
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Image" + s1;
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможен рисунок изображения" + s1;
                else
                    return "Şəkillər çəkmək mümkün deyil" + s1;
            }
        }


        // **************************************************************************** DrawImage

        // Rotate image *************************************************************************
        public Bitmap RotateImage1(Bitmap input, double angle)
        {
            Bitmap result = new Bitmap(input.Width, input.Height);
            Graphics g = Graphics.FromImage(result);
            g.TranslateTransform((float)input.Width / 2, (float)input.Height / 2);
            g.RotateTransform((float)angle);
            g.TranslateTransform(-(float)input.Width / 2, -(float)input.Height / 2);
            g.DrawImage(input, new Point(0, 0));
            return result;
        }

        // ************************************************************************* Rotate image

        // DrawImageWithRotate ******************************************************************
        // Draw Image with rotate

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        public double DegToRad = 0.017453292519943295;

        private void TestStyle(String s1,
                       // Angle of rotate, degree
                       double angle,
                       // Title
                       String s,
                       // Scale of image
                       double scl)
        {
            Image im1 = RotateImage(s1, (float)angle);//, imagePlaneInitial);

            _placeObjectStylePlane = objRasterMapControl.LoadObjectStyle(
                                     (Bitmap)im1,
                                     scale: scl,
                                     objectOffset: new Offset(0, 0),
                                     textOffset: new Offset(0, 15)
                                     );
        }

        private List<MapObjectStyle> TestStyle_1(String s1,

                       // Scale of image
                       double scl)
        {
            List<MapObjectStyle> lst_placeObjectStyle = new List<MapObjectStyle>();

            try
            {
                using (System.IO.FileStream stream = new System.IO.FileStream(s1, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    imagePlaneInitial = System.Drawing.Image.FromStream(stream);
                }

                for (int kj = 0; kj <= 360; kj++)
                {
                    MapObjectStyle objMapObjectStyle = objRasterMapControl.LoadObjectStyle(
                                         (Bitmap)imagePlaneInitial,
                                         scale: scl,
                                         objectOffset: new Offset(0, 0),
                                         textOffset: new Offset(0, 15)
                                         );
                    lst_placeObjectStyle.Add(objMapObjectStyle);
                    lst_placeObjectStyle[kj].Style.SymbolRotation = kj;
                }
            }
            catch
            {
                return lst_placeObjectStyle;
            }

            return lst_placeObjectStyle;
        }

        public float NormalizeAngle(float angle)
        {
            angle %= 360;
            if (angle < 0)
                angle += 360;
            return angle;
        }

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public System.Drawing.Image RotateImage(string fileName, float angle)
        {
            System.Drawing.Image image = null;
            using (System.IO.FileStream stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                image = System.Drawing.Image.FromStream(stream);
            }

            angle = NormalizeAngle(angle);
            if (angle % 90 == 0)
                switch (angle)
                {
                    case 0:
                        return image;

                    case 90:
                        image.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                        return image;

                    case 180:
                        image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                        return image;

                    case 270:
                        image.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
                        return image;
                }

            float sin = (float)Math.Sin(angle % 90 * DegToRad);
            float cos = (float)Math.Cos(angle % 90 * DegToRad);
            float oldWidth = image.Width;
            float oldHeight = image.Height;
            float newWidth = 0f;
            float newHeight = 0f;
            float originX = 0f;
            float originY = 0f;

            if ((angle > 0 && angle < 90) || (angle > 180 && angle < 270))
            {
                newWidth = sin * oldHeight + cos * oldWidth;
                newHeight = sin * oldWidth + cos * oldHeight;

                if (angle < 90)
                {
                    originX = sin * oldHeight;
                    originY = 0f;
                }
                else
                {
                    originX = cos * oldWidth;
                    originY = newHeight;
                }
            }
            else if ((angle > 90 && angle < 180) || (angle > 270 && angle < 360))
            {
                newHeight = sin * oldHeight + cos * oldWidth;
                newWidth = sin * oldWidth + cos * oldHeight;
                if (angle < 180)
                {
                    originX = newWidth;
                    originY = sin * oldHeight;
                }
                else
                {
                    originX = 0f;
                    originY = cos * oldWidth;
                }
            }

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap((int)newWidth, (int)newHeight);
            bmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(bmp))
            {
                gr.TranslateTransform(originX, originY);
                gr.RotateTransform(angle);
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                gr.DrawImage(image, new System.Drawing.Point(0, 0));
            }

            return bmp;
        }

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public string DrawImageRotate(
                       // Degree (for Geographic Map)
                       // Long=X, Lat=Y for Mercator Map
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Angle of rotate, degree
                       double angle,
                       // Title
                       String s,
                       // Scale of image
                       double scl,
                       byte Select
                      )
        {
            // OLD ****************************************************************************

            /*
                                    //IMapObject objectGrozaS1;
                                    MapObjectStyle _placeObjectStyleOwn;
                                    Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
                                    // ------------------------------------------------------------------------------
                                    try
                                    {
                                        // ..........................................................................
                                        pointOwn.X = Long;
                                        pointOwn.Y = Lat;
                                        // ..........................................................................
                                        Image im1 = RotateImage(s1, (float)angle);

                                        _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(
                                                             (Bitmap)im1,
                                                             scale: scl,
                                                             objectOffset: new Offset(0, 0),
                                                             textOffset: new Offset(0, 15)
                                                             );

                                        GlobalVarMap.objectGrozaS1_L = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);

                                        return "";
                                    }
                                    catch
                                    {
                                        return "Can't draw Image";
                                    }
            */
            // **************************************************************************** OLD

            // NEW ****************************************************************************

            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                int index = -1;

                // ..........................................................................
                pointOwn.X = Long;
                pointOwn.Y = Lat;

                if (Select == 0)
                    objRasterMapControl.AddMapObject(lst_placeObjectStylePlane[(int)angle], s, pointOwn);
                else
                    objRasterMapControl.AddMapObject(lst_placeObjectStylePlaneSelect[(int)angle], s, pointOwn);

                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Image";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможен рисунок изображения";
                else
                    return "Şəkillər çəkmək mümkün deyil";
            }

            // **************************************************************************** NEW
        }

        // ****************************************************************** DrawImageWithRotate

        // Draw PolygonXY ***********************************************************************
        // Draw Polygon: points.X, points.Y -> Mercator

        public string DrawPolygonXY(List<Point> points, byte Color1, byte Color2, byte Color3, byte Color4)

        {
            if (points.Count == 0)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "No coordinates";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Нет координат";
                else
                    return "Koordinatlar yoxdur";
            }
            // ----------------------------------------------------------------------------------
            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

            double lat = 0;
            double lon = 0;
            // ----------------------------------------------------------------------------------
            for (int i = 0; i < points.Count; i++)
            {
                // !!! 1-й идет долгота, 2-й широта
                var p = SphericalMercator.ToLonLat(points[i].X, points[i].Y);
                lat = p.Y;
                lon = p.X;

                pointPel.Add(new Mapsui.Geometries.Point(lon, lat));
            } // FOR
            // ----------------------------------------------------------------------------------
            try
            {
                // 100,255,255,255 -> белый
                // Yellow            -> 100, 255, 255, (byte)0.25 * 255
                // Pink(розовый)     -> 100, 255, 0, 255
                // Red               -> 100, 255, 0, 0
                // Purple(сиреневый) -> 100, 0, 0, 255
                // Green             -> 100, 0, 255, 0
                objRasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(Color1, Color2, Color3, Color4));
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw polygon";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможно нарисовать полигон";
                else
                    return "Çoxbucaqlı çəkmək olmur";
            }
            // ----------------------------------------------------------------------------------
        } // DrawPolygonXY

        // *********************************************************************** Draw PolygonXY

        // Draw PolygonLatLong ******************************************************************
        // Draw Polygon: points.X=Long, points.Y=Lat -> degree

        public string DrawPolygonLatLong(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4)

        {
            if (pointPel.Count == 0)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "No coordinates";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Нет координат";
                else
                    return "Koordinatlar yoxdur";
            }
            // ----------------------------------------------------------------------------------
            try
            {
                // 100,255,255,255 -> белый
                // Yellow            -> 100, 255, 255, (byte)0.25 * 255
                // Pink(розовый)     -> 100, 255, 0, 255
                // Red               -> 100, 255, 0, 0
                // Purple(сиреневый) -> 100, 0, 0, 255
                // Green             -> 100, 0, 255, 0
                objRasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(Color1, Color2, Color3, Color4));
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw polygon";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможно нарисовать полигон";
                else
                    return "Çoxbucaqlı çəkmək olmur";
            }
            // ----------------------------------------------------------------------------------
        } // DrawPolygonLatLong

        // ****************************************************************** Draw PolygonLatLong

        // Draw LinesLatLong ******************************************************************
        // Draw Lines: points.X=Long, points.Y=Lat -> degree (for geographic map)
        // points.X=X, points.Y=Y -> for Mercator map

        public string DrawLinesLatLong(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4)

        {
            if (pointPel.Count == 0)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "No coordinates";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Нет координат";
                else
                    return "Koordinatlar yoxdur";
            }
            // ----------------------------------------------------------------------------------
            try
            {
                // 100,255,255,255 -> белый
                // Yellow            -> 100, 255, 255, (byte)0.25 * 255
                // Pink(розовый)     -> 100, 255, 0, 255
                // Red               -> 100, 255, 0, 0
                // Purple(сиреневый) -> 100, 0, 0, 255
                // Green             -> 100, 0, 255, 0
                // Blue              -> 100,0,0,255
                objRasterMapControl.AddPolyline(pointPel, Mapsui.Styles.Color.FromArgb(Color1, Color2, Color3, Color4), 2);
                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Lines";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможно нарисовать линии";
                else
                    return "Xətlər çəkmək olmur";
            }

        } // DrawLinesLatLong

        public string DrawLineDistanceToObj(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4, int Id)

        {
            if (pointPel.Count == 0)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "No coordinates";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Нет координат";
                else
                    return "Koordinatlar yoxdur";
            }
            // ----------------------------------------------------------------------------------
            try
            {

                var lineDistanceObj = new LineDistanceObj() { ID = Id };
                lineDistanceObj.LineDistance = objRasterMapControl.AddPolyline(pointPel, Mapsui.Styles.Color.FromArgb(Color1, Color2, Color3, Color4), 2);
                lineDistanceObjs.Add(lineDistanceObj);

                return "";
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw Lines";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможно нарисовать линии";
                else
                    return "Xətlər çəkmək olmur";
            }

        } // DrawLinesLatLong


        // ****************************************************************** Draw LinesLatLong

        // DrawSector ************************************************************************
        // Sector

        public string DrawSectorXY(
                                   Point tpCenterPoint,
                                   // Color
                                   byte clr1,
                                   byte clr2,
                                   byte clr3,
                                   byte clr4,
                                   // m
                                   long iRadiusZone,
                                   // degree
                                   float SectorLeft,
                                   float SectorRight
                                   )
        {
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                double xx = 0;
                double yy = 0;
                double lt = 0;
                double lng = 0;
                // ..........................................................................
                xx = tpCenterPoint.X;
                yy = tpCenterPoint.Y;
                // ..........................................................................
                var p = SphericalMercator.ToLonLat(xx, yy);
                lt = p.Y;
                lng = p.X;
                // ..........................................................................
                Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();
                point.Y = lt;
                point.X = lng;

                float fSectorLeft = SectorLeft;
                float fSectorRight = SectorRight;
                // ..........................................................................

                var points = objRasterMapControl.CreateSectorPoints(
                    point,
                    fSectorLeft,
                    fSectorRight,
                    (float)iRadiusZone);
                // ..........................................................................

                // 100,255,255,255 -> белый
                // Yellow            -> 100, 255, 255, (byte)0.25 * 255
                // Pink(розовый)     -> 100, 255, 0, 255
                // Red               -> 100, 255, 0, 0
                // Purple(сиреневый) -> 100, 0, 0, 255
                // Green             -> 100, 0, 255, 0
                objRasterMapControl.AddPolygon(points, Mapsui.Styles.Color.FromArgb(clr1, clr2, clr3, clr4));

                // ..........................................................................

                return "";
            }
            // ------------------------------------------------------------------------------
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw sector";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможно нарисовать сектор";
                else
                    return "Sektor çəkmək olmur";
            }
            // ------------------------------------------------------------------------------
        }

        // ************************************************************************ DrawSector

        // DrawSector ************************************************************************
        // Draw Sector LatLong: tpCenterPoint.X=Long, tpCenterPoint.Y=Lat -> degree

        public string DrawSectorLatLong(
                                   // Degree
                                   Mapsui.Geometries.Point tpCenterPoint,
                                   // Color
                                   byte clr1,
                                   byte clr2,
                                   byte clr3,
                                   byte clr4,
                                   // m
                                   long iRadiusZone,
                                   // degree
                                   float SectorLeft,
                                   float SectorRight
                                   )
        {
            // ------------------------------------------------------------------------------
            try
            {
                /*
                // ..........................................................................
                double xx = 0;
                double yy = 0;
                double lt = 0;
                double lng = 0;
                // ..........................................................................
                xx = tpCenterPoint.X;
                yy = tpCenterPoint.Y;
                // ..........................................................................
                var p = Mercator.ToLonLat(xx, yy);
                lt = p.Y;
                lng = p.X;
                // ..........................................................................
                Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();
                point.Y = lt;
                point.X = lng;

                float fSectorLeft = SectorLeft;
                float fSectorRight = SectorRight;
                */
                // ..........................................................................

                var points = objRasterMapControl.CreateSectorPoints(
                    //point,
                    tpCenterPoint,
                    SectorLeft,
                    SectorRight,
                    (float)iRadiusZone);
                // ..........................................................................

                // 100,255,255,255 -> белый
                // Yellow            -> 100, 255, 255, (byte)0.25 * 255
                // Pink(розовый)     -> 100, 255, 0, 255
                // Red               -> 100, 255, 0, 0
                // Purple(сиреневый) -> 100, 0, 0, 255
                // Green             -> 100, 0, 255, 0
                objRasterMapControl.AddPolygon(points, Mapsui.Styles.Color.FromArgb(clr1, clr2, clr3, clr4));

                // ..........................................................................

                return "";
            }
            // ------------------------------------------------------------------------------
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    return "Can't draw sector";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    return "Невозможно нарисовать сектор";
                else
                    return "Sektor çəkmək olmur";
            }
            // ------------------------------------------------------------------------------
        }

        // ************************************************************************ DrawSector

        // CalculationAzimuth ****************************************************************
        // Calculation of azimuth from Point1 to Point2

        public double CalcAzimuth(double Lat1, double Long1, double Lat2, double Long2)
        {
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double dX = 0;
            double dY = 0;
            double Beta = 0;
            double Beta_tmp = 0;
            // .......................................................................
            var p = SphericalMercator.FromLonLat(Long1, Lat1);
            x1 = p.X;
            y1 = p.Y;

            var p1 = SphericalMercator.FromLonLat(Long2, Lat2);
            x2 = p1.X;
            y2 = p1.Y;

            dX = x2 - x1;
            dY = y2 - y1;
            // .......................................................................

            // ------------------------------------------------------------------------
            if (dY != 0)
                Beta_tmp = Math.Atan(Math.Abs(dX) / Math.Abs(dY));
            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;
        }

        // **************************************************************** CalculationAzimuth
    } // Class
} // Namespace