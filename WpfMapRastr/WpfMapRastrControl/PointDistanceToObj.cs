﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace WpfMapRastrControl
{
    public class PointDistanceToObj
    {
        public IMapObject PointDistance { get; set; }

        public int ID { get; set; }

        public PointDistanceToObj()
        { }
    }
}
