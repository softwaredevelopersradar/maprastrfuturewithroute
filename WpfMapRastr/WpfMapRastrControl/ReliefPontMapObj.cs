﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace WpfMapRastrControl
{
    public class ReliefPontMapObj
    {
        public IMapObject ReliefPoint { get; set; }

        public int ID { get; set; }

        public ReliefPontMapObj()
        { }

    }
}
