﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastrControl
{
   public class LettersConverter
    {
 

        public static byte? DefineLetter(double? dFreq, byte CustomLeters)
        {
            return GetLetter(dFreq, CustomLeters);
        }

        private static byte? GetLetter(double? dFreq, byte Amplifiers)
        {
            byte bLetter = 0;

            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_1 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_2 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_3 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_4 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_5 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_6 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_7 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_8 & dFreq < GlobalVarMap.RangesLetters.FREQ_START_LETTER_9)
                bLetter = 8;
            switch (Amplifiers)
            {
                case 0:
                    if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_9 & dFreq <= GlobalVarMap.RangesLetters.FREQ_START_LETTER_10)
                        bLetter = 9;
                    if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_10 & dFreq <= GlobalVarMap.RangesLetters.FREQ_STOP_LETTER_10)
                        bLetter = 10;
                    break;

                case 1:
                    if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_9 & dFreq <= GlobalVarMap.RangesLetters.FREQ_START_LETTER_10_MODIF)
                        bLetter = 9;
                    if (dFreq >= GlobalVarMap.RangesLetters.FREQ_START_LETTER_10_MODIF & dFreq <= GlobalVarMap.RangesLetters.FREQ_STOP_LETTER_10)
                        bLetter = 10;
                    break;
            }

            return bLetter;
        }
    }


}

