﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelsTablesDBLib;

namespace WpfMapRastrControl
{
   public class IRIEventArgs : EventArgs
    {
        public TableReconFWS TableReconFws { get; set; }

        public IRIEventArgs(TableReconFWS _tableReconFws)
        {
            this.TableReconFws = _tableReconFws;
        }

    }

   public class IRISuppressEventArgs : EventArgs
   {
       public TableSuppressFWS TableSuppressFws { get; set; }

        public IRISuppressEventArgs(TableSuppressFWS _tableSuppressFws)
       {
           this.TableSuppressFws = _tableSuppressFws;
       }

   }
}
