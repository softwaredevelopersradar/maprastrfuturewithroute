﻿using Mapsui.Geometries;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace WpfMapRastrControl
{
    public interface IMapJammerObject
    {
        IFeature Feature { get; }

        MapObjectStyle Style { get; }

        Point Position { get; }

        int ID { get; set; }
    }
}
