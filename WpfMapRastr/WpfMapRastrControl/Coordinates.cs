﻿namespace WpfMapRastrControl
{
    public class Coordinates
    {
        public double Lon{ get; set; }
        public double Lat { get; set; }

        public int? Alt { get; set; }

        public Coordinates(double Lat, double Lon, int? Alt)
        {
            this.Lat = Lat;
            this.Lon = Lon;
            this.Alt = Alt;
        }

    }
}