﻿// ???

using ClassLibraryIniFiles;
using GeoCalculator;

// Semen
using Mapsui.Projection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ModelsTablesDBLib;
using WpfMapControl;

// Otl
// Для YAML реализации
// Для JSON реализации
// Для реализации XML

using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;
using System.ComponentModel;
using ReliefProfileControl;
using System.Runtime.CompilerServices;
using static Mapsui.Providers.ArcGIS.TileInfo;
using Mapsui.UI.Wpf;
using LosZoneLib;
using Mapsui.UI;
using System.Diagnostics;
using System.Linq;
using Mapsui.Providers;
using Bearing;
using System.Threading;
using System.Collections.ObjectModel;
using LineOfSightZoneControl.Models;
using LineOfSightZoneControl;

// 333
//using WpfTasksControl;

namespace WpfMapRastrControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl, INotifyPropertyChanged
    {
        //private bool flag;
        // Functions of map interface
        public ClassInterfaceMap objClassInterfaceMap { get; set; }

        // Functions of map interface for Control call
        private ClassMapRastrMenu objClassMapRastrMenu;

        // 777
        // Functions of map redraw
        //private ClassMapRastrReDraw objClassMapRastrReDraw;
        public ClassMapRastrReDraw objClassMapRastrReDraw;
       
        // 666
        public event EventHandler<ClassArg> OnMouseCl = (object sender, ClassArg data) => { };

        /// <summary>
        /// Get coord for add coord to DB for spoofing
        /// </summary>
        public event EventHandler<CoordEventArgs> OnGetCoordSpoof;

        public event EventHandler<IRIEventArgs> OnAddIRIToSuppres;
        public event EventHandler<IRISuppressEventArgs> DeleteIRISuppress;
        public event EventHandler<ClassArg> OnAddLOS;
        public event EventHandler<ReliefEventArgs> OnAddRelief;
        public Location ClickedCoord { get; set; }

        private LosZoneCalculator zoneCalculator = new LosZoneCalculator();

        public IReadOnlyList<IMapJammerObject> MapJammerObjects => _mapJammerObjects;
        private readonly List<IMapJammerObject> _mapJammerObjects = new List<IMapJammerObject>();
        public bool DistanceVisibility 
        {
            get => distanceVisibility;
            set
            {
                distanceVisibility = value;
                OnPropertyChanged();
            }
        }

        //public double AntennaJS
        //{
        //    get => antennaJS;
        //    set
        //    {
        //        antennaJS = value;
        //        OnPropertyChanged();
        //    }
        //}

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // *****************************************************************************************
        public UserControl1()
        {
            InitializeComponent();
            MapControl.MapType = MapType.OSM;
           

            //flag = false;
            objClassInterfaceMap = new ClassInterfaceMap(MapControl);
            
            objClassMapRastrMenu = new ClassMapRastrMenu(MapControl);
            objClassMapRastrReDraw = new ClassMapRastrReDraw(MapControl, objClassInterfaceMap);
            MapControl.MapClickEvent += MapControlOnMapClickEvent;
            
            string s = "";
            double scale = -1;

            // OpenMap -------------------------------------------------------------------------

            // Get path to map from .yaml file
            s = objClassMapRastrMenu.PathMapFile();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't open .yaml file");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно открыть .yaml файл");
                else
                    MessageBox.Show(".Yaml faylı aça bilmir");
                return;
            }

            objClassInterfaceMap.OpenMap(s);
            fChangeLanguage(GlobalVarMap.FlagLanguageMap);

            GlobalVarMap.MapOpened = objClassInterfaceMap.MapOpened;

           


            if (GlobalVarMap.MapOpened == false)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't load map");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно загрузить карту");
                else
                    MessageBox.Show("Xəritəni yükləmək olmur");

                return;
            }
            // ------------------------------------------------------------------------- OpenMap
          
            // OpenMatrix ----------------------------------------------------------------------

            // Get path to height matrix from .yaml file
            s = objClassMapRastrMenu.PathMatrixFile();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't open .yaml file");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно открыть .yaml файл");
                else
                    MessageBox.Show(".Yaml faylı aça bilmir");

                return;
            }

            objClassInterfaceMap.OpenMatrix(s);

            GlobalVarMap.MatrixOpened = objClassInterfaceMap.MatrixOpened;

            if (GlobalVarMap.MatrixOpened == false)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't load the height matrix");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно загрузить матрицу высот");
                else
                    MessageBox.Show("DEM yüklənmədi");

                //return;
            }
            // ---------------------------------------------------------------------- OpenMatrix

            // Scale ---------------------------------------------------------------------------

            // Get scale from .yaml file
            scale = objClassMapRastrMenu.ReadScaleFile();

            if (scale == -1)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't open .yaml file");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно открыть .yaml файл");
                else
                    MessageBox.Show(".Yaml faylı aça bilmir");

                return;
            }

            objClassInterfaceMap.SetScale(scale);

            GlobalVarMap.Scale = objClassInterfaceMap.Scale;

            if (GlobalVarMap.Scale == -1)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't set scale");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно установить масштаб");
                else
                    MessageBox.Show("Miqyas təyin etmək olmur");

                return;
            }
            var p = SphericalMercator.FromLonLat(27.55, 53.9);
           
            objClassInterfaceMap.CenterMapToXY(p.X, p.Y);

            objClassInterfaceMap.SetScale(152.87);
            

            // --------------------------------------------------------------------------- Scale
        }

        private void MapControlOnMapClickEvent(object sender, MapMouseClickEventArgs e)
        {
            ClickedCoord = e.Location;
        }

        // ***************************************************************************************** CONSTRUCTOR

        // ButtonPlus ******************************************************************************************
        // Increase scale

        private void ButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            bool bvar = false;

            bvar = objClassInterfaceMap.IncreaseScale();

            // The scale has been increased
            if (bvar == true)
            {
                GlobalVarMap.Scale = objClassInterfaceMap.Scale;
            } // The scale has been increased

            // The scale hasn't been increased
            else
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Impossible to increase the scale of map");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно увеличить масштаб карты");
                else
                    MessageBox.Show("Xəritədə böyütmək olmur");

                return;
            } // The scale hasn't been increased

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            string s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale
        } // ButtonPlus

        // ****************************************************************************************** ButtonPlus

        // ButtonMinus *****************************************************************************************
        // Decrease scale

        private void ButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            bool bvar = false;

            bvar = objClassInterfaceMap.DecreaseScale();

            // The scale has been decreased
            if (bvar == true)
            {
                GlobalVarMap.Scale = objClassInterfaceMap.Scale;
            } // The scale has been decreased

            // The scale hasn't been decreased
            else
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Impossible to decrease the scale of map");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно уменьшить масштаб карты");
                else
                    MessageBox.Show("Xəritəni böyütmək olmur");

                return;
            } // The scale hasn't been decreased

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            string s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale
        } // ButtonMinus

        // ***************************************************************************************** ButtonMinus

        // ButtonBase ******************************************************************************************
        // Base scale

        private void ButtonBase_Click(object sender, RoutedEventArgs e)
        {
            bool bvar = false;

            bvar = objClassInterfaceMap.BaseScale();

            // The base scale has been set
            if (bvar == true)
            {
                GlobalVarMap.Scale = objClassInterfaceMap.Scale;
            } // The base scale has been set

            // The base scale hasn't been set
            else
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Impossible to set the base scale");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно установить базовый масштаб");
                else
                    MessageBox.Show("Əsas miqyaslı təyin etmək mümkün deyil");

                return;
            } // The base scale hasn't been set

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            string s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale
        } // ButtonBase

        // ****************************************************************************************** ButtonBase

        // ButtonSenter ****************************************************************************************
        // Центрирование карты по координатам базовой станции
        // Map centring on base station coordinates
        // 999

        private void ButtonSenter_Click(object sender, RoutedEventArgs e)
        {
            // -------------------------------------------------------------------------------------------------
            bool bvar = false;
            double x = 0;
            double y = 0;
            double lat = 0;
            double lon = 0;
            int i = 0;
            // -------------------------------------------------------------------------------------------------
            // Otl
            // Для отладки здесь пока зададим конкретные координаты (из прошлого проекта)
            // Mercator/degree

            // Otl
            //lat = 53.78746;
            //lon = 25.71507;
            // 19.07
            //objClassInterfaceMap.List_JS[0].ISOwn = true;

            for (i = 0; i < objClassInterfaceMap.List_JS.Count; i++)
            {
                if (objClassInterfaceMap.List_JS[i].ISOwn == true)
                {
                    lat = objClassInterfaceMap.List_JS[i].Coordinates.Latitude;
                    lon = objClassInterfaceMap.List_JS[i].Coordinates.Longitude;
                    i = objClassInterfaceMap.List_JS.Count + 1;
                }
            }

            var p = SphericalMercator.FromLonLat(lon, lat);
            x = p.X;
            y = p.Y;
            // -------------------------------------------------------------------------------------------------
            // MercatorMap or GeographicMap?

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------
            // MercatorMap

            if (initMapYaml.TypeMap == 0)
            {
                // Center map on pozition XY (Mercator)
                bvar = objClassInterfaceMap.CenterMapToXY(x, y);
            } // MercatorMap
            // -------------------------------------------------------------------------------------------------
            // GeographicMap
            else
            {
                // Center map on pozition LatLong (degree)
                bvar = objClassInterfaceMap.CenterMapToLatLong(lat, lon);
            } // GeographicMap
            // -------------------------------------------------------------------------------------------------
            if (bvar == false)
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't center map on this pozition");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно сцентрировать карту по этой позиции");
                else
                    MessageBox.Show("Xəritəni bu vəziyyətdə mərkəzləşdirmək mümkün deyil");

                return;
            } // The base scale has been set
            // -------------------------------------------------------------------------------------------------
        } // ButtonSenter

        // **************************************************************************************** ButtonSenter

        // OpenMap_Menu ****************************************************************************************
        // Open map from dialog window

        private void MenuItemOpen_Click(object sender, RoutedEventArgs e)
        {
            string s = "";

            // ................................................................................................
            // Path to map from dialog window
            s = objClassMapRastrMenu.PathDialog();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't get map path from dialog window");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно получить путь к карте из диалогового окна");
                else
                    MessageBox.Show("Dialoqdan xəritə yolu əldə edilə bilmir");

                return;
            }
            // ................................................................................................

            // Map wasn't opened
            if (GlobalVarMap.MapOpened == false)
            {
                // Open map
                objClassInterfaceMap.OpenMap(s);
                GlobalVarMap.MapOpened = objClassInterfaceMap.MapOpened;

                if (GlobalVarMap.MapOpened == false)
                {
                    if (GlobalVarMap.FlagLanguageMap == 1)
                        MessageBox.Show("Can't load map");
                    else if (GlobalVarMap.FlagLanguageMap == 0)
                        MessageBox.Show("Невозможно загрузить карту");
                    else
                        MessageBox.Show("Xəritəni yükləmək olmur");

                    return;
                }
            } // Map wasn't opened

            // Map was opened
            else
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Map is opened");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Карта уже открыта");
                else
                    MessageBox.Show("Xəritə artıq açıqdır");

                return;
            } // else (Map was opened)
            // ................................................................................................
        }

        // **************************************************************************************** OpenMap_Menu

        // OpenMatrix_Menu *************************************************************************************
        private void MenuItemMatr_Click(object sender, RoutedEventArgs e)
        {
            string s = "";

            // ................................................................................................
            // Path to the height matrix from dialog window

            s = objClassMapRastrMenu.PathDialog();

            if (s == "")
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't get height matrix path from dialog window");
                else if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Невозможно получить путь к матрице высот из диалогового окна");
                else
                    MessageBox.Show("DEM qutusundan informasiya qutusundan yol alınmadı");

                return;
            }
            // ................................................................................................

            // Matrix wasn't opened
            if (GlobalVarMap.MatrixOpened == false)
            {
                // Open Matrix
                objClassInterfaceMap.OpenMatrix(s);
                GlobalVarMap.MatrixOpened = objClassInterfaceMap.MatrixOpened;

                if (GlobalVarMap.MatrixOpened == false)
                {
                    if (GlobalVarMap.FlagLanguageMap == 1)
                        MessageBox.Show("Can't load the height matrix");
                    else if (GlobalVarMap.FlagLanguageMap == 0)
                        MessageBox.Show("Невозможно загрузить матрицу высот");
                    else
                        MessageBox.Show("DEM yüklənmədi");

                    return;
                }
            } // Matrix wasn't opened

            // Matrix was opened
            else
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("The height matrix is opened");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Матрица высот уже загружена");
                else
                    MessageBox.Show("DEM artıq yüklənib");

                return;
            } // else (Matrix was opened)
            // ................................................................................................
        }

        // ************************************************************************************* OpenMatrix_Menu

        // CloseMapMatr_Menu ***********************************************************************************
        // Close map, matrix, save current scale

        private void MenuItemClose_Click(object sender, RoutedEventArgs e)
        {
            string s = "";

            // CloseMap >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (GlobalVarMap.MapOpened == true)
            {
                objClassInterfaceMap.CloseMap();
                GlobalVarMap.MapOpened = objClassInterfaceMap.MapOpened;

                if (GlobalVarMap.MapOpened == true)
                {
                    if (GlobalVarMap.FlagLanguageMap == 1)
                        MessageBox.Show("Can't close map");
                    else if (GlobalVarMap.FlagLanguageMap == 0)
                        MessageBox.Show("Невозможно закрыть карту");
                    else
                        MessageBox.Show("Xəritəni bağlamaq olmur");

                    return;
                }
            } // if(GlobalVarMap.MapOpened == true)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CloseMap

            // CloseMatrix >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (GlobalVarMap.MatrixOpened == true)
            {
                objClassInterfaceMap.CloseMatrix();
                GlobalVarMap.MatrixOpened = objClassInterfaceMap.MatrixOpened;

                if (GlobalVarMap.MatrixOpened == true)
                {
                    if (GlobalVarMap.FlagLanguageMap == 1)
                        MessageBox.Show("Can't close the height matrix");
                    else if (GlobalVarMap.FlagLanguageMap == 0)
                        MessageBox.Show("Невозможно закрыть матрицу высот");
                    else
                        MessageBox.Show("DEM-i bağlamaq mümkün deyil");

                    return;
                }
            } // if(GlobalVarMap.MatrixOpened == true)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CloseMatrix

            // Scale >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Save current scale to . yaml file

            s = objClassMapRastrMenu.WriteScaleFile();

            if (s != "")
                MessageBox.Show(s);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Scale
        }

        // *********************************************************************************** CloseMapMatr_Menu

        // MOUSE_MOVE ******************************************************************************************
        // Element MapControl event: mouse motion on the map
        // 999

        private async void MapControl_MapMouseMoveEvent(object sender, Location e)
        {
            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------

            // .................................................................................................
            // File .yaml was read

            double R = 0.009;
            List<ClassJS> List_JS1 = new List<ClassJS>(objClassInterfaceMap.List_JS);
            List<TableSectorsRangesRecon> List_SectorsRangesRecon =
                new List<TableSectorsRangesRecon>(objClassInterfaceMap.List_SectorsRangesRecon);
            List<TableReconFWS> List_IRIFRCH_TD = new List<TableReconFWS>(objClassInterfaceMap.List_SRW_FRF_TD);
            List<TableSuppressFWS> List_IRIFRCH_Supress = new List<TableSuppressFWS>(objClassInterfaceMap.List_SRW_FRF_RS);
            try
            {
                /////////////////////////////////////////////////////////////////Информация об ИРИ ФРЧ Ц
                if (List_IRIFRCH_TD.Count != 0)
                {
                    for (int i = 0; i < List_IRIFRCH_TD.Count; i++) {
                        if (Math.Pow(e.Latitude - List_IRIFRCH_TD[i].Coordinates.Latitude, 2) +
                            Math.Pow(e.Longitude - List_IRIFRCH_TD[i].Coordinates.Longitude, 2) <= Math.Pow(R, 2))
                        {
                            var Alt = (int)MapControl.DtedMass.GetElevation(List_IRIFRCH_TD[i].Coordinates.Longitude, List_IRIFRCH_TD[i].Coordinates.Latitude);
                            Label labelIRIFRCH = new Label();
                            labelIRIFRCH.Content = "Координаты ИРИ: " + "\n" + "Широта: " +
                                               Convert.ToString(objClassInterfaceMap.List_SRW_FRF_TD[i].Coordinates.Latitude) +
                                               " ˚" + "\n" + "Долгота: " +
                                               Convert.ToString(objClassInterfaceMap.List_SRW_FRF_TD[i].Coordinates.Longitude) +
                                               " ˚" + "\n" + "Высота: " +
                                               Convert.ToString(Alt) + " м";

                            objClassMapRastrReDraw.ReDraw_SRW_FRF_TD_WithInfo(labelIRIFRCH, List_IRIFRCH_TD[i].Coordinates.Latitude, List_IRIFRCH_TD[i].Coordinates.Longitude);
                            await Task.Delay(2000);
                            objClassMapRastrReDraw.ReDrawMapAll();
                        }
                    }
                }
                /////////////////////////////////////////////////////////////////Информация об ИРИ ФРЧ Ц
                
                /////////////////////////////////////////////////////////////////Информация об ИРИ ФРЧ РП
                if (List_IRIFRCH_Supress.Count != 0)
                {
                    for (int i = 0; i < List_IRIFRCH_Supress.Count; i++)
                    {
                        if (Math.Pow(e.Latitude - List_IRIFRCH_Supress[i].Coordinates.Latitude, 2) +
                            Math.Pow(e.Longitude - List_IRIFRCH_Supress[i].Coordinates.Longitude, 2) <= Math.Pow(R, 2))
                        {
                            var Alt = (int)MapControl.DtedMass.GetElevation(List_IRIFRCH_Supress[i].Coordinates.Longitude, List_IRIFRCH_Supress[i].Coordinates.Latitude);
                            Label labelIRIFRCH_Supress = new Label();
                            labelIRIFRCH_Supress.Content = "Координаты ИРИ: " + "\n" + "Широта: " +
                                                           Convert.ToString(objClassInterfaceMap.List_SRW_FRF_RS[i].Coordinates.Latitude) +
                                                           " ˚" + "\n" + "Долгота: " +
                                                           Convert.ToString(objClassInterfaceMap.List_SRW_FRF_RS[i].Coordinates.Longitude) +
                                                           " ˚" + "\n" + "Высота: " +
                                                           Convert.ToString(Alt) + " м" + 
                                                            "\n" + "Литера: " + Convert.ToString(objClassInterfaceMap.List_SRW_FRF_RS[i].Letter) + "\n" + "Приоритет: "
                                                            + Convert.ToString(objClassInterfaceMap.List_SRW_FRF_RS[i].Priority);

                            objClassMapRastrReDraw.ReDraw_SRW_FRF_RS_WithInfo(labelIRIFRCH_Supress, List_IRIFRCH_Supress[i].Coordinates.Latitude, List_IRIFRCH_Supress[i].Coordinates.Longitude);
                            await Task.Delay(2000);
                            objClassMapRastrReDraw.ReDrawMapAll();
                        }
                    }
                }
                /////////////////////////////////////////////////////////////////Информация об ИРИ ФРЧ РПЦ


                if (List_JS1.Count == 1)
                {
                    if (Math.Pow(e.Latitude - List_JS1[0].Coordinates.Latitude, 2) +
                        Math.Pow(e.Longitude - List_JS1[0].Coordinates.Longitude, 2) <= Math.Pow(R, 2))
                    {
                        var Alt = (int)MapControl.DtedMass.GetElevation(objClassInterfaceMap.List_JS[0].Coordinates.Longitude, objClassInterfaceMap.List_JS[0].Coordinates.Latitude);
                        Label labelOwn = new Label();
                        labelOwn.Content = "Координаты СП: " + "\n" + "Широта: " +
                                           Convert.ToString(objClassInterfaceMap.List_JS[0].Coordinates.Latitude) +
                                           " ˚" + "\n" + "Долгота: " +
                                           Convert.ToString(objClassInterfaceMap.List_JS[0].Coordinates.Longitude) +
                                           " ˚" + "\n" + "Высота: " +
                                           Convert.ToString(Alt) + " м";

                        objClassMapRastrReDraw.ReDrawJSWithInfo(labelOwn, null);
                        await Task.Delay(2000); 
                        objClassMapRastrReDraw.ReDrawMapAll();
                    } 
                }
                else if (List_JS1.Count == 2)
                    {
                             if (Math.Pow(e.Latitude - List_JS1[0].Coordinates.Latitude, 2) + Math.Pow(e.Longitude - List_JS1[0].Coordinates.Longitude, 2) <= Math.Pow(R, 2))
                             {
                                 var Alt = (int)MapControl.DtedMass.GetElevation(objClassInterfaceMap.List_JS[0].Coordinates.Longitude, objClassInterfaceMap.List_JS[0].Coordinates.Latitude);
                                 Label labelOwn = new Label();
                                 labelOwn.Content = 
                                                    "Координаты СП: " + "\n" + "Широта: " + 
                                                    Convert.ToString(objClassInterfaceMap.List_JS[0].Coordinates.Latitude) + " ˚"
                                                    + "\n" + "Долгота: " + 
                                                    Convert.ToString(objClassInterfaceMap.List_JS[0].Coordinates.Longitude) + " ˚"
                                                    + "\n" + "Высота: " + 
                                                    Convert.ToString(Alt) + " м";

                                 objClassMapRastrReDraw.ReDrawJSWithInfo(labelOwn, null);
                                 await Task.Delay(2000);
                                 objClassMapRastrReDraw.DeleteJSWithInfo(List_JS1[0].Id);
                        
                                 //var element = _mapJammerObjects.FirstOrDefault(t => t.ID == 111); 
                                 //if(element != null) 
                                 //{ 
                                 //   RemoveObject(element);
                                 //}
                                 
                                 //objClassMapRastrReDraw.ReDrawMapAll();
                             }

                             if (Math.Pow(e.Latitude - List_JS1[1].Coordinates.Latitude, 2) +
                                 Math.Pow(e.Longitude - List_JS1[1].Coordinates.Longitude, 2) <= Math.Pow(R, 2))
                             {
                                 var Alt = (int)MapControl.DtedMass.GetElevation(objClassInterfaceMap.List_JS[1].Coordinates.Longitude, objClassInterfaceMap.List_JS[1].Coordinates.Latitude);
                                 Label labelSlave = new Label();
                                 labelSlave.Content =
                                                     "Координаты СП: " + "\n" + "Широта: " +
                                                     Convert.ToString(objClassInterfaceMap.List_JS[1].Coordinates.Latitude) + " ˚"
                                                     + "\n" + "Долгота: " +
                                                     Convert.ToString(objClassInterfaceMap.List_JS[1].Coordinates.Longitude) + " ˚" 
                                                     + "\n" + "Высота: " +
                                                     Convert.ToString(Alt) + " м";

                                 objClassMapRastrReDraw.ReDrawJSWithInfo(null, labelSlave);
                                 await Task.Delay(2000);
                                 objClassMapRastrReDraw.DeleteJSWithInfo(List_JS1[1].Id);
                             }
                             
                    }
                

            }
            catch (Exception exception)
            {
              
            }
            

            if (s == "")
            {
                if (initMapYaml.TypeMap == 0)
                {
                    // MERCATOR_MAP **********************************************************************************

                    if ((e.Latitude != null) && (e.Longitude != null))

                    {
                        try
                        {
                            GlobalVarMap.X_Rastr = e.Longitude;
                            GlobalVarMap.Y_Rastr = e.Latitude;
                            GlobalVarMap.H_Rastr = (double)e.Altitude;
                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == 1)
                                MessageBox.Show("Unable to determine map coordinates");
                            else if (GlobalVarMap.FlagLanguageMap == 0)
                                MessageBox.Show("Невозможно определить координаты карты");
                            else
                                MessageBox.Show("Xəritə koordinatlarını təyin etmək mümkün deyil");

                            return;
                        }

                        // Converting to LatLong
                        try
                        {
                            var p = Mercator.ToLonLat(GlobalVarMap.X_Rastr, GlobalVarMap.Y_Rastr);
                            GlobalVarMap.LAT_Rastr = GlobalVarMap.Y_Rastr; //p.Y
                            GlobalVarMap.LONG_Rastr = GlobalVarMap.X_Rastr; //p.X

                            if (GlobalVarMap.LAT_Rastr < -90)
                                GlobalVarMap.LAT_Rastr = -90;
                            if (GlobalVarMap.LAT_Rastr > 90)
                                GlobalVarMap.LAT_Rastr = 90;

                            if (GlobalVarMap.LONG_Rastr < -180)
                                GlobalVarMap.LONG_Rastr = -180;
                            if (GlobalVarMap.LONG_Rastr > 180)
                                GlobalVarMap.LONG_Rastr = 180;
                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == 1)
                                MessageBox.Show("Unable to determine Latitude and Longitude coordinates");
                            else if (GlobalVarMap.FlagLanguageMap == 0)
                                MessageBox.Show("Невозможно определить широту и долготу");
                            else
                                MessageBox.Show("Enlem ve boylam təyin oluna bilmir");

                            return;
                        }
                    } // (e.Latitude != null) && (e.Longitude != null) && (e.Altitude != null)
                      // -------------------------------------------------------------------------------------------------

                    DrawCurrentCoordinates();
                } // TypeMap==0
                // ********************************************************************************** MERCATOR_MAP
                else
                {
                    // GEOGRAPHICS_MAP ***********************************************************************************

                    // -------------------------------------------------------------------------------------------------
                    // Current map coordinates
                    // !!! Хотя за пределами карты долгота и широта показывают, а высота==null

                    // !!! Vernut
                    //if ((e.Latitude != null) && (e.Longitude != null) && (e.Altitude != null))
                    if ((e.Latitude != null) && (e.Longitude != null))

                    {
                        try
                        {
                            GlobalVarMap.LAT_Rastr = e.Latitude;
                            GlobalVarMap.LONG_Rastr = e.Longitude;
                            // GlobalVarMap.H_Rastr = (double)e.Altitude;

                            if (GlobalVarMap.LAT_Rastr < -90)
                                GlobalVarMap.LAT_Rastr = -90;
                            if (GlobalVarMap.LAT_Rastr > 90)
                                GlobalVarMap.LAT_Rastr = 90;

                            if (GlobalVarMap.LONG_Rastr < -180)
                                GlobalVarMap.LONG_Rastr = -180;
                            if (GlobalVarMap.LONG_Rastr > 180)
                                GlobalVarMap.LONG_Rastr = 180;
                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == 1)
                                MessageBox.Show("Unable to determine map coordinates");
                            else if (GlobalVarMap.FlagLanguageMap == 0)
                                MessageBox.Show("Невозможно определить координаты карты");
                            else
                                MessageBox.Show("Xəritə koordinatlarını təyin etmək mümkün deyil");

                            return;
                        }

                        // Converting to Merkator
                        try
                        {
                            var p = SphericalMercator.FromLonLat(e.Longitude, e.Latitude);
                            GlobalVarMap.X_Rastr = p.X;
                            GlobalVarMap.Y_Rastr = p.Y;
                        }
                        catch
                        {
                            if (GlobalVarMap.FlagLanguageMap == 1)
                                MessageBox.Show("Unable to determine Merkator coordinates");
                            else if (GlobalVarMap.FlagLanguageMap == 0)
                                MessageBox.Show("Невозможно определить координаты Меркатора");
                            else
                                MessageBox.Show("Mercatorun koordinatlarını təyin etmək mümkün deyil");

                            return;
                        }
                    } // (e.Latitude != null) && (e.Longitude != null) && (e.Altitude != null)
                      // -------------------------------------------------------------------------------------------------

                    DrawCurrentCoordinates();

                    // *********************************************************************************** GEOGRAPHICS_MAP
                } // TypeMap=1
            } // IF( Файл .yaml was read)
              // ...........................................................................
              // File .yaml wasn't read
            else
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Can't read .yaml file");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Невозможно прочитать .yaml файл");
                else
                    MessageBox.Show(".Yaml faylı oxunmur");
            } // ELSE (// File .yaml wasn't read)
              // ...........................................................................
        }

        // ****************************************************************************************** MOUSE_MOVE

        // MAP_CLICK *******************************************************************************************
        // Element MapControl event: mouse click on the map

        public void MapControl_MapClickEvent(object sender, MapMouseClickEventArgs e)
        //public void MapControl_MapRightClickEvent(object sender, MapMouseClickEventArgs e)
        {
            var p = e.ClickedButton.ChangedButton;

            if (p == MouseButton.Right)
            {
                objClassInterfaceMap.FlMouse = true;

                // ?????????????????????
                if (objClassInterfaceMap.Show_S == false)
                {
                    //MapControl.ContextMenu.IsEnabled=false;
                    //MapControl.ContextMenu.Visibility
                }

                GlobalVarMap.LAT_Rastr_Right = GlobalVarMap.LAT_Rastr;
                GlobalVarMap.LONG_Rastr_Right = GlobalVarMap.LONG_Rastr;
                GlobalVarMap.H_Rastr_Right = GlobalVarMap.H_Rastr;
            }
            else
            {
                objClassInterfaceMap.FlMouse = false;
            }

            // Преоывание для передачи в головную программу
            OnMouseCl(this, new ClassArg(GlobalVarMap.LAT_Rastr, GlobalVarMap.LONG_Rastr, GlobalVarMap.H_Rastr,
                                         GlobalVarMap.X_Rastr, GlobalVarMap.Y_Rastr, GlobalVarMap.Str_Rastr));
        }

        // ******************************************************************************************* MAP_CLICK

        // checkedBoxS_Checked *********************************************************************************
        // S from base station

        private bool distanceVisibility = false;
        public double antennaJS = 16;
        public double antennaEnemyObj = 3;
        private void checkBoxSSP_Checked(object sender, RoutedEventArgs e)
        {
            objClassInterfaceMap.Show_S = true;
            GlobalVarMap.ReliefControlOn = true;
            this.DistanceVisibility = true;
            objClassMapRastrReDraw.ReDrawMapAll();
        }

        // ********************************************************************************* checkedBoxS_Checked

        // checkedBoxS_Unchecked *******************************************************************************

        private void checkBoxSSP_Unchecked(object sender, RoutedEventArgs e)
        {
            objClassInterfaceMap.Show_S = false;
            GlobalVarMap.ReliefControlOn = false;
            this.DistanceVisibility = false;
            
            objClassMapRastrReDraw.ReDrawMapAll();
        }

        // ******************************************************************************* checkedBoxS_Unchecked

        // *****************************************************************************************************
        // Клик по контекстному меню при определении расстояния до объекта
      
        private  void SetDistanceMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (
                (objClassInterfaceMap.ShowJS == true) &&
                (objClassInterfaceMap.Show_S == true) &&
                (objClassInterfaceMap.FlMouse == true) // Нажата правая кнопка мыши
               )
            {
                // .....................................................................................
                // Source coordinates

                objClassInterfaceMap.LatSource_S = GlobalVarMap.LAT_Rastr_Right;
                objClassInterfaceMap.LongSource_S = GlobalVarMap.LONG_Rastr_Right;
                objClassInterfaceMap.HSource_S = GlobalVarMap.H_Rastr_Right;
                // .....................................................................................
                // !!! Расчет идет в перерисовке


                objClassMapRastrReDraw.DeletePointDistance(1);
                objClassMapRastrReDraw.DeleteLineDistance(2);
                objClassMapRastrReDraw.ReDrawS();

                List<Coordinates> list = new List<Coordinates>(objClassMapRastrReDraw.listAlt); // без учета кривизны земли
                List<Coordinates> newList = new List<Coordinates>(); // с учетом кривизны земли с поправками к высотам рельефа

                var CoordDistanceStart = new Mapsui.Geometries.Point();
                if (list.Count != 0) 
                {
                    CoordDistanceStart = SphericalMercator.ToLonLat(list[0].Lat, list[0].Lon);
                    newList.Add(new Coordinates(list[0].Lat, list[0].Lon, list[0].Alt));

                }

                for (int i = 1; i < list.Count; i++) // new
                {
                   
                    var CoordDistanceStop = SphericalMercator.ToLonLat(list[i].Lat, list[i].Lon);
                    var distance = ClassBearing.f_D_2Points(CoordDistanceStart.Y, CoordDistanceStart.X, CoordDistanceStop.Y, CoordDistanceStop.X, 1) / 1000; //дистанция до точки, длина дуги в км 
                    var R = 6373;
                    double Alfa = (180 * distance) / (Math.PI * R); // расчет угла
                    var Tang = Math.Tan(Alfa * (Math.PI / 180)); // тангенс угла
                    var S = R *Tang;
                    var L = Math.Sqrt(Math.Pow(R, 2) + Math.Pow(S, 2));
                    var Correction = L - R;
                    if (list[i].Alt - (Correction * 1000) < 0) 
                    {
                        newList.Add(new Coordinates(list[i].Lat, list[i].Lon, 0));
                    } else
                    newList.Add(new Coordinates (list[i].Lat, list[i].Lon, list[i].Alt - (int)(Correction * 1000)));
                }

                
                OnAddRelief?.Invoke(sender, new ReliefEventArgs(newList, antennaJS, antennaEnemyObj)); // потом брать высоты обьектов с контрола ЗПВ

                // .....................................................................................
            }  // Show_S==true && ShowJS==true
        }

        // *****************************************************************************************************

        // checkedBoxJS_Checked ********************************************************************************
        private void checkBoxJS_Checked(object sender, RoutedEventArgs e)
        {
            // DrawJS ------------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowJS = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DrawJS
        }

        // ******************************************************************************** checkedBoxJS_Checked

        // checkedBoxJS_Unchecked ******************************************************************************

        private void checkBoxJS_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelJS ------------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowJS = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelJS
        }

        // ****************************************************************************** checkedBoxJS_Unchecked

        // checkedBoxAntennas_Checked **************************************************************************
        private void checkBoxAntennas_Checked(object sender, RoutedEventArgs e)
        {
            // DrawAntennas ------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowAntennas = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------ DrawAntennas
        }

        // ************************************************************************** checkedBoxAntennas_Checked

        // checkedBoxAntennas_Unchecked ************************************************************************

        private void checkBoxAntennas_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelAntennas -------------------------------------------------------------------------------------
            objClassInterfaceMap.ShowAntennas = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------- DelAntennas
        }

        // ************************************************************************ checkedBoxAntennas_Unchecked

        // checkBoxObj1_Checked ********************************************************************************
        // ИРИ ФРЧ

        private void checkBoxObj1_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj1 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj1
        }

        // checkBoxObj1_Checked ********************************************************************************

        // checkBoxObj1_Unchecked ******************************************************************************
        private void checkBoxObj1_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelObj1 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj1
        }

        // ****************************************************************************** checkBoxObj1_Unchecked

        // checkBox_SRW_FRF_TD_Checked ********************************************************************************
        // ИРИ ФРЧ ЦР

        private void checkBoxObj2_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj2 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_TD = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj2
        }

        // ******************************************************************************** checkBoxObj2_Checked

        // checkBoxObj2_Unchecked ******************************************************************************

        private void checkBoxObj2_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb2 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_TD = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj2
        }

        // ****************************************************************************** checkBoxObj2_Unchecked

        // checkBox_SRW_FRF_RS_Checked ********************************************************************************
        // ИРИ ФРЧ РП

        private void checkBoxObj3_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj3 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_RS = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj3
        }

        // ******************************************************************************** checkBox_SRW_FRF_RS_Checked

        // checkBoxObj3_Unchecked ******************************************************************************

        private void checkBoxObj3_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb3 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_FRF_RS = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj3
        }

        // ****************************************************************************** checkBoxObj3_Unchecked

        // checkBox_SRW_STRF_Checked ***************************************************************************

        private void checkBoxObj4_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj4 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj4
        }

        // *************************************************************************** checkBox_SRW_STRF_Checked

        // checkBoxObj4_Unchecked ******************************************************************************

        private void checkBoxObj4_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb4 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj4
        }

        // ****************************************************************************** checkBoxObj4_Unchecked

        // checkBox_SRW_STRF_RS Checked ************************************************************************
        // Сейчас это пеленги

        private void checkBoxObj5_Checked(object sender, RoutedEventArgs e)
        {
            // DrawObj5 -----------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF_RS = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ----------------------------------------------------------------------------------------- DrawObj5
        }

        // ************************************************************************ checkBox_SRW_STRF_RS Checked

        // checkBoxObj5_Unchecked ******************************************************************************

        private void checkBoxObj5_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelOb5 ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_SRW_STRF_RS = false;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelObj5
        }

        // ****************************************************************************** checkBoxObj5_Unchecked

        // checkBox_Airplanes Checked **************************************************************************

        private void checkBoxAirplanes_Checked(object sender, RoutedEventArgs e)
        {
            // DrawAP ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_AP = true;
            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DrawAP
        }

        // ************************************************************************** checkBox_Airplanes Checked

        // checkBoxAirplanes_Unchecked *************************************************************************

        private void checkBoxAirplanes_Unchecked(object sender, RoutedEventArgs e)
        {
            // DelAP ------------------------------------------------------------------------------------------
            objClassInterfaceMap.Show_AP = false;

            objClassMapRastrReDraw.ReDrawMapAll();
            // ------------------------------------------------------------------------------------------ DelAP
            Txt10.Text = "";
        }

        // ************************************************************************* checkBoxAirplanes_Unchecked

        // INTERFACE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Functions of interface

        // OpenMap ******************************************************************************
        // Open Map (path - path to map: from dialog or from ini file)

        public void OpenMap(string pathMap)
        {
            objClassInterfaceMap.OpenMap(pathMap);
        }

        // ****************************************************************************** OpenMap

        // OpenMatrix ***************************************************************************
        // Open Matrix (path - path to Matrix: from dialog or from ini file)

        public void OpenMatrix(string pathMatrix)
        {
            objClassInterfaceMap.OpenMatrix(pathMatrix);
        }

        // *************************************************************************** OpenMatrix

        // SetScale *****************************************************************************

        public void SetScale(double scale)
        {
            objClassInterfaceMap.SetScale(scale);
        }

        // ***************************************************************************** SetScale

        // CloseMap *****************************************************************************
        // Close Map

        public void CloseMap()
        {
            objClassInterfaceMap.CloseMap();
        }

        // ***************************************************************************** CloseMap

        // CloseMatrix **************************************************************************
        // Close the height matrix

        public void CloseMatrix()
        {
            objClassInterfaceMap.CloseMatrix();
        }

        // ************************************************************************** CloseMatrix

        // Increase the scale *******************************************************************

        public bool IncreaseScale()
        {
            bool bvar = objClassInterfaceMap.IncreaseScale();
            return bvar;
        }

        // ******************************************************************* Increase the scale

        // Decrease the scale *******************************************************************

        public bool DecreaseScale()
        {
            bool bvar = objClassInterfaceMap.DecreaseScale();
            return bvar;
        }

        // ******************************************************************* Decrease the scale

        // Base scale ***************************************************************************

        public bool BaseScale()
        {
            bool bvar = objClassInterfaceMap.BaseScale();
            return bvar;
        }

        // *************************************************************************** Base scale

        // CenterMapToXY ************************************************************************
        // Center the map on pozition XY (Mercator)

        public bool CenterMapToXY(double x, double y)
        {
            bool bvar = objClassInterfaceMap.CenterMapToXY(x, y);
            return bvar;
        }

        // ************************************************************************ CenterMapToXY

        // CenterMapToLatLong *******************************************************************
        // Center the map on pozition Lat,Long (degree)

        public bool CenterMapToLatLong(double Lat, double Long)
        {
            bool bvar = objClassInterfaceMap.CenterMapToLatLong(Lat, Long);
            return bvar;
        }

        // ******************************************************************* CenterMapToLatLong

        // DrawImage ****************************************************************************
        // Draw Image with path to image

        public string DrawImage(
                       // Degree
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      )
        {
            string svar = objClassInterfaceMap.DrawImage(Lat, Long, s1, s, scl);
            return svar;
        }

        // **************************************************************************** DrawImage

        // Rotate image ************************************************************************
        public Bitmap RotateImage1(Bitmap input, double angle)
        {
            Bitmap svar = objClassInterfaceMap.RotateImage1(input, angle);
            return svar;
        }

        // *********************************************************************** Rotate image

        // DrawImageRotate ********************************************************************
        // Draw Image with rotate

        public string DrawImageRotate(
                       // Degree
                       double Lat,
                       double Long,
                       // Path to image
                       String s1,
                       // Angle of rotate, degree
                       double angle,
                       // Title
                       String s,
                       // Scale of image
                       double scl
                      )
        {
            string svar = objClassInterfaceMap.DrawImageRotate(Lat, Long, s1, angle, s, scl, 0);
            return svar;
        }

        // ******************************************************************** DrawImageWithRotate

        // Draw PolygonXY ***********************************************************************
        // Draw Polygon: points.X, points.Y -> Mercator

        public string DrawPolygonXY(List<Point> points, byte Color1, byte Color2, byte Color3, byte Color4)

        {
            string svar = objClassInterfaceMap.DrawPolygonXY(points, Color1, Color2, Color3, Color4);
            return svar;
        } // DrawPolygonXY

        // *********************************************************************** Draw PolygonXY

        // Draw PolygonLatLong ******************************************************************
        // Draw Polygon: points.X=Long, points.Y=Lat -> degree

        public string DrawPolygonLatLong(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4)
        {
            string svar = objClassInterfaceMap.DrawPolygonLatLong(pointPel, Color1, Color2, Color3, Color4);
            return svar;
        } // DrawPolygonLatLong

        // ****************************************************************** Draw PolygonLatLong

        // Draw LinesLatLong ******************************************************************
        // Draw Polygon: points.X=Long, points.Y=Lat -> degree

        public string DrawLinesLatLong(List<Mapsui.Geometries.Point> pointPel, byte Color1, byte Color2, byte Color3, byte Color4)
        {
            string svar = objClassInterfaceMap.DrawLinesLatLong(pointPel, Color1, Color2, Color3, Color4);
            return svar;
        } // DrawPolygonLatLong

        // ****************************************************************** Draw PolygonLatLong

        // DrawSector ************************************************************************
        // Sector

        public string DrawSectorXY(
                                   Point tpCenterPoint,
                                   // Color
                                   byte clr1,
                                   byte clr2,
                                   byte clr3,
                                   byte clr4,
                                   // m
                                   long iRadiusZone,
                                   // degree
                                   float SectorLeft,
                                   float SectorRight
                                   )
        {
            var svar = objClassInterfaceMap.DrawSectorXY(tpCenterPoint, clr1, clr2, clr3, clr4, iRadiusZone, SectorLeft, SectorRight);
            return svar;
        }

        // ************************************************************************ DrawSector

        // DrawSector ************************************************************************
        // Draw Sector LatLong: tpCenterPoint.X=Long, tpCenterPoint.Y=Lat -> degree

        public string DrawSectorLatLong(
                                   // Degree
                                   Mapsui.Geometries.Point tpCenterPoint,
                                   // Color
                                   byte clr1,
                                   byte clr2,
                                   byte clr3,
                                   byte clr4,
                                   // m
                                   long iRadiusZone,
                                   // degree
                                   float SectorLeft,
                                   float SectorRight
                                   )
        {
            var svar = objClassInterfaceMap.DrawSectorLatLong(tpCenterPoint, clr1, clr2, clr3, clr4, iRadiusZone, SectorLeft, SectorRight);
            return svar;
        }

        // ************************************************************************ DrawSector

        // CalculationAzimuth ****************************************************************
        // Calculation of azimuth from Point1 to Point2

        public double CalcAzimuth(double Lat1, double Long1, double Lat2, double Long2)
        {
            var Azimuth = objClassInterfaceMap.CalcAzimuth(Lat1, Long1, Lat2, Long2);
            return Azimuth;
        }

        // **************************************************************** CalculationAzimuth

        // DrawCurrentCoordinates ************************************************************
        // Нарисовать текущие координаты в зависимости от установленной в ini файле СК и
        // формата координат

        public void DrawCurrentCoordinates()
        {
            // -------------------------------------------------------------------------------------------------
            long ichislo1 = 0;
            double dchislo1 = 0;
            long ichislo2 = 0;
            double dchislo2 = 0;
            long ichislo3 = 0;
            double dchislo3 = 0;

            int ilatgrad = 0;
            double dlatmin = 0;
            int ilatmin = 0;
            long Llatmin = 0;
            double dlatsec = 0;
            int ilatsec = 0;
            long Llatsec = 0;

            int ilongrad = 0;
            double dlonmin = 0;
            int ilonmin = 0;
            long Llonmin = 0;
            double dlonsec = 0;
            int ilonsec = 0;
            long Llonsec = 0;

            // -------------------------------------------------------------------------------------------------
            // INI

            string s = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
            // -------------------------------------------------------------------------------------------------
            // WGS84

            // Latitude
            ichislo1 = (long)(GlobalVarMap.LAT_Rastr * 1000000);
            dchislo1 = ((double)ichislo1) / 1000000;
            // Longitude
            ichislo2 = (long)(GlobalVarMap.LONG_Rastr * 1000000);
            dchislo2 = ((double)ichislo2) / 1000000;
            // Altitude
            ichislo3 = (long)(GlobalVarMap.H_Rastr * 1000000);
            dchislo3 = ((double)ichislo3) / 1000000;

            if (GlobalVarMap.FlagLanguageMap == 1)
                Txt5.Text = Convert.ToString(dchislo3) + " m";
            else if (GlobalVarMap.FlagLanguageMap == 0)
                Txt5.Text = Convert.ToString(dchislo3) + " м";
            else
                Txt5.Text = Convert.ToString(dchislo3) + " m";

            // ...................................................................................................
            // WGS 84

            if (initMapYaml.SysCoord == 0)
            {
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0'; // Lat
                    Txt3.Text = "E " + Convert.ToString(dchislo2) + '\xB0'; // Long
                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm
                else if (initMapYaml.FormatCoord == 1)
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref dlatmin
                                                );
                    Llatmin = (long)(dlatmin * 10000);
                    dlatmin = ((double)Llatmin) / 10000;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(dlatmin) + "'"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref dlonmin
                                                );
                    Llonmin = (long)(dlonmin * 10000);
                    dlonmin = ((double)Llonmin) / 10000;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(dlonmin) + "'"; // Lon
                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss
                else
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref ilatmin,
                                                  ref dlatsec
                                                );
                    Llatsec = (long)(dlatsec * 100);
                    dlatsec = ((double)Llatsec) / 100;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(ilatmin) + "'" + Convert.ToString(dlatsec) + "''"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref ilonmin,
                                                  ref dlonsec
                                                );
                    Llonsec = (long)(dlonsec * 100);
                    dlonsec = ((double)Llonsec) / 100;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(ilonmin) + "'" + Convert.ToString(dlonsec) + "''"; // Lat
                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            } // WGS84
              // ...................................................................................................
              // SK42,m
            else if (initMapYaml.SysCoord == 2)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_Krug(GlobalVarMap.LAT_Rastr,
                                                GlobalVarMap.LONG_Rastr,
                                                ref xxx, ref yyy);

                if (GlobalVarMap.FlagLanguageMap == 1)
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " м";
                else
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";

                if (GlobalVarMap.FlagLanguageMap == 1)
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " м";
                else
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
            } // SK42,m
              // ...................................................................................................
              // SK42,grad
            else if (initMapYaml.SysCoord == 1)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_SK42_BL(GlobalVarMap.LAT_Rastr,
                                                 GlobalVarMap.LONG_Rastr,
                                                 ref xxx, ref yyy);
                // Latitude
                ichislo1 = (long)(xxx * 1000000);
                dchislo1 = ((double)ichislo1) / 1000000;
                // Longitude
                ichislo2 = (long)(yyy * 1000000);
                dchislo2 = ((double)ichislo2) / 1000000;

                //Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0';
                //Txt3.Text = "E " + Convert.ToString(dchislo2) + '\xB0';

                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD.dddddd

                if (initMapYaml.FormatCoord == 0)
                {
                    Txt1.Text = "N " + Convert.ToString(dchislo1) + '\xB0'; // Lat
                    Txt3.Text = "E " + Convert.ToString(dchislo2) + '\xB0'; // Long
                }
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM.mmmm
                else if (initMapYaml.FormatCoord == 1)
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref dlatmin
                                                );
                    Llatmin = (long)(dlatmin * 10000);
                    dlatmin = ((double)Llatmin) / 10000;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(dlatmin) + "'"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GM(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref dlonmin
                                                );
                    Llonmin = (long)(dlonmin * 10000);
                    dlonmin = ((double)Llonmin) / 10000;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(dlonmin) + "'"; // Lon
                } // DD MM.mmmm
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                // DD MM SS.ss
                else
                {
                    // Lat
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo1,
                                                  ref ilatgrad,
                                                  ref ilatmin,
                                                  ref dlatsec
                                                );
                    Llatsec = (long)(dlatsec * 100);
                    dlatsec = ((double)Llatsec) / 100;
                    Txt1.Text = "N " + Convert.ToString(ilatgrad) + '\xB0' + Convert.ToString(ilatmin) + "'" + Convert.ToString(dlatsec) + "''"; // Lat

                    // Long
                    ClassGeoCalculator.f_Grad_GMS(
                                                  dchislo2,
                                                  ref ilongrad,
                                                  ref ilonmin,
                                                  ref dlonsec
                                                );
                    Llonsec = (long)(dlonsec * 100);
                    dlonsec = ((double)Llonsec) / 100;
                    Txt3.Text = "E " + Convert.ToString(ilongrad) + '\xB0' + Convert.ToString(ilonmin) + "'" + Convert.ToString(dlonsec) + "''"; // Lat
                } // DD MM SS.ss
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            } // SK42,grad
              // ...................................................................................................
              // Mercator
            else if (initMapYaml.SysCoord == 3)
            {
                double xxx = 0;
                double yyy = 0;
                ClassGeoCalculator.f_WGS84_Mercator(GlobalVarMap.LAT_Rastr,
                                                    GlobalVarMap.LONG_Rastr,
                                                    ref xxx, ref yyy);

                if (GlobalVarMap.FlagLanguageMap == 1)
                {
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
                }
                else if (GlobalVarMap.FlagLanguageMap == 0)
                {
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " м";
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " м";
                }
                else
                {
                    Txt1.Text = "X " + Convert.ToString((int)xxx) + " m";
                    Txt3.Text = "Y " + Convert.ToString((int)yyy) + " m";
                }
            } // Mercator
            // ...................................................................................................

            // -------------------------------------------------------------------------------------------------
            try
            {
                GlobalVarMap.Str_Rastr = ClassGeoCalculator.f_WGS84_MGRS(GlobalVarMap.LAT_Rastr, GlobalVarMap.LONG_Rastr);
                Txt7.Text = "MGRS: " + GlobalVarMap.Str_Rastr;
            }
            catch
            {
                if (GlobalVarMap.FlagLanguageMap == 1)
                    MessageBox.Show("Invalid coordinates");
                else if (GlobalVarMap.FlagLanguageMap == 0)
                    MessageBox.Show("Недопустимые координаты");
                else
                    MessageBox.Show("Yanlış koordinatlar");

                return;
            }
            // -------------------------------------------------------------------------------------------------
        }

        // ************************************************************ DrawCurrentCoordinates

        // ChangeLanguage ********************************************************************************
        // AAA
        // Флаг языка: по умолчанию английский (false)
        // true  -> русский
        //FlagLanguageMap = false;

        public void fChangeLanguage(byte lang)
        {
            // Переход на русский **********************************************************************

            if (lang == 0)
            {
                GlobalVarMap.FlagLanguageMap = 0;
                Resources["Lang"] = "Установить расстояние от собственной станции";
                Resources["SpoofAdd"] = "Добавить спуфинг";
                Resources["SpoofDel"] = "Удалить спуфинг";
                Resources["AddIriToSuppress"] = "Добавить ИРИ на РП";
                Resources["DeleteIriSuppress"] = "Удалить ИРИ с РП";
                Resources["PointLOSZone"] = "Определить центр. точку ЗПВ";
            }
            // ********************************************************************** Переход на русский

            // Переход на английский *******************************************************************
            else if (lang == 1)
            {
                GlobalVarMap.FlagLanguageMap = 1;
                Resources["Lang"] = "Set distance from own station";
                Resources["SpoofAdd"] = "Add spoofing";
                Resources["SpoofDel"] = "Remove spoofing";
                Resources["AddIriToSuppress"] = "Add to Suppress";
                Resources["DeleteIriSuppress"] = "Delete Suppress";
                Resources["PointLOSZone"] = "Set central point LOS";
            }
            // ******************************************************************* Переход на английский
            else if (lang == 2)
            {
                GlobalVarMap.FlagLanguageMap = 2;
                Resources["Lang"] = "Öz stansiyanızdan məsafə təyin edin";
                Resources["SpoofAdd"] = "Saxtakarlıq əlavə edin";
                Resources["SpoofDel"] = "Saxtakarlığı aradan qaldırmaq";
                Resources["AddIriToSuppress"] = "Add to Suppress";
                Resources["PointLOSZone"] = "Set central point LOS";
            }
            objClassMapRastrReDraw.ReDrawMapAll();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnGetCoordSpoof?.Invoke(this, new CoordEventArgs(GlobalVarMap.LAT_Rastr_Right, GlobalVarMap.LONG_Rastr_Right, 0));
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            objClassMapRastrReDraw.AddSpoof = false;
            objClassMapRastrReDraw.ReDrawMapAll();
        }

        private void MenuItem_Click_Add_To_Suppress(object sender, RoutedEventArgs e)
        {
            double Radius = 0.009;
            List<TableReconFWS> listTD = new List<TableReconFWS>(objClassInterfaceMap.List_SRW_FRF_TD);

            for (int i = 0; i < listTD.Count; i++)
            {
                if (Math.Pow(ClickedCoord.Latitude - listTD[i].Coordinates.Latitude,2) + Math.Pow(ClickedCoord.Longitude - listTD[i].Coordinates.Longitude,2) <= Math.Pow(Radius, 2))
                {
                    OnAddIRIToSuppres?.Invoke(sender, new IRIEventArgs(listTD[i]));
                }

            }

        }

        private void MenuItem_Click_Delete_Suppress(object sender, RoutedEventArgs e)
        {
            double Radius = 0.009;
            List<TableSuppressFWS> listSuppress = new List<TableSuppressFWS>(objClassInterfaceMap.List_SRW_FRF_RS);

            for (int i = 0; i < listSuppress.Count; i++)
            {
                if (Math.Pow(ClickedCoord.Latitude - listSuppress[i].Coordinates.Latitude, 2) + Math.Pow(ClickedCoord.Longitude - listSuppress[i].Coordinates.Longitude, 2) <= Math.Pow(Radius, 2))
                {
                    DeleteIRISuppress?.Invoke(sender, new IRISuppressEventArgs(listSuppress[i]));
                }

            }

        }

        private List<Feature> losZone = new List<Feature>();
       
        private async void MenuItem_Click_Point_LOS_Zone(object sender, RoutedEventArgs e)
        {
           

            //if (!MapControl.IsMapLoaded)
            //{
            //    return;
            //}
            // Mapsui.Styles.Color BrushZoneLos = Mapsui.Styles.Color.FromArgb(150, 109, 11, 255);
            //List<ClassJS> List_JS1 = new List<ClassJS>(objClassInterfaceMap.List_JS);
            //if (List_JS1.Count == 0) { return; }


            OnAddLOS?.Invoke(sender, new ClassArg(ClickedCoord.Latitude, ClickedCoord.Longitude, ClickedCoord.Altitude ?? 0, 0, 0, "")); 

            


            //var zone = this.zoneCalculator.CreateLosZone(
            //                          centerZone,
            //                          GetHeightFunc,
            //                          List_JS1[0].AntHeightSup,
            //                          3);

            //var zoneNew = zone.Select(t => new List<Mapsui.Geometries.Point>() { new Mapsui.Geometries.Point(t.Start.X, t.Start.Y), new Mapsui.Geometries.Point(t.End.X, t.End.Y) });

            //if (losZone != null)
            //{
            //    foreach (var b in losZone)
            //        MapControl.RemoveObject(b);
            //}

            //foreach (var line in zoneNew)
            //{
            //    losZone.Add(DrawFeaturePolygonLine(line, new Mapsui.Styles.Pen(BrushZoneLos, 3)));
            //}

        }

        //private Feature DrawFeaturePolygonLine(List<Mapsui.Geometries.Point> wgs84Points, Mapsui.Styles.Pen outlinePen)
        //{
        //    Feature ifeature = new Feature();
        //    if (wgs84Points == null)
        //        return ifeature;
        //    try
        //    {
        //        ifeature = MapControl.AddPolyline(wgs84Points, outlinePen);

        //        return ifeature;

        //    }
        //    catch { return null; }
        //}

        //public int GetHeightFunc(int x, int y)
        //{

        //    var converted = MercatorDependOnMapType.ToLonLat(x, y);

        //    var height = MapControl.DtedMass.GetElevation(converted.X, converted.Y);
           
        //    return height ?? 0;
        //}

        //private MemoryProvider _objectLayerMemoryProvider;

        //public void RemoveObject(IMapJammerObject mapJammerObject)
        //{
        //    if (_mapJammerObjects.Contains(mapJammerObject))
        //    {
        //        _mapJammerObjects.Remove(mapJammerObject);
        //        _objectLayerMemoryProvider.Features.Delete(mapJammerObject.Feature["key"]);
        //        //MapControl.Refresh();
        //    }

        //}



        // ******************************************************************************** ChangeLanguage

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! INTERFACE
    } // Class
} // Namespace