﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastrControl
{
    public class ReliefEventArgs:EventArgs
    {
        public List<Coordinates> coordinates { get; set; }

        public double AntennaJS { get; set; }

        public double AntennaEnemy { get; set; }
        public ReliefEventArgs(List<Coordinates> _coordinates, double antennaJS, double antennaEnemy)
        {
            this.coordinates = _coordinates;
            AntennaJS = antennaJS;
            AntennaEnemy = antennaEnemy;
        }
    }
}
