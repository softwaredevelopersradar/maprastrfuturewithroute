﻿using System.Collections.Generic;

namespace WpfMapRastrControl
{
    // Route
    // Маршрут
    public class ClassRoute
    {
        // Для отображения маршрута на карте: true -> отображать
        public bool checkMap = true;

        // Название маршрута
        public string Name_Route = "";

        // Длина маршрута
        public double Length_Route = 0;

        // Лист точек маршрута
        public List<Objects> list_Points_Route = new List<Objects>();
    } // Class
} // Namespace