﻿using System;

namespace WpfMapRastrControl
{
    public class CoordEventArgs : EventArgs
    {
        public double Lat { get; set; }
        public double Lon { get; set; }

        public double Alt { get; set; }


        public CoordEventArgs(double Lat, double Lon, double Alt)
        {
            this.Lat = Lat;
            this.Lon = Lon;
            this.Alt = Alt;
        }
    }
}