﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;
using static GeoUtility.GeoSystem.MapService;

namespace WpfMapRastrControl
{
    public class JSMapObj
    {
        public IMapObject Station { get; set; }

        public int ID { get; set; }

        public JSMapObj()
        { }

    }
}
