﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WpfAzimuthControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        public event EventHandler OnClearAzimuth;

        public event EventHandler OnCheckedShowAzimuth;

        public event EventHandler OnUnCheckedShowAzimuth;

        public UserControl1()
        {
            InitializeComponent();
        }

        // Clear **************************************************************************
        public void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            OnClearAzimuth?.Invoke(this, new EventArgs());
        }

        // ************************************************************************** Clear

        // CheckBox ***********************************************************************
        private void checkBoxShow_Checked(object sender, RoutedEventArgs e)
        {
            OnCheckedShowAzimuth?.Invoke(this, new EventArgs());
        }

        private void checkBoxShow_Unchecked(object sender, RoutedEventArgs e)
        {
            OnUnCheckedShowAzimuth?.Invoke(this, new EventArgs());
        }

        // *********************************************************************** CheckBox
    } // Class
} // Namespace